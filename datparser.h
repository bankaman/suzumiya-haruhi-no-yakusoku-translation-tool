#ifndef DATPARSER_H
#define DATPARSER_H

#include <QFile>
#include <QTextCodec>
#include <QFileDialog>
#include <QStack>

#include "datfile.h"
class MainWindow;

namespace Ui {
class DatParser;
}

class DatParser : public QWidget
{
    Q_OBJECT

public:
    explicit DatParser(QWidget *parent = 0);
    ~DatParser();

    DatFile dat;
    MainWindow *main_window;
    QString last_filename;

    void openFile(const QString &filePath);

    static bool ifgoto(int where);

public slots:
    void on_open_button_clicked();

    void on_pushButton_start_clicked();

    void showMessageBox(QString message);

private slots:

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_read_as_hex_clicked();

    void on_pushButton_read_as_text_clicked();

    void on_lineEdit_cout_to_read_textChanged(const QString &arg1);

    void on_checkBox_clicked(bool checked);

    void on_checkBox_2_clicked(bool checked);

    void on_actionPrint_dat_triggered();

    void on_pushButton_reset_cursor_clicked();

private:
    Ui::DatParser *ui;



    void update_screen();
    void log(QString str);




    QTextCodec *read_codec;

    static DatParser* instance;
};

#endif // DATPARSER_H
