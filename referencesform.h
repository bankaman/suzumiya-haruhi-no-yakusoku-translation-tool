#ifndef REFERENCESFORM_H
#define REFERENCESFORM_H

#include <QWidget>

#include <QListWidget>
#include <QSettings>
#include <references.h>

namespace Ui {
class ReferencesForm;
}

class ReferencesForm : public QWidget
{
    Q_OBJECT

public:
    explicit ReferencesForm(QWidget *parent = 0);
    ~ReferencesForm();

    void update_data();

private slots:
    void on_trDirButton_clicked();
    void on_orDirButton_clicked();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::ReferencesForm *ui;
    QSettings *settings;

    QString translatedFilesDir;
    QString originalFilesDir;
    void init();

    void updateLabels();
    void showMessageBox(QString message);

    // QWidget interface
protected:
    void showEvent(QShowEvent *event);
};

#endif // REFERENCESFORM_H
