#include "btdfile.h"

BtdFile::BtdFile()
{
    text_codec = QTextCodec::codecForName("Shift-JIS");
}

BtdFile::~BtdFile()
{
    clear_btd();
}

QString BtdFile::read_as_string(QByteArray &data, int cursor)
{
    QByteArray out;
    while(data.at(cursor)!=0){
        out.append(data.at(cursor));
        cursor++;
        if(cursor>=data.length())
            break;
    }
    cursor++;
    return text_codec->toUnicode(out);
}

QByteArray BtdFile::read_until(unsigned char byte, QByteArray &data, int cursor)
{
    QByteArray out;
    int t = data.length();
    while((cursor<t) && (data.at(cursor) != byte)){
        out.append(data.at(cursor));
        cursor++;
    }
    return out;
}



void BtdFile::clear_btd()
{
    Theme *t;
    Variant *v;
    for(int i=0;i<btd.themes.length();i++){
        t=btd.themes.at(i);
        for(int j=0;j<t->variants.length();j++){
            delete t->variants.at(j);
        }
        delete t;
    }
    btd.themes.clear();
    btd.after.clear();
}

void BtdFile::adjust_size(QByteArray &ba, int size)
{
    if(ba.length()>size)
        ba=ba.mid(0,size);
    if(ba.length()<size)
        ba.append(size-ba.length(),(char)0);
}


bool BtdFile::load(QString filename)
{
    QFile file;
    file.setFileName(filename);
    if(!file.open(QFile::ReadOnly))
        return false;
	
	this->filename = filename;
		
    References::clear();

    QByteArray data;
    data = file.readAll();
    file.close();

    QString t;
    int cursor=0;
    t=read_as_string(data,cursor);
    if(t!="BTD")
        return false;
    cursor=4;

    clear_btd();

    Theme *th;
    Variant *var;
    for(int i=0;i<12;i++){
        th=new Theme;
        th->before.append(data.mid(cursor,4)); cursor+=4;
        for(int j=0;j<3;j++){
            var=new Variant;
            var->before.append(data.mid(cursor,4)); cursor+=4;
            var->text_bytes.append(data.mid(cursor,48)); cursor+=48;
            var->text=read_as_string(var->text_bytes,0);
            //qDebug()<<"Variant:"<<var->text;
            var->dat_params.append(data.mid(cursor,6)); cursor+=6;
            var->dat_file=read_as_string(data,cursor); cursor+=14;
            if(!var->dat_file.isEmpty())
                References::add(References::TYPE_SCRIPT, var->dat_file);
            th->variants.append(var);
        }
        th->text_bytes.append(data.mid(cursor,24)); cursor+=24;
        th->text = read_as_string(th->text_bytes,0);
        QByteArray temp;
        temp = read_until(0,th->text_bytes,0);
        th->after = read_until(0,th->text_bytes,temp.length()+1);
        //qDebug()<<"Theme:"<<th->text<<" | "<<th->after<<" | "<<QString(th->after);
        btd.themes.append(th);
    }
    btd.after.append(data.mid(cursor));
    return true;
}

bool BtdFile::save(QString filename)
{
    QByteArray out;

    out.append("BTD");
    out.append((char)0);

    Theme *th;
    Variant *var;
    QByteArray temp;
    for(int i=0;i<btd.themes.length();i++){
        th = btd.themes[i];
        out.append(th->before);
        for(int j=0;j<th->variants.length();j++){
            var = th->variants[j];
            out.append(var->before);
            var->text_bytes = text_codec->fromUnicode(var->text);
            adjust_size(var->text_bytes,VARIANT_MAX_SIZE);
            out.append(var->text_bytes);
            out.append(var->dat_params);
            temp.clear();
            temp.append(text_codec->fromUnicode(var->dat_file));
            adjust_size(temp,14);
            out.append(temp);
        }
        temp.clear();
        temp.append(text_codec->fromUnicode(th->text));
        if(temp.length()>THEME_MAX_SIZE-th->after.length()+1)
            temp = temp.mid(0,THEME_MAX_SIZE-th->after.length());
        temp.append((char)0);
        temp.append(th->after);
        adjust_size(temp,THEME_MAX_SIZE);
        out.append(temp);
    }
    out.append(btd.after);

    QFile file;
    file.setFileName(filename);
    if(!file.open(QFile::WriteOnly))
        return false;

    file.write(out);
    file.close();
    return true;
}

QStringList BtdFile::get_themes()
{
    QStringList out;
    for(int i=0;i<btd.themes.length();i++){
        out.append(btd.themes.at(i)->text);
    }
    return out;
}

QStringList BtdFile::get_variants(int theme_id)
{
    QStringList out;
    Theme *th;
    th=btd.themes.at(theme_id);
    for(int i=0;i<th->variants.length();i++)
        out.append(th->variants.at(i)->text);

    return out;
}

QString BtdFile::get_variant(int theme_id, int variant_id)
{
    return btd.themes[theme_id]->variants[variant_id]->text;
}

QString BtdFile::get_variant_filename(int theme_id, int variant_id)
{
    return btd.themes[theme_id]->variants[variant_id]->dat_file;
}

QString BtdFile::get_theme(int theme_id)
{
    return btd.themes[theme_id]->text;
}

int BtdFile::get_theme_max_size(int theme_id)
{
    return THEME_MAX_SIZE-btd.themes[theme_id]->after.length();
}

void BtdFile::set_theme_text(int theme_id, QString text)
{
    btd.themes[theme_id]->text = text;
}

void BtdFile::set_variant_text(int theme_id, int variant_id, QString text)
{
    btd.themes[theme_id]->variants[variant_id]->text = text;
}

QString BtdFile::getFileName(){
	return filename;
}
