#ifndef SCRIPTMAP_H
#define SCRIPTMAP_H

#include <QWidget>

#include "datfile.h"

namespace Ui {
class ScriptMap;
}

class ScriptMap : public QWidget
{
    Q_OBJECT

public:
    explicit ScriptMap(QWidget *parent = 0);
    ~ScriptMap();

    void update(DatFile &dat);



private slots:
    void on_pushButton_clicked();



    void on_pushButton_restore_cyr_bellow_clicked();

    void on_map_text_orig_anchorClicked(const QUrl &arg1);

    void on_map_text_anchorClicked(const QUrl &arg1);

private:
    Ui::ScriptMap *ui;
    DatFile *cur_dat_file;

    QString parseMap(QString string);
};

#endif // SCRIPTMAP_H
