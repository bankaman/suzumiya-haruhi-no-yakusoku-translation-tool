#ifndef BTDEDITOR_H
#define BTDEDITOR_H

#include <QWidget>
#include <QMessageBox>
#include <QFileDialog>
#include <QStandardItemModel>

#include "btdfile.h"

namespace Ui {
class BtdEditor;
}

class BtdEditor : public QWidget
{
    Q_OBJECT

public:
    explicit BtdEditor(QWidget *parent = 0);
    ~BtdEditor();

    void load_file(QString filename);

private slots:
    void on_LoadButton_clicked();

    void on_treeView_clicked(const QModelIndex &index);

    void on_lineEditTheme_textChanged(const QString &text);

    void on_lineEdit_Variant_textChanged(const QString &text);

    void on_pushButton_clicked();

    void on_pushButton_save_clicked();

    void on_pushButton_openDatFile_clicked();

    void on_pushButton_Theme_tolatin_clicked();

    void on_pushButton_variant_tolatin_clicked();

private:
    Ui::BtdEditor *ui;
    QString current_file;

    int current_theme;
    int current_variant;

    BtdFile btd;

    QStandardItemModel model;

    void showMessageBox(QString message);
    void update_ui_tree();
    void update_ui_labels();
    void update_ui_input_boxs();
};

#endif // BTDEDITOR_H
