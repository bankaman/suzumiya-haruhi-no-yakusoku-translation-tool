#ifndef H_INJECT_STRUCTS
#define H_INJECT_STRUCTS

#include "defines.h"

typedef struct InjectionI16 InjectionI16, *PInjectionI16;
struct InjectionI16 {
    uint addrHI;
	uint addrLO;
	uint target;
};

typedef struct InjectionJal InjectionJal, *PInjectionJal;
struct InjectionJal {
    uint addrJal;
	uint target;
};


typedef struct InjectionFull InjectionFull, *PInjectionFull;
struct InjectionFull {
    uint addr;
	uint target;
};

#endif