#include "scriptTrial.h"

#include <string.h>

#include "defines.h"
#include "imports.h"
#include "pspctrl.h"
#include "utils.h"

short *scriptTrialSomeStatusVarMaybe = CONVERT_ADDR(0x13c982);
short *scriptTrialSomeStatusVar2Maybe = CONVERT_ADDR(0x13c984);
int *currentCharacterId = CONVERT_ADDR(0x13c988);
// char **characterNames = CONVERT_ADDR(0x138300);
DisplayTextureStruct *ScriptTrialCharNameBgTexture = CONVERT_ADDR(0x13ca50);
DisplayTextureStruct *scriptTrialCharNameOutlineTexture = CONVERT_ADDR(0x13c994);

uint (*scriptTrialDoHideUi)() = CONVERT_ADDR(0x27988);

char *charaNames[] = {
    0,
    "Kv~",
    "Xapyxy",
    "@py~p-p~",
    "Hasao",
    "Kytx}y",
    "W-p~",
    "R}y~",
    "Dur{p",
    "Sp~ysy",
    "K~y{ytp",
    "Ky}yty-p~",
    "@py~p-p~",
    "C|prp {}. {|qp",
    "X|u~ {}. {|qp 1",
    "X|u~ {}. {|qp 2",
    "Ruv~{p",
    "Vpy",
    "My{",
    "_{y",
    "I{y",
    "Quuy",
    "X|u~ {|qp",
    "K}. Lpqpy",
    "E¢à",
    "VÌº",
    "HHH"};

// int delay = 120;
// int charaIdG = 1;

int drawCharacterName() {
    int result;

    if (*scriptTrialSomeStatusVarMaybe == 0) {
        int charaId = *currentCharacterId;
        // int charaId = charaIdG;
        // delay--;
        // if (delay < 0) {
        //     delay = 120;
        //     charaIdG = (charaIdG + 1) % 27;
        // }
        if (*scriptTrialSomeStatusVar2Maybe == 0) {
            result = 1;
        } else if (charaId == 0) {
            result = 1;
        } else {
            uint hideUi = scriptTrialDoHideUi();

            if (hideUi == 0) {
                char *charaName = charaNames[charaId];
                drawTexture(ScriptTrialCharNameBgTexture);
                drawTexture(scriptTrialCharNameOutlineTexture);
                setTextColor(0xffffffff);

                float charaNameWidth;
                float textSize = 1;
                float topOffset;
                int fontVertHeight;
                getCurFontParams(NULL, NULL, NULL, NULL, NULL, &fontVertHeight);
                float fontHeight;
                while (true) {
                    charaNameWidth = getTextWidth(charaName, textSize);
                    fontHeight = fontVertHeight / 64;
                    topOffset = scriptTrialCharNameOutlineTexture->height / 2 - fontHeight / 2;
                    if (charaNameWidth <= scriptTrialCharNameOutlineTexture->width - 10) {
                        break;
                    }
                    textSize -= 0.1;
                };

                drawText(charaName,
                         scriptTrialCharNameOutlineTexture->xPos + scriptTrialCharNameOutlineTexture->width / 2 - charaNameWidth / 2 - 4,
                         scriptTrialCharNameOutlineTexture->yPos + topOffset,
                         scriptTrialCharNameOutlineTexture->field4_0x10,
                         textSize, 1,
                         0.0, 0.0);

                result = 1;
            } else {
                result = 1;
            }
        }
    } else {
        result = 0;
    }
    return result;
}

//-------------------------------------------------------------------------------

typedef enum ScriptTrialWaitMode {
    WAIT_MODE_MANUAL = 0,
    WAIT_MODE_AUTO = 1,
    UNKNOWN_WAIT_MODE = 2
} ScriptTrialWaitMode;

int (*FUN_000c2a30)() = CONVERT_ADDR(0xc2a30);
int (*textLogRelatedFunc)() = CONVERT_ADDR(0xbbaf4);
void (*FUN_0002799c)() = CONVERT_ADDR(0x2799c);
void (*FUN_00028024)() = CONVERT_ADDR(0x28024);
int (*FUN_00022c18)() = CONVERT_ADDR(0x22c18);
ScriptTrialWaitMode (*getScriptTrialWaitMode)() = CONVERT_ADDR(0x27e34);
int (*FUN_00033fcc)() = CONVERT_ADDR(0x33fcc);
int (*FUN_00027e78)() = CONVERT_ADDR(0x27e78);
int (*getCurrentCharaId)() = CONVERT_ADDR(0x1fdc8);
int (*FUN_00023b68)(int) = CONVERT_ADDR(0x23b68);
void (*FUN_0008cd0c)() = CONVERT_ADDR(0x8cd0c);
void (*FUN_00033fa0)() = CONVERT_ADDR(0x33fa0);
void (*FUN_0002d778)() = CONVERT_ADDR(0x2d778);
uint (*FUN_000278d8)() = CONVERT_ADDR(0x278d8);
void (*FUN_000bb2a8)(int, int) = CONVERT_ADDR(0xbb2a8);
void (*showNextLineIndicator)(int) = CONVERT_ADDR(0x21868);
char *(*isStr1ContainsStr2)(char *, char *) = CONVERT_ADDR(0x21868);
// void (*addTextLogRec)(int, char *) = CONVERT_ADDR(0xbc2d0);
void addTextLogRec(int, char *);
char *(*strcpyWithLength)(char *dstStr, char *srcStr, int length) = CONVERT_ADDR(0xf390);

byte *scriptTrialFlags = CONVERT_ADDR(0x13d2a2);
byte *BYTE_000e66a5 = CONVERT_ADDR(0xe66a5);
byte *scriptTrialControlFlag = 0x894129e;
char **scriptLineTextPtr = CONVERT_ADDR(0x13d1f4);
short *scriptLinePrintedLength = CONVERT_ADDR(0x13d29c);

char *scriptTrialText = &newScriptTrialTextBuffer;  // CONVERT_ADDR(0x13d200);
short *scriptTrialLineTotalLength = CONVERT_ADDR(0x13d1fe);
short *SHORT_0013d2a0 = CONVERT_ADDR(0x13d2a0);
short *SHORT_000e66a8 = CONVERT_ADDR(0xe66a8);

char **scriptTrialCurrentChar = CONVERT_ADDR(0x13d1f8);
short *scriptTrialCharDelayCounter = CONVERT_ADDR(0x13d1fc);
byte *scriptTrialDefaultCharDelay = CONVERT_ADDR(0xe66a6);

int scriptTrialControlLogic() {
    short charLen;
    int resultVar;
    enum PspCtrlButtons buttons;
    int charaId;
    int charaId2;
    int charaId3;
    int strLength;

    ScriptTrialWaitMode waitMode;

    if ((*scriptTrialFlags & 1) == 0) {
        resultVar = 0;
    } else {
        resultVar = FUN_000c2a30();  // always 0 in here ?
        if (resultVar == 0) {
            resultVar = textLogRelatedFunc();
            if (resultVar == 0) {
                FUN_0002799c();
                FUN_00028024();

                if ((*scriptTrialControlFlag & 0b10) > 0) {
                    //   rxp~~u  uwy}} pxsrp  u~pwu}
                    resultVar = FUN_00022c18();
                    if (resultVar != 0) {
                        return 1;
                    }
                    *scriptTrialControlFlag = *scriptTrialControlFlag & 0xfd;
                }
                if ((*scriptTrialControlFlag & 1) == 0) {
                    waitMode = getScriptTrialWaitMode();
                    if (waitMode == UNKNOWN_WAIT_MODE) {
                        *scriptTrialCharDelayCounter = 0;
                        strLength = strlen(*scriptLineTextPtr);
                        *scriptLinePrintedLength = (short)strLength;
                        strcpy(scriptTrialText, *scriptLineTextPtr);
                        *scriptTrialControlFlag = (*scriptTrialControlFlag & 0xfa) | 1;
                        waitMode = getScriptTrialWaitMode();
                        if (waitMode != WAIT_MODE_MANUAL) {
                            return 1;
                        }
                        showNextLineIndicator(1);
                        return 1;
                    }
                    buttons = readController(PRESS_ONCE);
                    if ((buttons & PSP_CTRL_CIRCLE) == 0) {
                        if (*scriptTrialCharDelayCounter - 1 != 0) {
                            *scriptTrialCharDelayCounter = *scriptTrialCharDelayCounter - 1;
                            return 1;
                        }
                        if (*BYTE_000e66a5 == 4) {
                            *scriptTrialCharDelayCounter = 0;
                            *scriptLinePrintedLength = (short)strlen(*scriptLineTextPtr);
                            strcpy(scriptTrialText, *scriptLineTextPtr);
                            *scriptTrialControlFlag = (*scriptTrialControlFlag & 0xfa) | 1;
                            waitMode = getScriptTrialWaitMode();
                            if (waitMode == WAIT_MODE_MANUAL) {
                                showNextLineIndicator(1);
                            }
                        } else {
                            /*
                            Xpy~u {yrp~yu {{p u{p r
                            qu qpwu~y u{p
                            */
                            *scriptTrialCharDelayCounter = *scriptTrialDefaultCharDelay;
                            charLen = getCharLen(*scriptTrialCurrentChar);
                            *scriptTrialCurrentChar += charLen;

                            *scriptLinePrintedLength = *scriptLinePrintedLength + charLen;
                            if (*scriptLinePrintedLength < *scriptTrialLineTotalLength) {
                                strcpyWithLength(scriptTrialText, *scriptLineTextPtr, (int)*scriptLinePrintedLength);
                                scriptTrialText[*scriptLinePrintedLength] = 0;
                            } else {
                                *scriptTrialControlFlag = (*scriptTrialControlFlag & 0xfe) | 1;
                                waitMode = getScriptTrialWaitMode();
                                if (waitMode == WAIT_MODE_MANUAL) {
                                    showNextLineIndicator(1);
                                }
                            }
                        }
                    } else {
                        if (getScriptTrialWaitMode() == WAIT_MODE_MANUAL) {
                            *scriptTrialCharDelayCounter = 0;
                            *scriptLinePrintedLength = (short)strlen(*scriptLineTextPtr);
                            strcpy(scriptTrialText, *scriptLineTextPtr);
                            *scriptTrialControlFlag = (*scriptTrialControlFlag & 0xfa) | 1;
                            waitMode = getScriptTrialWaitMode();
                            if (resultVar == WAIT_MODE_MANUAL) {
                                showNextLineIndicator(1);
                            }
                        }
                    }
                } else {
                    resultVar = FUN_00022c18();
                    if (resultVar != 0) {
                        return 1;
                    }
                    waitMode = getScriptTrialWaitMode();
                    if (waitMode == UNKNOWN_WAIT_MODE) {
                        resultVar = FUN_00033fcc();
                        if (resultVar == 0) {
                            FUN_0008cd0c();
                        } else {
                            FUN_00033fa0();
                        }
                        showNextLineIndicator(0);
                        FUN_000bb2a8(0, 0);
                        resultVar = FUN_00027e78();
                        if ((resultVar != 1) && (resultVar != 5)) {
                            charaId3 = getCurrentCharaId();
                            addTextLogRec(charaId3, scriptTrialText);
                            FUN_0002d778();
                        }
                        *scriptTrialFlags = *scriptTrialFlags & 0xfe;
                    } else if (waitMode == WAIT_MODE_AUTO) {
                        if ((int)((uint)*scriptTrialControlFlag << 0x1d) < 0) {
                            resultVar = FUN_00023b68(2);
                            if (((resultVar != 2) && (resultVar != 1)) &&
                                (*SHORT_0013d2a0 = *SHORT_0013d2a0 + -1, *SHORT_0013d2a0 == 0)) {
                                *scriptTrialControlFlag = *scriptTrialControlFlag & 0xfb;
                                resultVar = FUN_00033fcc();
                                if (resultVar == 0) {
                                    FUN_0008cd0c();
                                } else {
                                    FUN_00033fa0();
                                }
                                showNextLineIndicator(0);
                                FUN_000bb2a8(0, 0);
                                resultVar = FUN_00027e78();
                                if ((resultVar != 1) && (resultVar != 5)) {
                                    charaId2 = getCurrentCharaId();
                                    addTextLogRec(charaId2, scriptTrialText);
                                    FUN_0002d778();
                                }
                                *scriptTrialFlags = *scriptTrialFlags & 0xfe;
                            }
                        } else {
                            resultVar = FUN_000278d8();
                            if (resultVar == 0) {
                                resultVar = FUN_00022c18();
                                if ((resultVar == 0) &&
                                    (((resultVar = FUN_00023b68(2), resultVar == 3 || (resultVar == 0)) ||
                                      (resultVar == 4)))) {
                                    *scriptTrialControlFlag = (*scriptTrialControlFlag & 0xfb) | 4;
                                    *SHORT_0013d2a0 = *SHORT_000e66a8;
                                }
                            } else {
                                *scriptTrialControlFlag = (*scriptTrialControlFlag & 0xfb) | 4;
                                *SHORT_0013d2a0 = *SHORT_000e66a8;
                            }
                        }
                    } else {
                        if (waitMode != WAIT_MODE_MANUAL) {
                            return 1;
                        }
                        buttons = readController(PRESS_ONCE);
                        if ((buttons & PSP_CTRL_CIRCLE) != 0) {
                            resultVar = FUN_00033fcc();

                            if (resultVar == 0) {
                                FUN_0008cd0c();
                            } else {
                                FUN_00033fa0();
                            }
                            showNextLineIndicator(0);
                            FUN_000bb2a8(0, 0);
                            resultVar = FUN_00027e78();
                            if ((resultVar != 1) && (resultVar != 5)) {
                                // p~u~yu px r q{|s
                                charaId = getCurrentCharaId();
                                addTextLogRec(charaId, scriptTrialText);
                                FUN_0002d778();
                            }
                            *scriptTrialFlags = *scriptTrialFlags & 0xfe;
                            *scriptTrialText = 0;
                        }
                    }
                }
                resultVar = 1;
            } else {
                resultVar = 1;
            }
        } else {
            resultVar = 1;
        }
    }
    return resultVar;
}

//------------------------------------------

char *currentTextLineGlobalVarBuffer = &newScriptTrialTextBuffer2;  // 0x8942a14;
unsigned char *scriptTrialFlagsMaybe = 0x89429e0;
unsigned char *DAT_089429e1 = 0x89429e1;
short *SHORT_089429e2 = 0x089429e2;
int (*FUN_0882c458)() = 0x882c458;
int (*FUN_0882c4a0)() = 0x882c4a0;

void (*startDrawScriptTrialTextLine)(float controlLogicPriority, float drawTextPriority, char *lineText, uint controlFlagsParamMaybe) = 0x8825430;

void *scriptCmdPrintDialogLine(void *str) {
    byte bVar1;
    uint uVar2;
    char *cmdEnd;
    // char textBuffer[258];
    short textLength;
    char auStack_4[2];
    char auStack_2[2];

    memcpy(&textLength, str, 2);
    memcpy(currentTextLineGlobalVarBuffer, str + 2, textLength);
    currentTextLineGlobalVarBuffer[textLength + 1] = 0;

    cmdEnd = str + 2 + textLength;

    stdPrintShiftJis(currentTextLineGlobalVarBuffer);

    if (((uint)*scriptTrialFlagsMaybe << 0x19) >> 0x1d == 0) {
        memcpy(auStack_2, cmdEnd + 1, 2);
        memcpy(auStack_4, cmdEnd + 3, 2);
    }
    uVar2 = ((uint)*scriptTrialFlagsMaybe << 0x19) >> 0x1d;
    if (uVar2 == 2) {
        bVar1 = 1;
    } else if (uVar2 == 1) {
        bVar1 = 0;
    } else {
        bVar1 = *DAT_089429e1 & 3;
    }

    if (bVar1 == 2) {
        *SHORT_089429e2 = *SHORT_089429e2 & 0xdfff;

        startDrawScriptTrialTextLine(1000.0, 3900.0, currentTextLineGlobalVarBuffer, ((uint)(byte)*SHORT_089429e2 << 0x1a) >> 0x1f);

        if ((int)((uint)(byte)*SHORT_089429e2 << 0x1a) < 0) {
            *SHORT_089429e2 = *SHORT_089429e2 & 0xffdf;
        }
        addFunctionToScene(1000.0, (SomeSceneStruct *)0x0, FUN_0882c458, 0, 0x10);
    } else if (bVar1 == 1) {
        if ((int)((uint)(unsigned char)*SHORT_089429e2 << 0x1d) < 0) {
            *SHORT_089429e2 = *SHORT_089429e2 & 0xdfff;
            startDrawScriptTrialTextLine(1000.0, 3900.0, currentTextLineGlobalVarBuffer, ((uint)(byte)*SHORT_089429e2 << 0x1a) >> 0x1f);
            if ((int)((uint)(byte)SHORT_089429e2 << 0x1a) < 0) {
                *SHORT_089429e2 = *SHORT_089429e2 & 0xffdf;
            }
            addFunctionToScene(1000.0, (SomeSceneStruct *)0x0, *FUN_0882c458, 0, 0x10);
        } else {
            addFunctionToScene(1000.0, (SomeSceneStruct *)0x0, *FUN_0882c4a0, 0, 0x10);
        }
    } else if (bVar1 == 0) {
        if ((int)((uint)(unsigned char)SHORT_089429e2 << 0x1e) < 0) {
            *SHORT_089429e2 = *SHORT_089429e2 & 0xdfff;
            startDrawScriptTrialTextLine(1000.0, 3900.0, currentTextLineGlobalVarBuffer, ((uint)(byte)SHORT_089429e2 << 0x1a) >> 0x1f);

            if ((int)((uint)(byte)*SHORT_089429e2 << 0x1a) < 0) {
                *SHORT_089429e2 = *SHORT_089429e2 & 0xffdf;
            }
            addFunctionToScene(1000.0, (SomeSceneStruct *)0x0, *FUN_0882c458, 0, 0x10);
        } else {
            addFunctionToScene(1000.0, (SomeSceneStruct *)0x0, *FUN_0882c4a0, 0, 0x10);
        }
    }
    *SHORT_089429e2 = *SHORT_089429e2 & 0xfe7fU | 0x80;

    return cmdEnd;
}

int drawScriptTrialText(void *ptr) {
    int (*origDrawText)(void *) = 0x88253a8;

    int result = origDrawText(ptr);

    return result;
}

//------------------------------------------------------------
char *curScriptLineVoiceMaybe = 0x08978044;

void addTextLogRec(int charaId, char *text) {
    TextLogStructure **ptr = 0x8978040;
    TextLogStructure *textLogStructPtr = *ptr;
    //=======

    short counter;
    char **charName;

    // stdPrintf("addTextLogRec\n textLogStructPtr = 0x%x\n", textLogStructPtr);

    if (textLogStructPtr != (TextLogStructure *)0x0) {
        counter = textLogStructPtr->recCounter + 1;
        textLogStructPtr->recCounter = counter;
        if (30 < counter) {
            textLogStructPtr->recCounter = 30;
        }
        if (charaId == 0) {
            charName = (char **)0x0;
        } else {
            charName = charaNames[charaId];
        }
        if ((textLogStructPtr->curSlot + 1) % 30 == (int)textLogStructPtr->wrappedCounter) {
            counter = textLogStructPtr->wrappedCounter + 1;
            textLogStructPtr->wrappedCounter = counter;
            textLogStructPtr->wrappedCounter = counter % 30;
        }
        if (charName != (char **)0x0) {
            strcpy(textLogStructPtr->logRecs[textLogStructPtr->curSlot].charName, (char *)charName);
        }

        uint textLength = strlen(text);
        uint tgtSize = 158;//sizeof(textLogStructPtr->logRecs[textLogStructPtr->curSlot].text);
        if (textLength >= tgtSize) {
            textLength = tgtSize-1;
        }

        // stdPrintf("tgtSize = %d\n", tgtSize);

        // strcpy(textLogStructPtr->logRecs[textLogStructPtr->curSlot].text, text);
        memcpy(textLogStructPtr->logRecs[textLogStructPtr->curSlot].text, text, textLength);
        textLogStructPtr->logRecs[textLogStructPtr->curSlot].text[textLength-1] = 0;

        strcpy(textLogStructPtr->logRecs[textLogStructPtr->curSlot].voice, curScriptLineVoiceMaybe);
        counter = textLogStructPtr->curSlot + 1;
        textLogStructPtr->curSlot = counter;
        textLogStructPtr->curSlot = counter % 30;
    }
    return;
}