#ifndef H_UTILS
#define H_UTILS

float getTextWidth(char *text, float size);
void drawTextMy(char *str, float xPos, float yPos, float param_4, float sizeX, float sizeY, float xSpacing, float ySpacing);
void debugPrint(float x, float y, char *format, ...);
void debugPrintSize(float x, float y, float size, char *format, ...);

short getCharLen(char *str);

void stdPrintShiftJis(char *str);
void printFrameFunctions();

#endif