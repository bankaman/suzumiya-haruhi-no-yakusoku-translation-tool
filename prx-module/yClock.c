
#include <pspctrl.h>
#include <psputils.h>

#include "defines.h"
#include "imports.h"

void (*drawFog)(void) = CONVERT_ADDR(0x823a4);
void (*setTexturePos)(DisplayTextureStruct *texture, float xPos, float yPos) = CONVERT_ADDR(0xa1a80);
void (*cropTextureView)(DisplayTextureStruct *texture, float x, float y, float width, float height) = CONVERT_ADDR(0xa1c54);

void (*drawAnimationMaybe3p)(DisplayTextureStruct *texture, int param2, byte *param3) = CONVERT_ADDR(0x25798);

int *yClockShowMenuFlag = CONVERT_ADDR(0x1b16e0);
DisplayTextureStruct *yClockTimerStartButtonBg = CONVERT_ADDR(0x1b1718);
DisplayTextureStruct *yClockBirthdayButtonBg = CONVERT_ADDR(0x1b1774);
DisplayTextureStruct *yClockAlarmButtonBg = CONVERT_ADDR(0x1b17d0);
DisplayTextureStruct *yClockBackButtonBg = CONVERT_ADDR(0x1b182c);
DisplayTextureStruct *yClockDatePickerCursorTexture = CONVERT_ADDR(0x1b1ab0);

char *timerStartString = CONVERT_ADDR(0x13a7d8);
short *yukiClockMenuXpos = CONVERT_ADDR(0x1b1f30);
int *someYClockState = CONVERT_ADDR(0x1b1f90);

bool *yClockDatePickCursorPos = CONVERT_ADDR(0x1b1f8d);
int *yClockDatePickerCursorRelated = CONVERT_ADDR(0x1b16fc);
byte *yClockDatePickCurAnimStruct = CONVERT_ADDR(0x1b1700);
DisplayTextureStruct *yClockSetDateBg = CONVERT_ADDR(0x1b1888);
DisplayTextureStruct *yClockPickerDigit1 = CONVERT_ADDR(0x1b18e4);
DisplayTextureStruct *yClockPickerDigit2 = CONVERT_ADDR(0x1b1940);
DisplayTextureStruct *yClockPickerDigit3 = CONVERT_ADDR(0x1b19f8);
DisplayTextureStruct *yClockPickerDelimTexture = CONVERT_ADDR(0x1b199c);

DisplayTextureStruct *yClockPickerDigit4 = CONVERT_ADDR(0x1b1a54);
byte *yClockPickerVal2 = CONVERT_ADDR(0x1b16ed);
byte *yClockPickerVal1 = CONVERT_ADDR(0x1b16ec);

float *digitOffsets = CONVERT_ADDR(0xe4aac);

void drawTextMy(char *str, float xPos, float yPos, float param_4, float sizeX, float sizeY, float param_7, float param_8);

void debugPrint();

int drawYukiClockMenu(void) {
    byte pickerVal1;
    byte pickerVal2;
    int dontRemove;

    if (*yClockShowMenuFlag == 0) {
        dontRemove = 0;
    } else {
        drawFog();

        drawTexture(yClockTimerStartButtonBg);
        drawTexture(yClockBirthdayButtonBg);
        drawTexture(yClockAlarmButtonBg);
        drawTexture(yClockBackButtonBg);

        setTextColor(0xffffffff);
        drawText("�H�p���������y���� ���p����", *yukiClockMenuXpos + 35, 29.0, 0.0, 0.7, 0.7, 0.0, 0.0);
        drawText("�D�u�~�� �����w�t�u�~�y��", *yukiClockMenuXpos + 35, 65.0, 0.0, 0.7, 0.7, 0.0, 0.0);
        drawText("�A���t�y�|���~�y�{", *yukiClockMenuXpos + 35, 101.0, 0.0, 0.7, 0.7, 0.0, 0.0);
        drawText("�B �}�u�~��", *yukiClockMenuXpos + 35, 137.0, 0.0, 0.7, 0.7, 0.0, 0.0);

        // is we setting birthday or setting alarm
        if ((*someYClockState == 8) || (*someYClockState == 7)) {
            if (*yClockDatePickCursorPos == false) {
                setTexturePos(yClockDatePickerCursorTexture, 268.0, 123.0);
            } else {
                setTexturePos(yClockDatePickerCursorTexture, 304.0, 123.0);
            }
            pickerVal2 = *yClockPickerVal2;
            pickerVal1 = *yClockPickerVal1;
            cropTextureView(yClockPickerDigit1, digitOffsets[pickerVal1 / 10], 0.0, 0.09375, 0.5);
            cropTextureView(yClockPickerDigit2, digitOffsets[pickerVal1 % 10], 0.0, 0.09375, 0.5);
            cropTextureView(yClockPickerDigit3, digitOffsets[pickerVal2 / 10], 0.0, 0.09375, 0.5);
            cropTextureView(yClockPickerDigit4, digitOffsets[pickerVal2 % 10], 0.0, 0.09375, 0.5);
            drawTexture(yClockSetDateBg);
            drawTexture(yClockPickerDigit1);
            drawTexture(yClockPickerDigit2);
            drawTexture(yClockPickerDelimTexture);
            drawTexture(yClockPickerDigit3);
            drawTexture(yClockPickerDigit4);

            drawAnimationMaybe3p(yClockDatePickerCursorTexture, *yClockDatePickerCursorRelated, yClockDatePickCurAnimStruct);
        }
        if (*someYClockState == 8) {
            drawText("�B���u�}�� �q���t�y�|���~�y�{�p", 134.0, 129.0, 0.0, 0.7, 0.7, 0.0, 0.0);
        } else if (*someYClockState == 7) {
            drawText("�S�r���z �t�u�~�� �����w�t�u�~�y��", 124.0, 129.0, 0.0, 0.7, 0.7, 0.0, 0.0);
        }
        dontRemove = 1;
    }
    return dontRemove;
}

int *menuChangeCounter = CONVERT_ADDR(0x1b16f8);
byte *yClockMenuSelectedButton = CONVERT_ADDR(0x1b1f8c);
void (*playMenuButtonSound)(MenuButtonSound mode) = CONVERT_ADDR(0xd3f40);
void (*yClockAlarmSetDirty)() = CONVERT_ADDR(0xd111c);
void (*addTextbox)(char *line1, char *line2, char *line3, char *line4, char *line5) = CONVERT_ADDR(0xc43f4);
uint *contorllerRelatedVar2 = CONVERT_ADDR(0x162364);
int *DAT_001b1f98 = CONVERT_ADDR(0x1b1f98);
byte *yClockAlarmHour = CONVERT_ADDR(0x1b16e6);
byte *yClockAlarmMinute = CONVERT_ADDR(0x1b16e7);
byte *yClockBirthdayMonth = CONVERT_ADDR(0x1b16e4);
byte *yClockBirthdayDay = CONVERT_ADDR(0x1b16e5);
byte *yClockSavedAlarmHour = CONVERT_ADDR(0x1b16ea);
byte *yClockSavedAlarmMinute = CONVERT_ADDR(0x1b16eb);
short *yClockChangeStateDelayMaybe = CONVERT_ADDR(0x1b1f94);
short *SHORT_001b1f96 = CONVERT_ADDR(0x1b1f96);

int yClockPushMenuButtonLogic(void) {
    uint keyPress;
    int newState;
    int *piVar1;
    int iVar2;

    newState = 6;
    if ((*menuChangeCounter < 3) || (keyPress = readController(PRESS_ONCE), (keyPress & PSP_CTRL_UP) == 0)) {
        if ((2 < *menuChangeCounter) &&
            (keyPress = readController(PRESS_ONCE), (keyPress & PSP_CTRL_DOWN) != 0)) {
            playMenuButtonSound(MENU_CHANGE);
            *yClockMenuSelectedButton = *yClockMenuSelectedButton + 1;
            if (3 < *yClockMenuSelectedButton) {
                *yClockMenuSelectedButton = 0;
            }
            *menuChangeCounter = 0;
        }
    } else {
        playMenuButtonSound(MENU_CHANGE);
        *yClockMenuSelectedButton = *yClockMenuSelectedButton - 1;
        if (*yClockMenuSelectedButton < 0) {
            *yClockMenuSelectedButton = 3;
        }
        *menuChangeCounter = 0;
    }
    if ((2 < *menuChangeCounter) &&
        (keyPress = readController(PRESS_ONCE), (keyPress & PSP_CTRL_CIRCLE) != 0)) {
        playMenuButtonSound(MENU_SELECT);
        if (*yClockMenuSelectedButton == 3) {
            *DAT_001b1f98 = 1;
            newState = 9;
        } else if (*yClockMenuSelectedButton == 2) {
            *yClockPickerVal1 = *yClockAlarmHour;
            *yClockPickerVal2 = *yClockAlarmMinute;
            cropTextureView(yClockPickerDelimTexture, 0.09375, 0.5, 0.09375, 0.5);
            *yClockDatePickCursorPos = false;
            newState = 8;
        } else if (*yClockMenuSelectedButton == 1) {
            *yClockPickerVal1 = *yClockBirthdayMonth;
            *yClockPickerVal2 = *yClockBirthdayDay;
            cropTextureView(yClockPickerDelimTexture, 0.0, 0.5, 0.09375, 0.5);
            *yClockDatePickCursorPos = false;
            newState = 7;
        } else if (*yClockMenuSelectedButton == 0) {
            if ((yClockAlarmHour != yClockSavedAlarmHour) || (yClockAlarmMinute != yClockSavedAlarmMinute)) {
                yClockAlarmSetDirty();
            }
            addTextbox(NULL,
                       "�H�p�������{ ���p�z�}�u���p",
                       "�P���w�p�|���z�����p �����r�u���~�y���u",
                       "�����������z�����r�� ���� ���p�����r���z",
                       "�������u�|�{�u.");
            *yClockChangeStateDelayMaybe = 180;
            *SHORT_001b1f96 = 0;
            newState = 5;
        }
        *menuChangeCounter = 0;
    }
    iVar2 = 0;
    piVar1 = yClockShowMenuFlag;
    do {
        cropTextureView((DisplayTextureStruct *)(piVar1 + 0xe), 0.0, 0.0, 1.0, 0.5);
        iVar2 = iVar2 + 1;
        piVar1 = piVar1 + 0x17;
    } while (iVar2 < 4);
    cropTextureView(yClockTimerStartButtonBg + *yClockMenuSelectedButton, 0.0, 0.5, 1.0, 0.5);
    return newState;
}
