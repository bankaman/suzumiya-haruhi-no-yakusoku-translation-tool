#ifndef H_IMPORTS
#define H_IMPORTS

#include "defines.h"

void (*drawText)(char *str, float xPos, float yPos, float param_4, float sizeX, float sizeY, float param_7, float param_8);
void (*drawTexture)(DisplayTextureStruct *texture);
void (*setTextColor)(uint color);
void (*getCurFontParams)(uint *param_1, uint *param_2, int *fontVertHeight1, int *param_4, int *param_5, int *fontVertHeight2);
uint (*readController)(ControllerReadMode mode);

void (*stdPrintf)(char *, ...);

//void (*memcpu)(void *dst, void *src, uint count);

int (*addFunctionToScene)(float priority, SomeSceneStruct *sceneStruct, void *functionPtr, int param_4, int param_5);

#endif