#ifndef H_DEFINES
#define H_DEFINES

#define OFFSET 0x8804000

#define CONVERT_ADDR(addr) addr + OFFSET

#define false 0
#define true 1

typedef unsigned int uint;
typedef char byte;
typedef byte bool;
typedef struct DisplayTextureStruct DisplayTextureStruct, *PDisplayTextureStruct;
typedef struct DisplayGeometryStruct DisplayGeometryStruct, *PDisplayGeometryStruct;

struct DisplayGeometryStruct {
    int field0_0x0;
    int field1_0x4;
    int field2_0x8;
    float field3_0xc;
    float field4_0x10;
    int field5_0x14;
    int field6_0x18;
    int field7_0x1c;
    int field8_0x20;
    float field9_0x24;
    float field10_0x28;
    int field11_0x2c;
    int field12_0x30;
    int field13_0x34;
    int field14_0x38;
    float field15_0x3c;
    float field16_0x40;
    int field17_0x44;
    int field18_0x48;
    int field19_0x4c;
    int field20_0x50;
    float field21_0x54;
    float field22_0x58;
    int field23_0x5c;
};

struct DisplayTextureStruct {
    struct DisplayGeometryStruct *geometryPtr;
    uint flags;
    float xPos;
    float yPos;
    float field4_0x10;
    float width;
    float height;
    float field7_0x1c;
    float field8_0x20;
    float field9_0x24;
    int field10_0x28;
    int field11_0x2c;
    int field12_0x30;
    int field13_0x34;
    int field14_0x38;
    float p1x;
    float p1y;
    float p2x;
    float p2y;
    float p3x;
    float p3y;
    float p4x;
    float p4y;
};

typedef struct SceneStruct SceneStruct, *PSceneStruct;
struct SceneStruct {
    char *sceneName;
    char *sceneNameMaybe;
    int (*sceneFunc)(int);
};

typedef enum ControllerReadMode {
    PRESSED = 0,
    PRESS_ONCE = 1,
    PRESS_ONCE_REPEAT = 2
} ControllerReadMode;

typedef enum MenuButtonSound {
    MENU_CHANGE = 0,
    MENU_SELECT = 1,
    MENU_RETURN = 2
} MenuButtonSound;

typedef struct TextBoxStruct TextBoxStruct, *PTextBoxStruct;
struct TextBoxStruct {
    bool someFlag;
    bool someFlag2;
    bool someFlag3;
    bool someFlag4;
    bool someFlag5;
    byte field5_0x5;
    short kWindowGimId;
    short kWindowSelectGimId;
    short field8_0xa;
    float lineOffsets[5];
    char lines[5][64];
};

typedef struct GlyphLeftTop GlyphLeftTop, *PGlyphLeftTop;

struct GlyphLeftTop {
    int bitmapLeft;
    int bitmapTop;
};

typedef struct GlyphInfo2 GlyphInfo2, *PGlyphInfo2;

struct GlyphInfo2 {
    int glyphWidthMaybe;
    int glyphHeightMaybe;
    int widthRelated;
    int field3_0xc;
};

/*typedef enum PspCtrlButtons {
    PSP_CTRL_SELECT=1,
    PSP_CTRL_START=8,
    PSP_CTRL_UP=16,
    PSP_CTRL_RIGHT=32,
    PSP_CTRL_DOWN=64,
    PSP_CTRL_LEFT=128,
    PSP_CTRL_LTRIGGER=256,
    PSP_CTRL_RTRIGGER=512,
    PSP_CTRL_TRIANGLE=4096,
    PSP_CTRL_CIRCLE=8192,
    PSP_CTRL_CROSS=16384,
    PSP_CTRL_SQUARE=32768,
    PSP_CTRL_HOME=65536,
    PSP_CTRL_HOLD=131072,
    PSP_CTRL_WLAN_UP=262144,
    PSP_CTRL_REMOTE=524288,
    PSP_CTRL_VOLUP=1048576,
    PSP_CTRL_VOLDOWN=2097152,
    PSP_CTRL_SCREEN=4194304,
    PSP_CTRL_NOTE=8388608,
    PSP_CTRL_DISC=16777216,
    PSP_CTRL_MS=33554432
} PspCtrlButtons;*/

typedef struct SomeSceneStruct SomeSceneStruct, *PSomeSceneStruct;

typedef struct SceneFuncStorageStruct SceneFuncStorageStruct, *PSceneFuncStorageStruct;

struct SomeSceneStruct {
    int val1;
    int val2;
    int val3;
    struct SceneFuncStorageStruct *compFunctionsMaybe;
};

typedef unsigned char undefined;
struct SceneFuncStorageStruct {
    undefined field0_0x0;
    undefined field1_0x1;
    undefined field2_0x2;
    undefined field3_0x3;
    undefined field4_0x4;
    undefined field5_0x5;
    undefined field6_0x6;
    undefined field7_0x7;
    float priority;
};

//------------------------------------------------------

typedef struct GlobParamsStruct GlobParamsStruct, *PGlobParamsStruct;

typedef struct FrameFunctionsStruct FrameFunctionsStruct, *PFrameFunctionsStruct;

typedef struct MemManagementStruct MemManagementStruct, *PMemManagementStruct;

typedef struct FrameFunctionStruct FrameFunctionStruct, *PFrameFunctionStruct;

struct MemManagementStruct {
    void * (*align_alocPtr)(uint, uint);
    void * (*mallocPtr)(uint);
    void (*freePtr)(void *);
};

struct FrameFunctionsStruct {
    void *field0_0x0;
    uint someFlag;
    void *field2_0x8;
    struct FrameFunctionStruct *firstFunction;
};

struct FrameFunctionStruct {
    struct FrameFunctionStruct *nextFunc;
    uint someFlags;
    float priority;
    int (*funcPtr)(void *);
    void *memPtr; /* Created by retype action */
};

struct GlobParamsStruct {
    struct FrameFunctionsStruct *frameFunctions;
    int field1_0x4;
    undefined field2_0x8;
    undefined field3_0x9;
    undefined field4_0xa;
    undefined field5_0xb;
    undefined field6_0xc;
    undefined field7_0xd;
    undefined field8_0xe;
    undefined field9_0xf;
    undefined field10_0x10;
    undefined field11_0x11;
    undefined field12_0x12;
    undefined field13_0x13;
    undefined field14_0x14;
    undefined field15_0x15;
    undefined field16_0x16;
    undefined field17_0x17;
    undefined field18_0x18;
    undefined field19_0x19;
    undefined field20_0x1a;
    undefined field21_0x1b;
    undefined field22_0x1c;
    undefined field23_0x1d;
    undefined field24_0x1e;
    undefined field25_0x1f;
    undefined field26_0x20;
    undefined field27_0x21;
    undefined field28_0x22;
    undefined field29_0x23;
    undefined field30_0x24;
    undefined field31_0x25;
    undefined field32_0x26;
    undefined field33_0x27;
    undefined field34_0x28;
    undefined field35_0x29;
    undefined field36_0x2a;
    undefined field37_0x2b;
    undefined field38_0x2c;
    undefined field39_0x2d;
    undefined field40_0x2e;
    undefined field41_0x2f;
    struct MemManagementStruct memManagement;
    uint someFlag;
    uint field44_0x40;
};

//--------------------------------------------------------------

typedef struct TextLogStructure TextLogStructure, *PTextLogStructure;

typedef struct LogRecStruct LogRecStruct, *PLogRecStruct;

struct LogRecStruct {
    char charName[11];
    char voice[21];
    char text[168];
};

struct TextLogStructure {
    undefined field0_0x0;
    undefined field1_0x1;
    short wrappedCounter;
    short curSlot;
    short recCounter;
    undefined field5_0x8;
    undefined field6_0x9;
    undefined field7_0xa;
    undefined field8_0xb;
    struct LogRecStruct logRecs[30];
    int field10_0x177c;
    undefined field11_0x1780;
    undefined field12_0x1781;
    undefined field13_0x1782;
    undefined field14_0x1783;
    undefined field15_0x1784;
    undefined field16_0x1785;
    undefined field17_0x1786;
    undefined field18_0x1787;
    undefined field19_0x1788;
    undefined field20_0x1789;
    undefined field21_0x178a;
    undefined field22_0x178b;
    undefined field23_0x178c;
    undefined field24_0x178d;
    undefined field25_0x178e;
    undefined field26_0x178f;
    undefined field27_0x1790;
    undefined field28_0x1791;
    undefined field29_0x1792;
    undefined field30_0x1793;
    undefined field31_0x1794;
    undefined field32_0x1795;
    undefined field33_0x1796;
    undefined field34_0x1797;
    undefined field35_0x1798;
    undefined field36_0x1799;
    undefined field37_0x179a;
    undefined field38_0x179b;
    undefined field39_0x179c;
    undefined field40_0x179d;
    undefined field41_0x179e;
    undefined field42_0x179f;
    undefined field43_0x17a0;
    undefined field44_0x17a1;
    undefined field45_0x17a2;
    undefined field46_0x17a3;
    undefined field47_0x17a4;
    undefined field48_0x17a5;
    undefined field49_0x17a6;
    undefined field50_0x17a7;
    undefined field51_0x17a8;
    undefined field52_0x17a9;
    undefined field53_0x17aa;
    undefined field54_0x17ab;
    undefined field55_0x17ac;
    undefined field56_0x17ad;
    undefined field57_0x17ae;
    undefined field58_0x17af;
    undefined field59_0x17b0;
    undefined field60_0x17b1;
    undefined field61_0x17b2;
    undefined field62_0x17b3;
    undefined field63_0x17b4;
    undefined field64_0x17b5;
    undefined field65_0x17b6;
    undefined field66_0x17b7;
    undefined field67_0x17b8;
    undefined field68_0x17b9;
    undefined field69_0x17ba;
    undefined field70_0x17bb;
    undefined field71_0x17bc;
    undefined field72_0x17bd;
    undefined field73_0x17be;
    undefined field74_0x17bf;
    undefined field75_0x17c0;
    undefined field76_0x17c1;
    undefined field77_0x17c2;
    undefined field78_0x17c3;
    undefined field79_0x17c4;
    undefined field80_0x17c5;
    undefined field81_0x17c6;
    undefined field82_0x17c7;
    undefined field83_0x17c8;
    undefined field84_0x17c9;
    undefined field85_0x17ca;
    undefined field86_0x17cb;
    undefined field87_0x17cc;
    undefined field88_0x17cd;
    undefined field89_0x17ce;
    undefined field90_0x17cf;
    undefined field91_0x17d0;
    undefined field92_0x17d1;
    undefined field93_0x17d2;
    undefined field94_0x17d3;
    undefined field95_0x17d4;
    undefined field96_0x17d5;
    undefined field97_0x17d6;
    undefined field98_0x17d7;
    undefined field99_0x17d8;
    undefined field100_0x17d9;
    undefined field101_0x17da;
    undefined field102_0x17db;
    undefined field103_0x17dc;
    undefined field104_0x17dd;
    undefined field105_0x17de;
    undefined field106_0x17df;
    undefined field107_0x17e0;
    undefined field108_0x17e1;
    undefined field109_0x17e2;
    undefined field110_0x17e3;
    undefined field111_0x17e4;
    undefined field112_0x17e5;
    undefined field113_0x17e6;
    undefined field114_0x17e7;
    undefined field115_0x17e8;
    undefined field116_0x17e9;
    undefined field117_0x17ea;
    undefined field118_0x17eb;
    undefined field119_0x17ec;
    undefined field120_0x17ed;
    undefined field121_0x17ee;
    undefined field122_0x17ef;
    undefined field123_0x17f0;
    undefined field124_0x17f1;
    undefined field125_0x17f2;
    undefined field126_0x17f3;
    undefined field127_0x17f4;
    undefined field128_0x17f5;
    undefined field129_0x17f6;
    undefined field130_0x17f7;
    undefined field131_0x17f8;
    undefined field132_0x17f9;
    undefined field133_0x17fa;
    undefined field134_0x17fb;
    undefined field135_0x17fc;
    undefined field136_0x17fd;
    undefined field137_0x17fe;
    undefined field138_0x17ff;
    undefined field139_0x1800;
    undefined field140_0x1801;
    undefined field141_0x1802;
    undefined field142_0x1803;
    undefined field143_0x1804;
    undefined field144_0x1805;
    undefined field145_0x1806;
    undefined field146_0x1807;
    undefined field147_0x1808;
    undefined field148_0x1809;
    undefined field149_0x180a;
    undefined field150_0x180b;
    undefined field151_0x180c;
    undefined field152_0x180d;
    undefined field153_0x180e;
    undefined field154_0x180f;
    undefined field155_0x1810;
    undefined field156_0x1811;
    undefined field157_0x1812;
    undefined field158_0x1813;
    undefined field159_0x1814;
    undefined field160_0x1815;
    undefined field161_0x1816;
    undefined field162_0x1817;
    undefined field163_0x1818;
    undefined field164_0x1819;
    undefined field165_0x181a;
    undefined field166_0x181b;
    undefined field167_0x181c;
    undefined field168_0x181d;
    undefined field169_0x181e;
    undefined field170_0x181f;
    undefined field171_0x1820;
    undefined field172_0x1821;
    undefined field173_0x1822;
    undefined field174_0x1823;
    undefined field175_0x1824;
    undefined field176_0x1825;
    undefined field177_0x1826;
    undefined field178_0x1827;
    undefined field179_0x1828;
    undefined field180_0x1829;
    undefined field181_0x182a;
    undefined field182_0x182b;
    undefined field183_0x182c;
    undefined field184_0x182d;
    undefined field185_0x182e;
    undefined field186_0x182f;
    undefined field187_0x1830;
    undefined field188_0x1831;
    undefined field189_0x1832;
    undefined field190_0x1833;
    undefined field191_0x1834;
    undefined field192_0x1835;
    undefined field193_0x1836;
    undefined field194_0x1837;
    undefined field195_0x1838;
    undefined field196_0x1839;
    undefined field197_0x183a;
    undefined field198_0x183b;
    undefined field199_0x183c;
    undefined field200_0x183d;
    undefined field201_0x183e;
    undefined field202_0x183f;
};

#endif