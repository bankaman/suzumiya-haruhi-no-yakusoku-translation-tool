#include "imports.h"

void (*drawText)(char *str, float xPos, float yPos, float param_4, float sizeX, float sizeY, float param_7, float param_8) = CONVERT_ADDR(0xa08b4);
void (*drawTexture)(DisplayTextureStruct *texture) = CONVERT_ADDR(0xa19f4);
void (*setTextColor)(uint color) = CONVERT_ADDR(0xcc200);
void (*getCurFontParams)(uint *param_1, uint *param_2, int *fontVertHeight1, int *param_4, int *param_5, int *fontVertHeight2) = CONVERT_ADDR(0xa0bd0);
uint (*readController)(ControllerReadMode mode) = CONVERT_ADDR(0xa1584);

void (*stdPrintf)(char *str, ...) = 0x8855610;
void (*memcpu)(void *dst, void *src, uint count) = 0x88101f8;

int (*addFunctionToScene)(float priority, SomeSceneStruct *sceneStruct, void *functionPtr, int param_4, int param_5) = 0x88a2908;