/*
 * Suzumiya Haruhi no Yakusoku patch module
 * Tomilin Dmitriy (bankastudio@gmail.com) 31.01.2024
 */
#include <pspkernel.h>

#include "defines.h"
#include "imports.h"
#include "injectStructs.h"
#include "macros.h"
#include "scriptTrial.h"
#include "utils.h"

PSP_MODULE_INFO("Haruhi Patch", 0x1000, 1, 1);

int drawYukiClockMenu();
int yClockPushMenuButtonLogic();
void textBoxPrepareText();

#define SCENE_YUKI_CLOCK CONVERT_ADDR(0x139e78)
#define SCENE_TITLE CONVERT_ADDR(0x1391e8)
#define SCENE_POKER_TRIAL CONVERT_ADDR(0xf29e0)

InjectionI16 I16_INJECTIONS[] = {
    // {.addrHI = 0x1e138, .addrLO = 0x1e13c, .target = SCENE_YUKI_CLOCK},  // first scene to yukiClock
    // {.addrHI = 0x1e138, .addrLO = 0x1e13c, .target = SCENE_TITLE},  // first scene to SCENE_TITLE
    // {.addrHI = 0x1e138, .addrLO = 0x1e13c, .target = SCENE_POKER_TRIAL},  // first scene to SCENE_POKER_TRIAL
    {.addrHI = 0xd25d4, .addrLO = 0xd25e0, .target = &drawYukiClockMenu},
    {.addrHI = 0x1f9e0, .addrLO = 0x1f9e8, .target = &drawCharacterName},
    {.addrHI = 0x2146c, .addrLO = 0x21478, .target = &scriptTrialControlLogic}};

InjectionI16 I16_INJECTIONS_NO_CONVERT[] = {
    {.addrHI = 0x88250d0, .addrLO = 0x88250d8, .target = &newScriptTrialTextBuffer},
    {.addrHI = 0x8825188, .addrLO = 0x8825190, .target = &newScriptTrialTextBuffer},
    {.addrHI = 0x882524c, .addrLO = 0x8825254, .target = &newScriptTrialTextBuffer},
    {.addrHI = 0x8825340, .addrLO = 0x8825348, .target = &newScriptTrialTextBuffer},
    {.addrHI = 0x8825408, .addrLO = 0x882541c, .target = &newScriptTrialTextBuffer},
    {.addrHI = 0x8824dec, .addrLO = 0x8824df8, .target = &newScriptTrialTextBuffer},
    {.addrHI = 0x8824fb4, .addrLO = 0x8824fc0, .target = &newScriptTrialTextBuffer},
    {.addrHI = 0x8825058, .addrLO = 0x8825064, .target = &newScriptTrialTextBuffer},

    {.addrHI = 0x882c4f8, .addrLO = 0x882c4fc, .target = &newScriptTrialTextBuffer2},
    {.addrHI = 0x882c5d4, .addrLO = 0x882c5d8, .target = &newScriptTrialTextBuffer2},
    {.addrHI = 0x882c6a4, .addrLO = 0x882c6a8, .target = &newScriptTrialTextBuffer2},
    {.addrHI = 0x882c7a0, .addrLO = 0x882c7a4, .target = &newScriptTrialTextBuffer2},
    {.addrHI = 0x882c880, .addrLO = 0x882c884, .target = &newScriptTrialTextBuffer2},
    {.addrHI = 0x882c950, .addrLO = 0x882c954, .target = &newScriptTrialTextBuffer2},
    {.addrHI = 0x882b2e8, .addrLO = 0x882b2ec, .target = &newScriptTrialTextBuffer2},
    
    {.addrHI = 0x8825488, .addrLO = 0x8825494, .target = &drawScriptTrialText},
};

InjectionJal JAL_INJECTIONS[] = {
    {.addrJal = 0xd1894, .target = &yClockPushMenuButtonLogic},
    {.addrJal = 0xc4200, .target = &textBoxPrepareText},
    {.addrJal = 0xc4424, .target = &textBoxPrepareText},
    {.addrJal = 0xc44ac, .target = &textBoxPrepareText}};

InjectionFull FULL_INJECT[] = {
    {.addr = 0x88dade8, .target = scriptCmdPrintDialogLine}};

int main(int argc, char **arg) {
    stdPrintf("**********************************************\n");
    stdPrintf("** Suzumiya Haruhi no Yakusoku patch module **\n");
    stdPrintf("**    by bankaman (bankastudio@gmail.com)   **\n");
    stdPrintf("**********************************************\n");
    stdPrintf("Injecting patches... ");

    foreach (InjectionI16 *inject, I16_INJECTIONS) {
        uint16_t *hi = CONVERT_ADDR(inject->addrHI);
        uint16_t *lo = CONVERT_ADDR(inject->addrLO);
        uint16_t valHi = (inject->target >> 16) & 0xFFFF;
        uint16_t valLo = (inject->target) & 0xFFFF;
        if ((valLo & 0x8000) > 0) {
            valHi++;
        }
        *hi = valHi;
        *lo = valLo;
    }

    foreach (InjectionI16 *inject, I16_INJECTIONS_NO_CONVERT) {
        uint16_t *hi = inject->addrHI;
        uint16_t *lo = inject->addrLO;
        uint16_t valHi = (inject->target >> 16) & 0xFFFF;
        uint16_t valLo = (inject->target) & 0xFFFF;
        if ((valLo & 0x8000) > 0) {
            valHi++;
        }
        *hi = valHi;
        *lo = valLo;
    }

    foreach (InjectionJal *inject, JAL_INJECTIONS) {
        uint *jal = CONVERT_ADDR(inject->addrJal);
        *jal = (0b11 << 26) | ((inject->target >> 2) & 0x3FFFFFF);
    }

    foreach (InjectionFull *inject, FULL_INJECT) {
        uint *addres = inject->addr;
        *addres = inject->target;
    }

    stdPrintf("done\n");

    stdPrintf("stdPrintShiftJis addr = 0x%x\n", &stdPrintShiftJis);
    stdPrintf("scriptCmdPrintDialogLine addr = 0x%x\n", &scriptCmdPrintDialogLine);
    stdPrintf("drawScriptTrialText addr = 0x%x\n", &drawScriptTrialText);
    stdPrintf("printFrameFunctions addr = 0x%x\n", &printFrameFunctions);
    stdPrintf("newScriptTrialTextBuffer addr = 0x%x\n", &newScriptTrialTextBuffer);
    stdPrintf("newScriptTrialTextBuffer2 addr = 0x%x\n", &newScriptTrialTextBuffer2);


    return 0;
}