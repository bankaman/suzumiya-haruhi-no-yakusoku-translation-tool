#include <string.h>

#include "defines.h"
#include "utils.h"

TextBoxStruct *textBoxStruct = CONVERT_ADDR(0x1a4d00);

void textBoxPrepareText(char *line1, char *line2, char *line3, char *line4, char *line5) {
    if (line1 == NULL) {
        textBoxStruct->lines[0][0] = '\0';
    } else {
        strcpy(textBoxStruct->lines[0], line1);
    }
    if (line2 == NULL) {
        textBoxStruct->lines[1][0] = '\0';
    } else {
        strcpy(textBoxStruct->lines[1], line2);
    }
    if (line3 == NULL) {
        textBoxStruct->lines[2][0] = '\0';
    } else {
        strcpy(textBoxStruct->lines[2], line3);
    }
    if (line4 == NULL) {
        textBoxStruct->lines[3][0] = '\0';
    } else {
        strcpy(textBoxStruct->lines[3], line4);
    }
    if (line5 == NULL) {
        textBoxStruct->lines[4][0] = '\0';
    } else {
        strcpy(textBoxStruct->lines[4], line5);
    }

    uint length;
    for (int i = 0; i < 5; i++) {
        char *str = textBoxStruct->lines[i];
        length = strlen(str);
        // textBoxStruct->lineOffsets[i] = (480.0 - length / 2 * 14.5) / 2.0 + 2.0;
        float lineWidth = getTextWidth(str, 0.8);
        textBoxStruct->lineOffsets[i] = (480.0 / 2 - lineWidth / 2);
    }
}