#include "utils.h"

#include <string.h>

#include "defines.h"
#include "imports.h"
#include "shiftjis.h"
#include "stdarg.h"

//---------------------
void (*FUN_000a072c)() = CONVERT_ADDR(0xa072c);
void (*FUN_000cbe78)() = CONVERT_ADDR(0xcbe78);
void (*FUN_000cc758)() = CONVERT_ADDR(0xcc758);
short (*getMultibyteChar)(char **curCharPtr) = CONVERT_ADDR(0x1b7c8);
uint (*convertShiftJisToFontIdMaybe)(short param_1, uint param_2) = CONVERT_ADDR(0x1c308);
float (*getGlyphInfoAndImageMaybe)(int charCode, GlyphLeftTop *glyphInfoOut, GlyphInfo2 *glyphInfoOut2) = CONVERT_ADDR(0xa01b0);
void (*drawTextGlyphMaybe)(int param_1, int param_2, int param_3, short param_4, short param_5, short param_6,
                           int param_7) = CONVERT_ADDR(0xcbe94);

void (*getCharInfo)(uint charCode, uint *bitmapLeft, int *bitmapTop, uint *bitmapWidth, uint *bitmapHeight,
                    int *sfp26Ascender, int *sfp26Descender, int *sfp26AdvanceH) = CONVERT_ADDR(0xa0c5c);

int *INT_00151448 = CONVERT_ADDR(0x151448);
int *INT_00151444 = CONVERT_ADDR(0x151444);
int *INT_0015143c = CONVERT_ADDR(0x15143c);
char *isSpecialCharacter = CONVERT_ADDR(0xd6474);
void *fontGlyphsTexturePtr = CONVERT_ADDR(0x151440);
int *globFontSize1 = CONVERT_ADDR(0x151450);
int *globFontSize2 = CONVERT_ADDR(0x15144c);

char buffer[256];
void debugPrint(float x, float y, char *format, ...) {
    va_list argp;
    va_start(argp, format);
    debugPrintSize(x, y, 0.5, format);
    va_end(argp);
}
void debugPrintSize(float x, float y, float size, char *format, ...) {
    va_list argp;
    va_start(argp, format);
    sprintf(&buffer, format, argp);
    va_end(argp);

    drawTextMy(&buffer, x, y, 0.0, size, size, 0.0, 0.0);
}

void drawTextMy(char *str, float xPos, float yPos, float param_4, float sizeX, float sizeY, float xSpacing, float ySpacing) {
    short uVar1;
    short uVar2;
    uint currentCharUInt;
    float advanceX;
    float nextLineYPosMaybe;
    float xPosition;
    GlyphInfo2 glyphInfo2;
    GlyphLeftTop glyphInfo;
    int fontVertHeight2;
    int fontVertHeight1;
    char *currentChar;

    xPosition = xPos;
    currentChar = str;
    getCurFontParams((uint *)0x0, (uint *)0x0, &fontVertHeight1, (int *)0x0, (int *)0x0, &fontVertHeight2);
    FUN_000a072c();
    FUN_000cbe78(*INT_00151448, *INT_00151444, (int)((float)*INT_00151448 * sizeX),
                 (int)((float)*INT_00151444 * sizeY));
    currentCharUInt = (uint)(byte)*currentChar;
    nextLineYPosMaybe = yPos + ((float)fontVertHeight1 / 64.0) * sizeY;
    if (currentCharUInt != 0) {
        do {
            if (currentCharUInt == 10) {
                currentChar = currentChar + 1;
                nextLineYPosMaybe = nextLineYPosMaybe + sizeY * ((float)fontVertHeight2 / 64.0 + ySpacing);
                xPosition = xPos;
            } else if ((isSpecialCharacter[currentCharUInt] & 0x20) == 0) {
                short shortChar = getMultibyteChar(&currentChar);
                currentCharUInt = convertShiftJisToFontIdMaybe(shortChar, 0);
                advanceX = getGlyphInfoAndImageMaybe(currentCharUInt & 0xffff, &glyphInfo, &glyphInfo2);
                if (*INT_0015143c != 0) {
                    *INT_0015143c = 0;
                    sceKernelDcacheWritebackRange(fontGlyphsTexturePtr, *globFontSize1 * *globFontSize2);
                    FUN_000cc758();
                }
                drawTextGlyphMaybe((int)(xPosition + (float)glyphInfo.bitmapLeft * sizeX),
                                   (int)(nextLineYPosMaybe + (float)glyphInfo.bitmapTop * sizeY),
                                   (int)param_4, (short)glyphInfo2.glyphWidthMaybe,
                                   (short)glyphInfo2.glyphHeightMaybe, 0, 0);
                xPosition = xPosition + sizeX * (advanceX + xSpacing);
            }
            currentCharUInt = (uint)(byte)*currentChar;
        } while (currentCharUInt != 0);
    }
}

float getTextWidth(char *text, float size) {
    char *curChar = text;

    float result = 0;
    float xSpacing = 0;

    int advance;
    uint currCharUint = (uint)*curChar;
    do {
        short val = getMultibyteChar(&curChar);
        currCharUint = convertShiftJisToFontIdMaybe(val, 0);
        getCharInfo(currCharUint, NULL, NULL, NULL, NULL, NULL, NULL, &advance);
        float fAdvance = advance / 64.0;
        result = result + size * (fAdvance + xSpacing);
        currCharUint = (uint)*curChar;
    } while (currCharUint != 0);
    return result;
}

short getCharLen(char *str) {
    int c = (unsigned char)*str;

    if (((c > 0x80) && (c < 0xA0)) || ((c >= 0xE0) && (c <= 0xFC))) {
        return 2;
    }
    return 1;
}

void stdPrintShiftJis(char *input) {
    char output[1024];
    memset(&output, 0, sizeof(output));

    uint indexInput = 0, indexOutput = 0;
    uint inputLength = strlen(input);

    while (indexInput < inputLength) {
        char arraySection = ((unsigned char)input[indexInput]) >> 4;

        uint arrayOffset;
        if (arraySection == 0x8) {
            arrayOffset = 0x100;  // these are two-byte shiftjis
        } else if (arraySection == 0x9) {
            arrayOffset = 0x1100;
        } else if (arraySection == 0xE) {
            arrayOffset = 0x2100;
        } else {
            arrayOffset = 0;  // this is one byte shiftjis
        }

        //     //determining real array offset
        if (arrayOffset) {
            arrayOffset += (((unsigned char)input[indexInput]) & 0xf) << 8;
            indexInput++;
            if (indexInput >= inputLength) {
                break;
            }
        }
        arrayOffset += (unsigned char)input[indexInput++];
        arrayOffset <<= 1;

        //     //unicode number is...
        short unicodeValue = (shiftJIS_convTable[arrayOffset] << 8) | shiftJIS_convTable[arrayOffset + 1];

        //     //converting to UTF8
        if (unicodeValue < 0x80) {
            output[indexOutput++] = unicodeValue;
        } else if (unicodeValue < 0x800) {
            output[indexOutput++] = 0xC0 | (unicodeValue >> 6);
            output[indexOutput++] = 0x80 | (unicodeValue & 0x3f);
        } else {
            output[indexOutput++] = 0xE0 | (unicodeValue >> 12);
            output[indexOutput++] = 0x80 | ((unicodeValue & 0xfff) >> 6);
            output[indexOutput++] = 0x80 | (unicodeValue & 0x3f);
        }
    }
    output[indexOutput-1] = 0;

    sceKernelPrintf("\n%s\n", &output);
}


// -------------------------------

GlobParamsStruct* globParamsPtr = 0x8954d50;

void printFrameFunctions(){
    FrameFunctionStruct *curFunc = globParamsPtr->frameFunctions->firstFunction;

    while (curFunc!=NULL)
    {
        stdPrintf("%f : 0x%x\n", curFunc->priority, curFunc->funcPtr);
        curFunc = curFunc->nextFunc;
    }
    
}