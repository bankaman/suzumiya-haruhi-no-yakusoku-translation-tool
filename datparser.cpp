#include "datparser.h"
#include "ui_datparser.h"
#include "mainwindow.h"
#include <QMessageBox>
#include <QFontDatabase>

DatParser* DatParser::instance;

DatParser::DatParser(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DatParser)
{
    ui->setupUi(this);
    read_codec=QTextCodec::codecForName("Shift-JIS");

    const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    ui->textEdit->setFont(fixedFont);
    instance=this;
}

DatParser::~DatParser()
{
    delete ui;
}

void DatParser::showMessageBox(QString message){
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.exec();
}

void DatParser::log(QString str){
    ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(str);
}



void DatParser::openFile(const QString &filePath){
    QString filename;
    filename=filePath;
    main_window->addToRecentFiles(filePath);
    main_window->update_title(filePath);

    dat.setReadCodec(read_codec);
    if(!dat.load_file(filePath)){
        showMessageBox(dat.get_and_clear_log());
        return;
    }

    log(dat.get_and_clear_log());
    last_filename = filePath;

    update_screen();
}

bool DatParser::ifgoto(int where)
{
    int reply = QMessageBox::question(instance,"Условный переход","Перейти на "+QString::number(where,16).toUpper()+" ?",QMessageBox::Yes, QMessageBox::No);
    return (reply == QMessageBox::Yes);
}

void DatParser::on_open_button_clicked()
{    
    /*if(dat.read_codec!=NULL){
        read_codec=dat.read_codec;
    }*/

    QString filename;
    filename=QFileDialog::getOpenFileName(0, "Open dat file", "", "Haruhi Script(*.dat);;Other(*.*)", 0, 0);
    if(filename!=NULL)
        openFile(filename);
}

void DatParser::update_screen(){
    ui->label_info->setText("cursor = 0x"+QString::number(dat.getCursor(),16).toUpper()+" ("+QString::number(dat.getCursor())+") ");
}




void DatParser::on_pushButton_start_clicked()
{
    int temp,temp2,scur;
    QString string;
    bool progress;


    dat.clear();
    References::clear();



    if(!dat.parse_all()){
        this->show();
    }

    log(dat.get_and_clear_log());

    if(dat.not_resolved_links.length()!=0) {
        showMessageBox("Warning! I can't resolve some links! Result file will be broken!");
    }
}

void DatParser::on_pushButton_3_clicked()
{
    dat.setCursor(dat.getCursor()+1);
    update_screen();
}

void DatParser::on_pushButton_4_clicked()
{
    dat.setCursor(dat.getCursor()-1);
    update_screen();
}

void DatParser::on_pushButton_read_as_hex_clicked()
{
    int count;
    bool ok;
    count=ui->lineEdit_cout_to_read->text().toInt(&ok);
    dat.print_as_hex(count);
    log(dat.get_and_clear_log());
    update_screen();
}

void DatParser::on_pushButton_read_as_text_clicked()
{
    int count;
    bool ok;
    count=ui->lineEdit_cout_to_read->text().toInt(&ok);
    dat.print_as_text(count);
    log(dat.get_and_clear_log());
    update_screen();
}

void DatParser::on_lineEdit_cout_to_read_textChanged(const QString &arg1)
{
    bool ok;
    int num=arg1.toInt(&ok);
    if(num+dat.getCursor()>dat.getDataLength()){
        ui->lineEdit_cout_to_read->setText(QString::number(dat.getDataLength()-dat.getCursor()));
    }
}


void DatParser::on_checkBox_clicked(bool checked)
{
    dat.follow_goto_commands=checked;
}

void DatParser::on_checkBox_2_clicked(bool checked)
{
    dat.add_to_dat_object=checked;
}

void DatParser::on_actionPrint_dat_triggered()
{
    log("\n\n\n_______________printing dat____________\n");
    for(int i=0;i<dat.commands.length();i++){
        log("command №"+QString::number(i)+"|address=0x"+QString::number(dat.commands[i]->original_address,16).toUpper()+" \""+dat.commands[i]->command.toHex().toUpper()+"\"\n");
    }
    log("\n_______________printing dat links____________\n");
    log("count="+QString::number(dat.links.length())+"\n");
    for(int i=0;i<dat.links.length();i++){
        log("link("+QString::number(i)+"),to_address="+QString::number(dat.links[i]->to_command_address,16).toUpper()+", from="+QString::number(dat.links.at(i)->from_command_id)+", to="+QString::number(dat.links.at(i)->to_command_id)+"\n");
    }
    log("\n_______________ not resolved links_______________\n");
    log("count="+QString::number(dat.not_resolved_links.length())+"\n");
    for(int i=0;i<dat.not_resolved_links.length();i++){
        log("id="+QString::number(dat.not_resolved_links.at(i))+"\n");
    }
}

void DatParser::on_pushButton_reset_cursor_clicked()
{
    dat.setCursor(8);
    update_screen();
}
