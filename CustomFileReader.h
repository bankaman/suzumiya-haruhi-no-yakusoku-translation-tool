#ifndef DATAPARSER_H
#define DATAPARSER_H

#define ERROR_OUT_OF_RANGE 1
#define ERROR_UNKNOWN_COMMAND 2

#include <QString>
#include <QTextCodec>



class CustomFileReader
{
public:
    CustomFileReader(QByteArray _data, QTextCodec *read_codec);
    CustomFileReader();
    void init(QByteArray _data, QTextCodec *read_codec);

    int read_4_byte_int();
    int read_2_byte_int();
    QString read_string();
    QString read_string(int count);
    int read_reverse_2_byte_int();
    int read_2_signed_byte_int();

protected:
    int cursor;
    QByteArray data;
    QTextCodec *read_codec;
};

#endif // DATAPARSER_H
