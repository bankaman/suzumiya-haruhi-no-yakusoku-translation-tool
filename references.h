#ifndef REFERENCES_H
#define REFERENCES_H

#include <QString>
#include <QVector>
#include <QDebug>
#include <QDir>

class References
{
    struct Reference{
        int type;
        QString file;
    };
public:
    static const int TYPE_BG_IMAGE;
    static const int TYPE_SCRIPT;
    static const int TYPE_BTD;
    static const int TYPE_MAP;
    static const int TYPE_OBJ_IMAGE;
    static const int TYPE_VIDEO;

    References();
    static void clear();
    static void add(int type, QString file);
    static QString getFileName(int id);
    static int getFileType(int id);
    static QStringList list_items();
    static QVector<Reference*> references;
};

#endif // REFERENCES_H
