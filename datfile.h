#ifndef DATFILE_H
#define DATFILE_H

#include <qvector.h>
#include <qstring.h>
#include <qtextcodec.h>
#include <CustomFileReader.h>
#include <QStack>


class Link{
public:
    int from_command_id;
    int to_command_id;
    int to_command_address;
    int shift;
    int length;
};

class Command{
public:
    int original_address;
    QByteArray command;
    QByteArray new_command;
};

class TextPatch{
public:
    int command_id;
    QString text;
};

class DatFile : CustomFileReader
{
public:
    DatFile();
    ~DatFile();

    void append_command(int address, QByteArray command);
    void clear();
    int get_command_id_by_address(int address);
    int next_text_command_id(int from_id);
    int prev_text_command_id(int from_id);
    QString get_original_text(int command_id);
    QString get_audio_name(int command_id);
    QString get_patched_text(int command_id);
    int set_patched_text(int command_id, QString text);
    void create_new_text_command(int after_command_id, QString text);

    bool patch_and_save(QString filename);

    bool save_patch_to_file(QString filename);
    bool load_patch_from_file(QString filename);
    void clear_patch_info();

    int get_text_command_index_by_command_index(int command_id);
    int get_text_commands_count();

    bool add_to_dat_object=true;
    bool follow_goto_commands=false;

    bool load_file(QString filename);
    bool parse_command();
    bool parse_all();
    void clear_log();
    QString get_and_clear_log();
    void setIfgoto(void *func_pointer);
    void print_as_text(int count);
    void print_as_hex(int count);

    static QString decrypt_name_plate(int num);
    static QString decrypt_place_plate(int num);
    int getCursor(){return cursor;}
    void setCursor(int cursor){this->cursor=cursor;}
    int getDataLength(){return data.length();}
	QString getFileName(){return last_filename;}

    QString resore_cyrillic(QString str);
    QString get_map(bool restore_cyrillic, bool use_source_text, bool print_ids );
    QString get_map();

    QVector<Command*> commands;
    QVector<Link*> links;
    QVector<int> not_resolved_links;
    QVector<TextPatch*> text_patches;
    QVector<int> text_commands_adresses;

    void setReadCodec(QTextCodec* codec){read_codec = codec;}
	void setWriteCodec(QTextCodec* codec){write_codec = codec;}
	QTextCodec *getReadCodec(){return read_codec;}
    QTextCodec *write_codec;
    QTextCodec *utf;

private:
    int length; //Длина в байтах


    int read_as_2byte_int(QByteArray &data, int cursor);
    QString read_as_string(QByteArray &data, int cursor);
    QString read_string_until_zero(QByteArray &data, int &cursor);
    void append_2byte_int(QByteArray &data, int value);
    void add_link(int from_command_id, int to_command_address, int shift, int length);
    bool is_text_command(int id);
    int get_text_patch_id(int command_id);
    void normalize_textpatch();
    void showMessageBox(QString message);

    void set_link_offset(int from_command_id, int offset);

    void log(QString str);


    QVector<Link*> get_links_from_command(int command_id);
    QString log_string;
    QString last_filename;

    int max_string_length;
    bool (*ifgotocallback)(int where);

    void ifgoto(int where);

    QStack<int> stack;
};

#endif // DATFILE_H
