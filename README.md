# Translation project for game «Suzumiya Haruhi no Yakusoku» #

This repository includes translation tool, and translated files of the game.

# Компиляция программы #
## Ubuntu 16.04 ##
Установите необходимые пакеты:

```
#!bash

sudo apt-get install mercurial qt5-qmake qt5-default
```
Клонируйте репозиторий в любую папку, допустим "~/suzumiya-translation" :

```
#!bash

mkdir ~/suzumiya-translation
cd ~/suzumiya-translation
hg clone https://bitbucket.org/bankaman/suzumiya-haruhi-no-yakusoku-translation-tool
```

Создайте папку с названием build, после этого можно начать сборку программы

```
#!bash
mkdir build
cd build
qmake ../suzumiya-haruhi-no-yakusoku-translation-tool/
make

```

После этого можно запускать скомпилированный файл.
```
#!bash
./Haruhi_no_Yakusoku_obj_editor

```

# Запуск на эмуляторе PPSSPP #
Для того чтобы запустить результат перевода на эмуляторе нужно либо сгенерировать ISO образ, способом, который описан ниже, либо можно просто любым файловым менеджером распаковать содержимое оригинального образа игры и затем скопировать с заменой папку **PSP_GAME** из этого репозитория　("translated files/PSP_GAME").
После этого игру можно будет запустить на эмуляторе.

# Генерация ISO образа #

Для этого шага требуется наличие оригинального образа игры. И компьютера с ОС Windows, или установленный　wine.

1) Сделайте копию образа.
2) Запустите файл, который находится в папке репозитория /tools/WQSG_UMD_R31.exe со следующими параметрами:

```
#!batch

WQSG_UMD_R31.exe --iso=<имя образа> --file=<Путь до папки 'translated files/PSP_GAME'>
```
Например если все файлы и папки будут находится в одной директории то команда будет выглядеть так:
```
#!batch

WQSG_UMD_R31.exe --iso=Suzumiya_Haruhi_no_Yakusoku_JP.iso --file=.\PSP_GAME
```
Где "Suzumiya_Haruhi_no_Yakusoku_JP.iso" - оригинальный образ игры. После выполнения этой команды в него будут записаны переведённые файлы.