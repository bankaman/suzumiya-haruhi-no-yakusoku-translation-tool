#include "nltester.h"
#include "ui_nltester.h"
#include "mainwindow.h"

#include <QDebug>

NLTester::NLTester(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NLTester)
{
    ui->setupUi(this);

    bg = QImage(":/LT/bg.png");
    canvas = QImage(480,272,QImage::Format_RGB32);

    utf_codec=QTextCodec::codecForName("UTF-8");

    //Загрузка изображений символов
    QString ldata = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭЮЯабвгдеёжзийклмнопрстуфхцчшщьъыэюяABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,?'\"-!0123456789()";
    //QString ldata = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    QImage* temp;
    for(int i=0; i<ldata.size(); i++){
        temp = new QImage(QString(":/LT/alphabet/")+utf_codec->fromUnicode(ldata.at(i)).toHex().toUpper()+QString(".png"));
        letters.insert(ldata.at(i).unicode(), temp);
    }

    drawCanvas();
}

NLTester::~NLTester()
{
    delete ui;

    foreach (QImage* temp, letters) {
        delete temp;
    }
}

//------------------------------------ отрисовка данных на холст ----------------------
void NLTester::drawCanvas(){
    QPainter p;
    p.begin(&canvas);
    p.drawImage(0,0,bg);

    QImage* l;
    int x=4, y=208;
    for(int i=0;i<text.size();i++){
        l=letters[text.at(i).unicode()];
        if(l!=NULL){
            p.drawImage(x,y,*l);
            x+=l->width()-1;
        }
        if(text.at(i).unicode()==32){
            x+=5;
        }
        if(text.at(i).unicode()==10){
            y+=18;
            x=4;
        }

        //qDebug()<<text.at(i)<<" = "<<text.at(i).unicode();
    }

    p.end();
}


//----------------------------------------


void NLTester::setText(QString text_){
    //qDebug()<<"settext "<<text_;
    text=text_;
    drawCanvas();
    update();
}


void NLTester::showEvent(QShowEvent *e){

    drawCanvas();
}

void NLTester::paintEvent(QPaintEvent *e){

    QPainter p(this);
    int width = canvas.width();
    int height = canvas.height();

    width = this->width();
    height = width*canvas.height()/canvas.width();
    if(height>this->height()-50){
        height = this->height()-50;
        width = height*canvas.width()/canvas.height();
    }

    scale = canvas.scaled(width,height,Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    p.drawImage(this->width()/2-scale.width()/2,50,scale);

}
//------------------------------ update ---------------------
void NLTester::on_pushButton_clicked()
{
    setText(MainWindow::getCurrentLocalizedTextStatic());
    drawCanvas();
    update();
}
