#include "btdeditor.h"
#include "ui_btdeditor.h"
#include "mainwindow.h"

BtdEditor::BtdEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BtdEditor)
{
    ui->setupUi(this);
    current_theme=-1;
    current_variant=-1;
}

BtdEditor::~BtdEditor()
{
    delete ui;
}

void BtdEditor::on_LoadButton_clicked()
{
    QString filename=QFileDialog::getOpenFileName(0, "Open btd file", "", "BTD File(*.btd);;Other(*.*)", 0, 0);
    if(filename!=NULL)
        load_file(filename);
}

void BtdEditor::showMessageBox(QString message)
{
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.exec();
}

void BtdEditor::load_file(QString filename)
{
    current_file = filename;
    if(!btd.load(filename)){
        showMessageBox("Error loading fle: "+filename);
        return;
    }
    update_ui_tree();
}

void BtdEditor::update_ui_tree()
{
    model.clear();
    QStringList themes = btd.get_themes();

    QStringList items;

    QStandardItem *parent;
    QStandardItem *variant;
    for(int i = 0; i<themes.length();i++){

        parent = new QStandardItem();
        parent->setText(themes.at(i));
        items = btd.get_variants(i);
        for(int j=0;j<items.length();j++){
            variant=new QStandardItem();
            variant->setText(items.at(j));
            parent->appendRow(variant);
        }
        model.appendRow(parent);
    }

    ui->treeView->setModel(&model);
}

void BtdEditor::update_ui_labels()
{
    QString tstr;
    QByteArray t_ba;
    int t;

    if(current_theme>=0){
        tstr = btd.get_theme(current_theme);
        t_ba = btd.text_codec->fromUnicode(tstr);
        t = btd.get_theme_max_size(current_theme)-t_ba.length();
        if(t<0)
            tstr = "<font color=#880000>"+QString::number(t)+"</font>";
        else
            tstr = "<font color=#008800>"+QString::number(t)+"</font>";
        ui->labelTheme->setText("Theme: ("+tstr+")");
    }else
        ui->labelTheme->setText("Theme:");

    if(current_variant>=0){
        tstr = btd.get_variant(current_theme, current_variant);
        t_ba = btd.text_codec->fromUnicode(tstr);
        t = btd.VARIANT_MAX_SIZE-t_ba.length();
        if(t<0)
            tstr = "<font color=#880000>"+QString::number(t)+"</font>";
        else
            tstr = "<font color=#008800>"+QString::number(t)+"</font>";
        ui->label_Variant->setText("Variant: ("+tstr+")");
        ui->label_datFile->setText(btd.get_variant_filename(current_theme, current_variant));
    }else{
        ui->label_datFile->setText("None");
        ui->label_Variant->setText("Variant:");
    }
}



void BtdEditor::update_ui_input_boxs()
{
    ui->lineEditTheme->setReadOnly(current_theme<0);
    ui->lineEdit_Variant->setReadOnly(current_variant<0);

    QString tstr;
    QByteArray t_ba;
    int t;

    if(current_theme>=0){
        tstr = btd.get_theme(current_theme);
        ui->lineEditTheme->setText(tstr);

    }else
        ui->lineEditTheme->setText("");

    if(current_variant>=0){
        tstr = btd.get_variant(current_theme, current_variant);
        ui->lineEdit_Variant->setText(tstr);
    }else{
        ui->lineEdit_Variant->setText("");
    }

    update_ui_labels();
}



void BtdEditor::on_treeView_clicked(const QModelIndex &index)
{
    int parent = index.parent().row();
    int row = index.row();

    if(parent==-1){
        current_theme=row;
        current_variant=-1;
    }else{
        current_theme=parent;
        current_variant=row;
    }
    update_ui_input_boxs();
}

void BtdEditor::on_lineEditTheme_textChanged(const QString &text)
{
    if(current_theme<0)
        return;

    btd.set_theme_text(current_theme,text);
    model.item(current_theme)->setText(text);
    update_ui_labels();
}

void BtdEditor::on_lineEdit_Variant_textChanged(const QString &text)
{
    if((current_theme<0)||(current_variant<0))
        return;

    btd.set_variant_text(current_theme,current_variant,text);
    model.item(current_theme)->child(current_variant)->setText(text);
    update_ui_labels();
}


void BtdEditor::on_pushButton_clicked()
{
    QString filename=QFileDialog::getSaveFileName(0, "Save btd file", "", "BTD File(*.btd)", 0, 0);
    if(filename!=NULL){
        if(!filename.endsWith(".btd")) filename.append(".btd");
        if(!btd.save(filename))
            showMessageBox("Error saving file!");
    }
}

void BtdEditor::on_pushButton_save_clicked()
{
    QMessageBox::StandardButton r = QMessageBox::question(this,"Save","Save into same file (May be dangerous)?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No );
    if (r == QMessageBox::No)
        return;
    btd.save(current_file);
}

void BtdEditor::on_pushButton_openDatFile_clicked()
{
    if(current_theme<0)
        return;
    if(current_variant<0)
        return;
    QString filename = btd.get_variant_filename(current_theme,current_variant);

    QString path = current_file.section("/",0,-2)+"/../script/"+filename;

    MainWindow::me->openScriptFile(path);
}

void BtdEditor::on_pushButton_Theme_tolatin_clicked()
{
    if(current_theme<0)
        return;
    btd.set_theme_text(current_theme,MainWindow::toLatin(btd.get_theme(current_theme)));
    update_ui_input_boxs();
}

void BtdEditor::on_pushButton_variant_tolatin_clicked()
{
    if(current_theme<0)
        return;
    if(current_variant<0)
        return;

    btd.set_variant_text(current_theme,current_variant,MainWindow::toLatin(btd.get_variant(current_theme,current_variant)));
    update_ui_input_boxs();
}
