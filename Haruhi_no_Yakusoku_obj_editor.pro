#-------------------------------------------------
#
# Project created by QtCreator 2016-04-07T15:56:37
#
#-------------------------------------------------

QT       += core gui sql network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Haruhi_no_Yakusoku_obj_editor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    aboutdialog.cpp \
    datfile.cpp \
    datparser.cpp \
    shiftjiscoder.cpp \
    translationhelper.cpp \
    translationhelperconfig.cpp \
    nltester.cpp \
    btdeditor.cpp \
    btdfile.cpp \
    referencesform.cpp \
    references.cpp \
    scriptmap.cpp \
    CustomFileReader.cpp

HEADERS  += mainwindow.h \
    aboutdialog.h \
    datfile.h \
    datparser.h \
    shiftjiscoder.h \
    translationhelper.h \
    translationhelperconfig.h \
    nltester.h \
    btdeditor.h \
    btdfile.h \
    referencesform.h \
    references.h \
    scriptmap.h \
    CustomFileReader.h

FORMS    += mainwindow.ui \
    aboutdialog.ui \
    datparser.ui \
    shiftjiscoder.ui \
    translationhelperconfig.ui \
    nltester.ui \
    btdeditor.ui \
    referencesform.ui \
    scriptmap.ui

RESOURCES += \
    resources.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
