#include "CustomFileReader.h"

CustomFileReader::CustomFileReader(QByteArray _data, QTextCodec *read_codec)
{
    init(data,read_codec);
}
CustomFileReader::CustomFileReader(){

}

void CustomFileReader::init(QByteArray _data, QTextCodec *read_codec)
{
    cursor = 0;
    data = _data;

    this->read_codec=read_codec;
}

int CustomFileReader::read_4_byte_int(){
    int out=0;
    if(cursor+4>data.length()) throw ERROR_OUT_OF_RANGE;
    out=(data.at(cursor)&0xff);cursor++;
    out+=((data.at(cursor)&0xff)<<8);cursor++;
    out+=((data.at(cursor)&0xff)<<16);cursor++;
    out+=((data.at(cursor)&0xff)<<24);cursor++;
    return out;
}

int CustomFileReader::read_2_byte_int(){
    int out=0;
    //print_as_hex(4);
    if(cursor+2>data.length()) throw ERROR_OUT_OF_RANGE;
    out=(data.at(cursor)&0xff);cursor++;
    out+=((data.at(cursor)&0xff)<<8);cursor++;

    return out;
}

QString CustomFileReader::read_string()
{
    QString out;
    int len = read_2_byte_int();
    out = read_codec->toUnicode(data.mid(cursor,len));
    cursor+=len;
    return out;
}

QString CustomFileReader::read_string(int count)
{
    QString out;
    out = read_codec->toUnicode(data.mid(cursor,count));
    cursor+=count;
    return out;
}

int CustomFileReader::read_reverse_2_byte_int(){
    int out=0;
    //print_as_hex(4);
    if(cursor+2>data.length()) throw ERROR_OUT_OF_RANGE;
    out=(data.at(cursor)&0xff)<<8;cursor++;
    out+=((data.at(cursor)&0xff));cursor++;

    return out;
}

int CustomFileReader::read_2_signed_byte_int(){
    int out=0;
    //print_as_hex(4);
    if(cursor+2>data.length()) throw ERROR_OUT_OF_RANGE;
    out=(data.at(cursor)&0xff);cursor++;
    out+=((data.at(cursor)&0xff)<<8);cursor++;

    if((((data.at(cursor-1)&0xff)>>7)&1)==1){
        out=out|0xffff0000;
    }

    return out;
}
