#include "scriptmap.h"
#include "ui_scriptmap.h"
#include <QScrollBar>
#include <QDebug>
#include "mainwindow.h"

ScriptMap::ScriptMap(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScriptMap)
{
    ui->setupUi(this);

    QScrollBar* bar1 = ui->map_text->verticalScrollBar();
    QScrollBar* bar2 = ui->map_text_orig->verticalScrollBar();

    connect(bar1, SIGNAL(valueChanged(int)), bar2, SLOT(setValue(int)));
    connect(bar2, SIGNAL(valueChanged(int)), bar1, SLOT(setValue(int)));

    ui->map_text->setOpenLinks(false);
    ui->map_text_orig->setOpenLinks(false);
}

ScriptMap::~ScriptMap()
{
    delete ui;
}

void ScriptMap::update(DatFile &dat)
{
    cur_dat_file = &dat;

    int scrolValue = ui->map_text->verticalScrollBar()->value();
    ui->map_text->clear();
    ui->map_text_orig->clear();
    ui->map_text_orig->append(parseMap(dat.get_map(true,true,true)));
    ui->map_text->append(parseMap(dat.get_map(true,false,true)));
    ui->map_text->verticalScrollBar()->setValue(scrolValue);
    ui->map_text_orig->verticalScrollBar()->setValue(scrolValue);
}

void ScriptMap::on_pushButton_clicked()
{
    ui->map_text->clear();
    ui->map_text->append(cur_dat_file->get_map(true,true,false));
}

void ScriptMap::on_pushButton_restore_cyr_bellow_clicked()
{
    QString tmp = ui->map_text->toPlainText();
    ui->map_text->clear();
    ui->map_text->append(cur_dat_file->resore_cyrillic(tmp));
}

QString ScriptMap::parseMap(QString string)
{
    QStringList ar = string.split("\n");
    QStringList out;

    int id=-1;
    for(int i=0;i<ar.length();i++){
        QString t = ar[i];
        if(ar[i].length()>5 && ar.at(i).mid(0,5)=="[[id:"){
            QString sid = ar[i].mid(5);
            sid = sid.mid(0,sid.length()-2);
            id = sid.toInt();
            //qDebug()<<"t="<<t<<"  id="<<id;
            continue;
        }
        if(id>=0){
            t = "<a href='"+QString::number(id)+"' >"+t+"</a>";
        }
        //qDebug()<<t;
        out.append(t);
    }

    return out.join("<br>\n");
}

void ScriptMap::on_map_text_orig_anchorClicked(const QUrl &arg1)
{
    MainWindow::me->gotoId(arg1.toString().toInt());
    MainWindow::me->updateTranslationHelper();
}

void ScriptMap::on_map_text_anchorClicked(const QUrl &arg1)
{
    MainWindow::me->gotoId(arg1.toString().toInt());
    MainWindow::me->updateTranslationHelper();
}
