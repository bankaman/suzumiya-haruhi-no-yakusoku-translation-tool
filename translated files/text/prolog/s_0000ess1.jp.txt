-(GOTO 55)-
(LABEL 55)
((プロローグ))
[Харухи]
「つまり、あんたが言いたいことをまとめるとこういう
ことね」

「宇宙人も未来人も超能力者も、実は既にあたしの身近
にいて」

「それは有希と、みくるちゃんと、古泉くんだって」

[Кён]
「まさしく、そういうことだ」

[Харухи]
「……」

「ふざけんなっ！」

[Кён]
まあそう言いたい気持ちはよくわかる。俺だって同じこ
とを誰かに言われたら、同じ反応をするだろう。

[Харухи]
「キョン、よーく聞きなさい」

「宇宙人や未来人や超能力者なんてのはね、すぐそこら
へんに転がってなんかいないのよ！」

「探して見つけて捕まえて首つかんでぶらさげて逃げ出
さないようにグルグル巻きにしとかないといけないくら
いの希少価値があるものなのよ！」

「選んできた団員が全員そんなのだなんて、あるわけ
ないじゃないの！」

[Кён]
そんなわけで、俺が思いきって口にした厳然たる真実は
『面白くないアホジョーク』と決め付けられ、

挙げ句の果てにハルヒは財布を忘れたとかで喫茶店の払
いも俺がすることになり、何もならないどころかこれ
じゃ収支は文字通りマイナスである。

(――)
[Кён]
だが後から考えると、《あの一件》は、もしかしたらこ
こでの会話がきっかけだったかもしれないという気もす
る。

《あの一件》。それが起こったのは、喫茶店での会話か
ら約５カ月を経て、夏休みも、体育祭も終わった頃――

文化祭を翌日に控えた、暦の上ではとっくに秋だという
のにまるで夏の置き土産のようにじっとりと暑い、そん
な日だった。

 -( запустить 's_eyecatch.dat' )-

