(妹があだ名でしか呼んでくれん)
(Сестрёнка зовёт меня только по прозвищу)
(Сестрёнка не называет меня по имени)
(Сестрёнка зовёт меня по прозвищу)

-(GOTO 59)-
(LABEL 59)
-(IF ... )-(GOTO 62)-
-(IF ... )-(GOTO 123)-
-(IF ... )-(GOTO 170)-
(LABEL 62)
-(IF ... )-(GOTO 96)-

[Коидзуми]
- А как она тебя изначально называла?

[Кён]
- Ну, обычно "братик".

[Коидзуми]
- Так ты хочешь чтобы она так тебя называла?

[Кён]
- А? Коидзуми, ты серьёзно это говоришь?

[Коидзуми]
- Ты решил что я шучу, да? Мне казалось что я выгляжу серьёзным.

-(GOTO 220)-
(LABEL 96)

[Коидзуми]
- Так как ты хочешь чтобы она тебя называла?

[Кён]
- Ну ничего особенного, наверное просто "братик".

[Коидзуми]
- Понятно, возьму на заметку.

[Кён]
- Подожди, что за заметка?

[Коидзуми]
- Так что мне взять на заметку?

[Кён]
- Свою шутку например.

-(GOTO 220)-
(LABEL 123)
-(IF ... )-(GOTO 149)-

[Коидзуми]
- Так а в чём проблема?

[Кён]
- Ну, почему не "братик"?

[Коидзуми]
- Значит ты хочешь чтобы она тебя так называла, понятно какие у тебя предпочтения.

[Кён]
- Подожди причём здесь предпочтения? Не в этом суть.

[Коидзуми]
- Так это не так?

[Кён]
- Совершенно не так!

-(GOTO 220)-
(LABEL 149)

[Коидзуми]
- Я думаю это не проблема.

[Кён]
- Но, это как то удручает когда твоя младшая сестра зовёт тебя по кличке.

[Коидзуми]
- Другими словами дома ты всегда удручённый бездеятельный лентяй?

[Кён]
- Я такого не говорил.

-(GOTO 220)-
(LABEL 170)
-(IF ... )-(GOTO 192)-

[Коидзуми]
- Может так даже лучше что она обращается к тебе только по прозвищу.

[Кён]
- В каком смысле?

[Коидзуми]
- Что если она начнёт к тебе обращаться "Эй ты", или "Слыш чувак"?

[Кён]
- Прекрати. Даже представлять не хочу.

-(GOTO 220)-
(LABEL 192)

[Коидзуми]
- У вас похоже хорошие отношения, тебе не на что жаловаться.

[Кён]
- Но когда младшая сестра тебя называет по прозвищу, как же уважение к старшему брату?

[Коидзуми]
- Извини, а было ли оно изначально?

[Кён]
- Как строго.

-(GOTO 220)-
(LABEL 220)
(LABEL 220)
(LABEL 220)
(LABEL 220)
(LABEL 220)
(LABEL 220)

