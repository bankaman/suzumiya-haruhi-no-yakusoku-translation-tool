(未来から来るってどんな気分ですか)
(Каково это, придти из будущего?)

-(GOTO 31)-
(LABEL 31)
-(IF ... )-(GOTO 33)-
-(IF ... )-(GOTO 91)-
(LABEL 33)
-(IF ... )-(GOTO 61)-

[Асахина-сан]
- Мне, знающей что будет в будущем, нельзя менять события, которые напрямую с ним связаны.

- Поэтому неоднократно возникают такие раздражающие мысли.

[Кён]
- Это тяжело. Я наверное бы не выдержал.

[Асахина-сан]
- Несмотря на то что это тяжело, это мой долг. Так что ничего не поделаешь.

-(GOTO 132)-
(LABEL 61)

[Асахина-сан]
- Мне постоянно страшно, вдруг кто то из этого времени узнает кто я на самом деле.

[Кён]
- Я думаю тебе не стоит об этом беспокоиться. В это время никому не придёт в голову, что рядом с ним путешественник во времени.

[Асахина-сан]
- Ага... Но я ведь неуклюжая, вдруг всё равно кто нибудь заметит.

[Кён]
- Невозможно.

-(GOTO 132)-
(LABEL 91)
-(IF ... )-(GOTO 115)-

[Асахина-сан]
- Здорово, что мне удалось прикоснуться к неизвестной культуре.

[Кён]
- Ясно, то что для нас кажется обычным, для тебя это другая культура, да?

[Асахина-сан]
- Да. Тут столько всего удивительного. И ещё много того, чего я не знаю.

[Кён]
- Если захочешь о чём то спросить или что нибудь попробовать, говори без стеснения. Помогу чем смогу.

[Асахина-сан]
- Ага, спасибо. Как нибудь спрошу.

-(GOTO 132)-
(LABEL 115)

[Асахина-сан]
- Ну, это как "Закрытая информация" или "Закрытая информация", когда "Закрытая информация".

[Кён]
- Совсем ничего не понятно...

-(GOTO 132)-
(LABEL 132)
(LABEL 132)
(LABEL 132)
(LABEL 132)

