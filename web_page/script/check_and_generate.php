<?php

include_once "storage.php";
include_once "config.php";

function hash_to_array($text){
  $text = explode("\n",$text);
  $out = [];
  foreach ($text as $row) {
    if(!$row)
      continue;
    $e = explode("  ",$row);
    $out[$e[1]]=$e[0];
  }
  return $out;
}

$git = "git --git-dir \"".REPO_DIR."/.git\" --work-tree \"".REPO_DIR."\"";

shell_exec("$git pull");
shell_exec("$git checkout");

$current_revision = shell_exec($git." log --format=\"%H\" -n 1");
$last_revision = Storage::get("revision", -1);
if($current_revision == $last_revision)
  die;

Storage::set("revision",$current_revision);

// get translated precent

$to_translate = hash_to_array(file_get_contents("storage/need_to_translate.txt"));
$translated = hash_to_array(shell_exec("cd \"".REPO_DIR."/translated files/PSP_GAME\" ; find . -type f -exec md5sum {} \;"));

$tr_counter = 0;
$tfiles = [];
foreach ($translated as $name => $hash) {
  if(
      (array_key_exists($name,$to_translate)) &&
      ($to_translate[$name] != $hash)
    ){
      $tr_counter++;
      $tfiles[] = $name;
    }
}
$precent = floor((100*$tr_counter/count($to_translate))*100)/100;
Storage::set("precent",$precent);

//getting last activity
$last_activity = shell_exec("$git --no-pager log -n 100 --date=format:'%Y-%m-%d %H:%M:%S' --pretty=format:\"%an, %ad : %s\"");
Storage::set("last_activity",$last_activity);

//generating iso
shell_exec("./bash/iso_autobuild.sh \"".ISO_JP."\" \"".REPO_DIR."/translated files/PSP_GAME"."\" \"".ISO_RU."\"");
Storage::set("iso_generation_time",date("d.m.Y H:i:s"));

//generating zsync
shell_exec("cd \"".ISO_FOLDER."\"; zsyncmake -u \"".ISO_SITE_URL."\" -f \"".ISO_RU_NAME."\" \"".ISO_RU_NAME."\"");

//cancel seeding previos iso_file
shell_exec("sh ./bash/transmission_delete.sh \"".ISO_RU_NAME."\"");

//generating torrent
shell_exec("cd \"".ISO_FOLDER."\" ;transmission-create -t \"".TRACKER_URL."\" \"".ISO_RU."\"");
shell_exec("cp \"".TORRENT_FILE."\" \"".TRANSMISSION_WATCH_DIR."/\"");
shell_exec("chmod 775 \"".TRANSMISSION_WATCH_DIR.'/'.ISO_RU_NAME.".torrent\"");
shell_exec("chmod 775 \"".TORRENT_FILE."\"");

Storage::sync();
