<?php

define("STORAGE_FOLDER", dirname(__FILE__)."/storage/");

class Storage{
  private static $storage = [];

  public static function get($var, $default = false){
    if(array_key_exists($var,self::$storage))
      return self::$storage[$var];
    else
      return $default;
  }

  public static function init(){
    if(!file_exists(STORAGE_FOLDER."storage.txt"))
      return;
    self::$storage = unserialize(file_get_contents(STORAGE_FOLDER."storage.txt"));
  }

  public static function set($var, $value){
    self::$storage[$var] = $value;
  }

  public static function sync(){
    return file_put_contents(STORAGE_FOLDER."storage.txt", serialize(self::$storage));
  }
}

Storage::init();
