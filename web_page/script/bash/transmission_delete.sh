#!/bin/bash

name=$1

torrent_id=$( transmission-remote  --authenv --list | grep "${name}" |  awk -F" " '{print $1}' )

transmission-remote --authenv --torrent ${torrent_id} --remove


