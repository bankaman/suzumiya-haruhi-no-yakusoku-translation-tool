#!/bin/bash

WINE='wine'

function usage(){
    echo "$0 source_iso PSP_GAME result_iso"
}

source_iso=$1
gamedir=$2
result_iso=$3

patcher="$( dirname $0 )/patcher.exe"

if [[ $# -lt 3 ]] ; then
    usage
    exit 1
fi

#Копирование исходного образа в получаемый
echo "Copying source image"
pv "${source_iso}" > "${result_iso}"

if [[ $? -ne "0" ]]; then
    echo "failed to copy file"
    exit 2;
fi

#Приминение патча
echo "Patching"

result_iso="Z:"` echo "$result_iso" | tr '/' '\\'`
gamedir="Z:"` echo "$gamedir" | tr '/' '\\'`
echo "$result_iso"
echo "$gamedir"

$WINE "${patcher}" --iso="${result_iso}" --file="${gamedir}"

if [[ $? -ne "0" ]]; then
    echo "fail while patching"
    exit 3;
fi

echo "ok";


    
