<?php
include "script/config.php";
include "script/storage.php";

$last_activity = Storage::get("last_activity","error");
$precent = Storage::get("precent");
$iso_generation_time = Storage::get("iso_generation_time");

$subject = "Проект перевода игры «Обещание Харухи Судзумии»";
$description = "Страница для загрузки последней версии перевода игры «Обещание Харухи Судзумии» на русский язык."

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Suzumiya Haruhi no Yakusoku Translation Project</title>
    <link rel="stylesheet" href="css/index.css" type="text/css">
    <meta name="subject" content="<?= $subject ?>"/>
    <meta property="og:title" content="<?= $subject ?>"/>
    <meta name="description" content="<?= $description ?>"/>
    <meta property="og:description" content="<?= $description ?>"/>
    <meta name="author" content="Tomilin Dmitriy, bankastudio@gmail.com">
    <meta property="og:url" content="<?= SITE_URL ?>"/>
    <meta property="og:image" content="<?= SITE_URL ?>img/icon.png"/>
	<link rel="icon" type="image/png" href="/img/icon.png">
</head>
<body>
    <div class="container">
        <h3>Проект перевода игры<br>«Обещание Харухи Судзумии»</h3>
        <div class="precent_download">
            <div class="precent">
                <p>Завершено:</p>
                <h3><?= $precent ?>%</h3>
            </div>
            <div class="download">
                <p>Дата генерации:<br><?= $iso_generation_time ?></p>
                <a href="<?= ISO_SITE_URL ?>"><div class="download_btn" >Скачать ISO</div></a>
                <p style="margin-top: 0;" ><a href="<?= ISO_SITE_URL ?>.zsync">zsync</a>|
                <a href="<?= TORRENT_SITE_URL ?>">torrent</a></p>
            </div>
        </div>
        <div class="clear"></div>
        <div class="sites_info">
          <p><a href="http://anivisual.net/board/1-1-0-521">Cтраница на Anivisual.net</a> | <a href="https://bitbucket.org/bankaman/suzumiya-haruhi-no-yakusoku-translation-tool">Проект на bitbucket</a> | <a href="http://vk.com/club192454742" >Группа ВКонтакте</a></p>
        </div>
        <div class="clear"></div>
        <h4>Последняя активность:</h4>
        <div class='last_activity'>
          <pre><?= $last_activity ?></pre>
        </div>
    </div>
</body>
</html>
