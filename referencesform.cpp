#include "mainwindow.h"
#include "referencesform.h"
#include "ui_referencesform.h"

#include <QDesktopServices>

ReferencesForm::ReferencesForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReferencesForm)
{
    ui->setupUi(this);    
}

void ReferencesForm::showMessageBox(QString message){
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.exec();
}

ReferencesForm::~ReferencesForm()
{
    delete ui;
}

void ReferencesForm::update_data()
{
    ui->listWidget->clear();
    ui->listWidget->addItems(References::list_items());
}

void ReferencesForm::init()
{
    settings = MainWindow::me->getSettings();
    translatedFilesDir = settings->value("translatedFilesDir",".").toString();
    originalFilesDir = settings->value("originalFilesDir",".").toString();
    updateLabels();
}

void ReferencesForm::updateLabels()
{
    ui->trDirLabel->setText("translated files: "+translatedFilesDir);
    ui->orDirLabel->setText("orig files: "+originalFilesDir);
}

void ReferencesForm::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    init();
}

void ReferencesForm::on_trDirButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), nullptr, QFileDialog::ShowDirsOnly);
    if(!dir.isNull()){
        translatedFilesDir = dir;
        settings->setValue("translatedFilesDir", dir);
        settings->sync();
        updateLabels();
    }
}

void ReferencesForm::on_orDirButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), nullptr, QFileDialog::ShowDirsOnly);
    if(!dir.isNull()){
        originalFilesDir = dir;
        settings->setValue("originalFilesDir", dir);
        settings->sync();
        updateLabels();
    }
}

void ReferencesForm::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    int index = ui->listWidget->selectionModel()->selectedIndexes().first().row();
    QString fileName = References::getFileName(index);
    int type = References::getFileType(index);
    if(type == References::TYPE_BG_IMAGE){
        fileName = "USRDIR/data/bg/"+fileName;
    }else if(type == References::TYPE_SCRIPT){
        fileName = "USRDIR/data/script/"+fileName;
    }else if(type == References::TYPE_OBJ_IMAGE){
        fileName = "USRDIR/data/obj/"+fileName;
    }else if(type == References::TYPE_VIDEO){
        fileName = "USRDIR/data/"+fileName;
    }
    QString path = translatedFilesDir + "/" + fileName;
    if(!QFile::exists(path)){
        path = originalFilesDir + "/" + fileName;
        if(!QFile::exists(path)){
            showMessageBox("File does not exists: "+path);
            return;
        }
    }

    QDesktopServices::openUrl(QUrl::fromLocalFile(path));
}
