#include "references.h"

const int References::TYPE_BG_IMAGE  = 0;
const int References::TYPE_SCRIPT    = 1;
const int References::TYPE_BTD       = 2;
const int References::TYPE_MAP       = 3;
const int References::TYPE_OBJ_IMAGE = 4;
const int References::TYPE_VIDEO     = 5;

QVector<References::Reference*> References::references;

References::References()
{

}

void References::clear()
{
    for(int i=0;i<references.length();i++){
        delete references.at(i);
    }
    references.clear();
}

void References::add(int type, QString file)
{

    Reference *ref;
    ref = new Reference;
    ref->type = type;
    ref->file = file;
    if(type == TYPE_BTD){
        ref->file = ref->file.mid(ref->file.lastIndexOf(QDir::separator())+1);
        ref->file = ref->file.mid(0,ref->file.length()-3)+"btd";
    }
    references.append(ref);
    //qDebug()<<"add reference "<<type<<ref->file;
}

QString References::getFileName(int id)
{
    return references.at(id)->file;
}

int References::getFileType(int id)
{
    return references.at(id)->type;
}

QStringList References::list_items()
{
    QStringList out;
    Reference *ref;
    for(int i=0;i<references.length();i++){
        ref = references.at(i);
        out.append(QString::number(ref->type)+" | "+ref->file);
    }
    return out;
}
