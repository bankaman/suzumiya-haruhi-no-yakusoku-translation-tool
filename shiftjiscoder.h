#ifndef SHIFTJISCODER_H
#define SHIFTJISCODER_H

#include <QDialog>
#include <QTextCodec>

namespace Ui {
class ShiftJisCoder;
}

class ShiftJisCoder : public QDialog
{
    Q_OBJECT

public:
    explicit ShiftJisCoder(QWidget *parent = 0);
    ~ShiftJisCoder();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::ShiftJisCoder *ui;

    void log(QString string);
    QString toHex(QString string);

    QTextCodec *jp;
};

#endif // SHIFTJISCODER_H
