<?php

define('JP_FOLD', '/home/banka/Haruhi/Games/PSP/Suzumiya Haruhi no Yakusoku/translation/jp/NoLabel/PSP_GAME/USRDIR/data/script');
define('RU_FOLD', '/home/banka/QT/suzumiya-haruhi-no-yakusoku-translation-tool/translated files/PSP_GAME/USRDIR/data/script');

function std_debug($str){
    print_r ($str);
    echo "\n";
}

function query($data){
    $url = "http://127.0.0.1:3000/api";
    $handle = curl_init();
    curl_setopt($handle, CURLOPT_URL, $url);
    curl_setopt($handle, CURLOPT_POST, true);
    curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    
    $out = curl_exec($handle);
    curl_close($handle);
    return $out;
}

function get_api_functions_list(){
    $data = [
        'command' => 'get_api_functions_list',
        'params' => []
    ];
    return json_decode(query(json_encode($data)),true);
}

function open_file($filename){
    $data = [
        'command' => 'open_file',
        'params' => [
            'path' => $filename
        ]
    ];
    return json_decode(query(json_encode($data)),true);
}

function close_file($id){
    $data = [
        'command' => 'close_file',
        'params' => [
            'id' => $id
        ]
    ];
    return json_decode(query(json_encode($data)),true);
}

function get_file_map($id){
    $data = [
        'command' => 'get_file_map',
        'params' => [
            'id' => $id,
            'map_from_source' => true
        ]
    ];
    return json_decode(query(json_encode($data)),true);
}

function generateMap($filename){
    $inf = open_file($filename);
    
    $id = $inf['id'];
    
    $map = explode("\n",get_file_map($id)['result']);
    $pmap = "";
    foreach($map as $line){
        if(strpos($line,'[[id: ') === 0)
            continue;
        $pmap.=$line."\n";
    }
    
    close_file($id);
    return $pmap;
}

if($argc< 2){
    std_debug("Usage: make-txt-from-dat.php list-of-files\n");
    exit;
}

$files = explode(",",$argv[1]);

foreach ($files as $file){
    $filename = JP_FOLD."/".$file.".dat";
    $map = generateMap($filename);
    file_put_contents($file.'.jp.txt',$map);
    
        $filename = RU_FOLD."/".$file.".dat";
    $map = generateMap($filename);
    file_put_contents($file.'.ru.txt',$map);
}
//
//std_debug(open_file());
