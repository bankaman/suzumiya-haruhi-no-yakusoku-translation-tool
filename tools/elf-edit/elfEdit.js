const fs = require('fs');

const ARCH_CLASS_X32 = 1;
const ARCH_CLASS_X64 = 2;

const LITTLE_ENDIAN = 1;
const BIG_ENDIAN = 2;

const R_MIPS_32 = 2;
const R_MIPS_26 = 4;
const R_MIPS_HI_16 = 5;
const R_MIPS_LO_16 = 6;

class BinaryReader {
    /**
     * @param buffer : Buffer
     */
    constructor(buffer) {
        this.buffer = buffer;
        this.cur = 0;
        this.endian = LITTLE_ENDIAN;
        this.virtualAddress = 0;
    }

    readByte() {
        let value = this.buffer[this.cur];
        this.cur++;
        return value;
    }

    readWord() {
        let value = 0;
        if (this.endian === LITTLE_ENDIAN) {
            value = this.readByte();
            value |= this.readByte() << 8;
        } else {
            value = this.readByte() << 8;
            value |= this.readByte();
        }
        return value;
    }

    readInt() {
        let value = 0;
        if (this.endian === LITTLE_ENDIAN) {
            value = this.readByte();
            value |= this.readByte() << 8;
            value |= this.readByte() << 16;
            value |= this.readByte() << 24;
        } else {
            value = this.readByte() << 24;
            value = this.readByte() << 16;
            value = this.readByte() << 8;
            value |= this.readByte();
        }
        return value;
    }

    readString() {
        let start = this.cur;
        while (this.buffer[this.cur] !== 0) {
            this.cur++;
        }
        let result = this.buffer.subarray(start, this.cur);
        this.cur++;
        return result.toString();
    }

    writeInt(value) {
        if (this.endian === LITTLE_ENDIAN) {
            this.buffer[this.cur] = value & 0xFF;
            this.buffer[this.cur + 1] = (value >> 8) & 0xFF;
            this.buffer[this.cur + 2] = (value >> 16) & 0xFF;
            this.buffer[this.cur + 3] = (value >> 24) & 0xFF;
        } else {
            this.buffer[this.cur + 3] = value & 0xFF;
            this.buffer[this.cur + 2] = (value >> 8) & 0xFF;
            this.buffer[this.cur + 1] = (value >> 16) & 0xFF;
            this.buffer[this.cur] = (value >> 24) & 0xFF;
        }
        this.cur += 4;
    }

    setCursor(value) {
        this.cur = value - this.virtualAddress;
    }

    /**
     * @returns {number}
     */
    getCursor() {
        return this.cur + this.virtualAddress;
    }
}

/**
 * @param elf : BinaryReader
 */
async function parseElfHeader(elf) {
    let signature = elf.buffer.subarray(0, 4);
    let valid = signature[0] === 0x7f && signature[1] === 0x45 && signature[2] === 0x4c && signature[3] === 0x46;
    if (!valid) {
        console.error('Error: file is not an elf!');
        return;
    }
    elf.cur = 4;
    let archClass = elf.readByte();
    console.log("arch: " + ((archClass === 1) ? 'x32' : 'x64'));

    if (archClass !== ARCH_CLASS_X32) {
        console.error('Error: x64 is not supported');
        return;
    }

    let endian = elf.readByte();

    console.log('endian: ' + (endian === LITTLE_ENDIAN ? 'littleEndian' : 'bigEndian'));
    elf.endian = endian;

    elf.cur = 7;
    let abi = elf.readByte();
    console.log('abi:', abi);

    elf.cur = 0x10;
    let elfType = elf.readWord();
    console.log('elfType:', elfType.toString(16));

    let machine = elf.readWord();
    console.log('machine:', machine.toString(16));
    elf.cur += 4;

    let entryPoint = elf.readInt();
    console.log('entry point:', entryPoint.toString(16));

    let programHeaderOffsetAddress = elf.getCursor();
    let programHeaderOffset = elf.readInt();
    console.log('program header offset:', programHeaderOffset.toString(16));

    let sectionHeaderOffsetAddress = elf.getCursor();
    let sectionHeaderOffset = elf.readInt();
    console.log('section header offset:', sectionHeaderOffset.toString(16));
    let flags = elf.readInt();
    console.log('flags:', flags.toString(2));

    let headerSize = elf.readWord();
    console.log('headerSize:', headerSize);

    let programHeaderSize = elf.readWord();
    let programHeaderCount = elf.readWord();
    console.log('programHeaderSize:', programHeaderSize);
    console.log('programHeaderCount:', programHeaderCount);

    let sectionHeaderSize = elf.readWord();
    let sectionHeaderCount = elf.readWord();
    console.log('sectionHeaderSize:', sectionHeaderSize);
    console.log('sectionHeaderCount:', sectionHeaderCount);

    let programHeader = readProgramHeader(programHeaderOffset, programHeaderCount, elf);
    console.log(programHeader);
    console.log('prog physicalAddress:', programHeader[0].physicalAddress.toString(16));

    let sections = readSectionsHeader(sectionHeaderOffset, sectionHeaderCount, sectionHeaderSize, elf);
    let strtabSection;
    for (let section of sections) {
        if (section.type === 3) {
            strtabSection = section;
        }
    }

    let sectionsByName = {};
    for (let section of sections) {
        elf.cur = strtabSection.offset + section.name;
        section.strName = elf.readString();
        section.data = elf.buffer.subarray(section.offset, section.offset + section.size);
        if (section.strName.length > 0) {
            sectionsByName[section.strName] = section;
        }
    }


    console.log('sections:', sectionsByName);
    console.log('prog.physicalAddress:', programHeader[0].physicalAddress.toString(16));
    console.log('prog.segmentSize:', programHeader[0].segmentSize.toString(16));
    console.log('prog.segmentSizeInMem:', programHeader[0].segmentSizeInMem.toString(16));
    console.log('text.virtualAddress:', sectionsByName['.text'].virtualAddress.toString(16));
    console.log('text.size:', sectionsByName['.text'].size.toString(16));
    console.log('data.virtualAddress:', sectionsByName['.data'].virtualAddress.toString(16));
    console.log('data.size:', sectionsByName['.data'].size.toString(16));
    console.log('data.offset:', sectionsByName['.data'].offset.toString(16));

    ///////////////////////////////////////////////////////////

    let section = sectionsByName['.rel.text']; 
    console.log(section);
    let relData = readRelationSection(section, elf);

    for(let rel of relData){
        if(rel.address == 0xa0b80){
            rel.type = R_MIPS_HI_16;
            let index = relData.indexOf(rel);
            relData.splice(index+1,0,{address : rel.address+4, type: R_MIPS_LO_16});
            break;
        }
    }

    let fixedRelData = writeRelationSection(relData);
    console.log(fixedRelData);
    let sizeDif = fixedRelData.length - section.size;
    section.size = fixedRelData.length;
    section.data = fixedRelData;

    fixSectionOffsets(section, sections, sizeDif);

    if(sectionHeaderOffset > section.offset){
        sectionHeaderOffset += sizeDif;
        fixedElf.setCursor(sectionHeaderOffsetAddress);
        fixedElf.writeInt(sectionHeaderOffset);
    }

    if(programHeaderOffset > section.offset){
        programHeaderOffset += sizeDif;
        fixedElf.setCursor(programHeaderOffsetAddress);
        fixedElf.writeInt(programHeaderOffset);
    }

    let fixedElfBuffer = Buffer.alloc(elf.buffer.length + sizeDif);
    elf.buffer.copy(fixedElfBuffer);
    let fixedElf = new BinaryReader(fixedElfBuffer);

    for(let section of sections){
        section.data.copy(fixedElfBuffer, section.offset);
    }

    patchSectionsHeader(sections, sectionHeaderOffset, fixedElf);

    fs.writeFileSync("EBOOT.BIN", fixedElfBuffer);

    ///////////////////////////////////////////////////////////

    /*let section = sectionsByName['.data'];
    let dataSection = elf.buffer.subarray(section.offset, section.offset + section.size);
    let fixedData = concatBuffers(dataSection, Buffer.from("disk0:/PSP_GAME/USRDIR/font.pgf\0"));
    section.data = fixedData;
    section.size = fixedData.length;
    let sizeDif = fixedData.length - dataSection.length;


    console.log(section);

    let fixedElfBuffer = Buffer.alloc(elf.buffer.length + sizeDif);
    elf.buffer.copy(fixedElfBuffer);
    //fixedElfBuffer.fill(0xdd);
    let fixedElf = new BinaryReader(fixedElfBuffer);

    //programHeader[0].segmentSize += 16;
    //programHeader[0].segmentSizeInMem += 16;
    console.log(programHeader);
    writeProgramHeader(programHeader, programHeaderOffset, fixedElf);

    if(sectionHeaderOffset > section.offset){
        sectionHeaderOffset += sizeDif;
        fixedElf.setCursor(sectionHeaderOffsetAddress);
        fixedElf.writeInt(sectionHeaderOffset);
    }

    if(programHeaderOffset > section.offset){
        programHeaderOffset += sizeDif;
        fixedElf.setCursor(programHeaderOffsetAddress);
        fixedElf.writeInt(programHeaderOffset);
    }
    
    fixSectionOffsets(section, sections, sizeDif);

    for(let section of sections){
        section.data.copy(fixedElfBuffer, section.offset);
    }

    patchSectionsHeader(sections, sectionHeaderOffset, fixedElf);

    fs.writeFileSync("EBOOT.BIN", fixedElfBuffer);*/
}

function patchSectionsHeader(sections, offset, elf){
    elf.setCursor(offset);
    for(let r of sections){
        elf.writeInt(r.name)
        elf.writeInt(r.type);
        elf.writeInt(r.flags);
        elf.writeInt(r.virtualAddress);
        elf.writeInt(r.offset);
        elf.writeInt(r.size);
        elf.writeInt(r.link);
        elf.writeInt(r.info);
        elf.writeInt(r.alligment);
        elf.writeInt(r.entSize);
    }
}

function fixSectionOffsets(resizedSection, sections, diff){
    for(section of sections){
        if(section.offset > resizedSection.offset){
            section.offset += diff;
            console.log("moving section", section.strName, "0x"+section.offset.toString(16));
        }
        /*if(section.virtualAddress > resizedSection.virtualAddress){
            section.virtualAddress += diff;
            console.log("moving virtAddress", section.strName, "0x"+section.virtualAddress.toString(16));
        }*/
    }
}

function concatBuffers(buf1, buf2){
    let result = Buffer.alloc(buf1.length + buf2.length);
    buf1.copy(result);
    buf2.copy(result, buf1.length);
    return result;
}

function writeProgramHeader(programHeader, programHeaderOffset, elf) {
    elf.setCursor(programHeaderOffset);

    for (let r of programHeader) {
        elf.writeInt(r.type);
        elf.writeInt(r.segmentOffset);
        elf.writeInt(r.virtualAddress);
        elf.writeInt(r.physicalAddress);
        elf.writeInt(r.segmentSize);
        elf.writeInt(r.segmentSizeInMem);
        elf.writeInt(r.flags);
        elf.writeInt(r.allign);
    }
}

/**
 *
 * @param programHeaderOffset : int
 * @param programHeaderCount : int
 * @param elf : BinaryReader
 */
function readProgramHeader(programHeaderOffset, programHeaderCount, elf) {
    let a = [];

    elf.cur = programHeaderOffset;

    for (let i = 0; i < programHeaderCount; i++) {
        let r = {};
        r.type = elf.readInt();
        r.segmentOffset = elf.readInt();
        r.virtualAddress = elf.readInt();
        r.physicalAddress = elf.readInt();
        r.segmentSize = elf.readInt();
        r.segmentSizeInMem = elf.readInt();
        r.flags = elf.readInt();
        r.allign = elf.readInt();
        a.push(r);
    }

    return a;
}

/**
 *
 * @param sectionHeaderOffset : int
 * @param sectionHeaderCount : int
 * @param headerSize : int
 * @param elf : BinaryReader
 * @returns Array
 */
function readSectionsHeader(sectionHeaderOffset, sectionHeaderCount, headerSize, elf) {
    let offset = sectionHeaderOffset;
    let a = [];
    for (let i = 0; i < sectionHeaderCount; i++) {
        let r = {};
        elf.cur = offset;
        r.descriptionOffset = elf.cur;

        r.name = elf.readInt();
        r.type = elf.readInt();
        r.flags = elf.readInt();
        r.virtualAddress = elf.readInt();
        r.offset = elf.readInt();
        r.size = elf.readInt();

        r.link = elf.readInt();
        r.info = elf.readInt();

        r.alligment = elf.readInt();
        r.entSize = elf.readInt();

        a.push(r);
        offset += headerSize;
    }
    return a;
}

/**
 *
 * @param section : Object
 * @param elf : BinaryReader
 * @returns {undefined}
 */
function readRelationSection(section, elf) {
    let result = [];

    elf.cur = section.offset;
    while (elf.cur - section.offset < section.size) {
        let o = {};
        o.address = elf.readInt();
        o.type = elf.readInt();
        result.push(o);
    }

    return result;
}

function writeRelationSection(relData) {
    let result = Buffer.alloc(relData.length*8);
    let reader = new BinaryReader(result);
    reader.setCursor(0);
    for(let o of relData){
        reader.writeInt(o.address);
        reader.writeInt(o.type);
    }

    return result;
}

let elfFileName = process.argv[2];
const elf = fs.readFileSync(elfFileName);
parseElfHeader(new BinaryReader(elf));