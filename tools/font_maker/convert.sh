#!/bin/bash

alphabet="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭЮЯабвгдеёжзийклмнопрстуфхцчшщьъыэюяABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,?'\"-!0123456789()";

mkdir out
#convert -background 'rgba(255,255,255,0)' -fill white  -font chinese.msyh.ttf -pointsize 18 label:"Щ" x.png
IFS=$'\n'
for l in `echo $alphabet | sed -e 's/\(.\)/\1\n/g'` ; do
    
    echo $l | hexdump -v -e '/1 "%X"'
    
    x=`echo -n  $l | hexdump -v -e '/1 "%02X"'`  
    
    
    echo $l " = " $x
    
    convert -background 'rgba(255,255,255,0)' -fill white  -font chinese.msyh.ttf -pointsize 18 label:"$l" out/$x.png
done
