#!/bin/bash

if [ $# -gt "0" ] ; then


    cd GimConv
    wine GimConv.exe "$1" -o "$2"

else
    echo "usage: $0 <input gim> <output png>"
fi
