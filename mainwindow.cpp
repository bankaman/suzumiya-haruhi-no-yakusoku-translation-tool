#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "datparser.h"

#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTextCodec>

#include <QClipboard>

QTextCodec* MainWindow::curTextCodec=NULL;
MainWindow* MainWindow::me = NULL;

void showMessageBox(QString message){
    QMessageBox msgBox;
    msgBox.setText(message);
    msgBox.exec();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    MainWindow::me = this;
    ui->setupUi(this);
    jp_codec=QTextCodec::codecForName("Shift-JIS");
    ru_codec=QTextCodec::codecForName("CP1251");
    utf_codec=QTextCodec::codecForName("UTF-8");
    setTextCodec(jp_codec);

    datparser.main_window=this;

    QString configPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/";
    QDir confDir(configPath);
    if(!confDir.exists()){
        confDir.mkdir(configPath);
    }

    //loading config file
    settings = new QSettings(configPath+"config.ini",QSettings::IniFormat);

    //setting up translator helper
    translator.connect(settings->value("words_database",configPath+"/words.db3").toString());

    //create menu fo recent opened files
    QAction* recentFileAction = 0;
    for(int i = 0; i < maxFileNr; i++){
        recentFileAction = new QAction(this);
        recentFileAction->setVisible(false);
        QObject::connect(recentFileAction, SIGNAL(triggered()),
                                             this, SLOT(openRecent()));
        recentFileActionList.append(recentFileAction);
    }
    for(int i = 0; i < maxFileNr; i++)
            ui->menuOpenRecent->addAction(recentFileActionList.at(i));

    updateRecentActionList();

    current_file=settings->value("lastFile", "").toString();
    if(current_file.length()>0){
        openScriptFile(current_file);
        cursor=datparser.dat.next_text_command_id(settings->value("lastCursor",0).toInt()-1);
        QString lastStatePath = configPath+"last_state.htp";
        if(QFile::exists(lastStatePath)){
            datparser.dat.load_patch_from_file(lastStatePath);
        }
        if(cursor!=-1) print_to_ui();
    }
}

MainWindow::~MainWindow()
{
    settings->setValue("lastCursor",cursor);
    settings->sync();
    QString configPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/";
    QString lastStatePath = configPath+"last_state.htp";
    datparser.dat.save_patch_to_file(lastStatePath);

    References::clear();
    delete settings;
    delete ui;
}


void MainWindow::openScriptFile(QString filename){
    update_title(filename);
    if(filename.endsWith(".btd")){
        btdeditor.load_file(filename);
        btdeditor.show();
    }else{
        current_file=filename;
        datparser.dat.setReadCodec(jp_codec);
        datparser.openFile(filename);
        datparser.on_pushButton_start_clicked();
        cursor=datparser.dat.next_text_command_id(-1);
        if(cursor!=-1) print_to_ui();
    }
}


//open recent function
void MainWindow::openRecent(){
    QAction *action = qobject_cast<QAction *>(sender());
        if (action){
            datparser.dat.setReadCodec(jp_codec);
            showMessageBox(action->data().toString());
            openScriptFile(action->data().toString());
        }
}

void MainWindow::updateRecentActionList(){
    QStringList recentFilePaths =
            settings->value("recentFiles").toStringList();

    int itEnd = 0;
    if(recentFilePaths.size() <= maxFileNr)
        itEnd = recentFilePaths.size();
    else
        itEnd = maxFileNr;

    for (int i = 0; i < itEnd; i++) {
        QString strippedName = QFileInfo(recentFilePaths.at(i)).fileName();
        recentFileActionList.at(i)->setText(strippedName);
        recentFileActionList.at(i)->setData(recentFilePaths.at(i));
        recentFileActionList.at(i)->setVisible(true);
    }

    for (int i = itEnd; i < maxFileNr; i++)
        recentFileActionList.at(i)->setVisible(false);
}

void MainWindow::update_title(QString filename)
{
    QFileInfo info(filename);

    setWindowTitle(info.fileName()+" - Suzumiya Haruhi no Yakusoku Translation Tool");
}

void MainWindow::gotoId(int id)
{
    cursor = id;
    print_to_ui();
}

void MainWindow::updateTranslationHelper()
{
    on_buttonTHConfig_clicked();
}

QSettings *MainWindow::getSettings()
{
    return settings;
}


//add path to recent files
void MainWindow::addToRecentFiles(const QString &filePath){
    current_file=filePath;
    QStringList recentFilePaths = settings->value("recentFiles").toStringList();
    recentFilePaths.removeAll(filePath);
    recentFilePaths.prepend(filePath);
    while (recentFilePaths.size() > maxFileNr)
            recentFilePaths.removeLast();
    settings->setValue("recentFiles", recentFilePaths);
    settings->setValue("lastFile",current_file);
    settings->sync();

    updateRecentActionList();
}


//------------------static function to setting up text codec ---------------------
void MainWindow::setTextCodec(QTextCodec *codec){
    MainWindow::curTextCodec=codec;
}


//-------------------------------Loading script file----------------------------------------------

void MainWindow::load_script_file(){
    datparser.on_open_button_clicked();
    datparser.on_pushButton_start_clicked();
    cursor=datparser.dat.next_text_command_id(-1);
    if(cursor!=-1) print_to_ui();
}

void MainWindow::on_actionLoad_obj_triggered()
{
    datparser.dat.setReadCodec(jp_codec);
    load_script_file();
}

//------------------------update translation label --------------

void MainWindow::update_translation_info(){

}
//------------------------print data to ui form------------------
void MainWindow::print_to_ui(){
    int com_ind = datparser.dat.get_text_command_index_by_command_index(cursor)+1;
    int com_count=datparser.dat.get_text_commands_count();
    if (com_count<=0)
        return;
    int precent = com_ind*100/com_count;
    ui->label_info->setText("№: "+QString::number(com_ind)+"/"+QString::number(datparser.dat.get_text_commands_count())+" | ("+QString::number(precent)+"%)\n"+
                            "audio file: \""+datparser.dat.get_audio_name(cursor)+"\"");

    ui->textEdit_source->setText(datparser.dat.get_original_text(cursor));
    ui->textEdit_localized->setText(datparser.dat.get_patched_text(cursor));
}
//---------------------- Previous Record ------------------------
void MainWindow::on_pushButton_prevRec_clicked()
{
    cursor=datparser.dat.prev_text_command_id(cursor);
    print_to_ui();
}

//---------------------- Next Record ------------------------
void MainWindow::on_pushButton_nextRec_clicked()
{
    cursor=datparser.dat.next_text_command_id(cursor);
    print_to_ui();
}


//--------------------------Update text-----------------------
void MainWindow::on_textEdit_localized_textChanged()
{
    QString text=ui->textEdit_localized->toPlainText();
    //if(!text.endsWith("\n")) text.append("\n");
    int count = curTextCodec->fromUnicode(text).length();

    int max_length=156;
    if(count<=max_length){
        ui->label_translate_info->setText("length=<font color=#008800>"+QString::number(count)+"</font>");
    }else{
        ui->label_translate_info->setText("length=<font color=#880000>"+QString::number(count)+"("+QString::number(max_length-count)+")</font>");
    }

    datparser.dat.set_patched_text(cursor, text);
    nl_tester.setText(text);
    if(scriptmap.isVisible()){
        scriptmap.update(datparser.dat);
    }
}

//--------------------------About this program-----------------------
void MainWindow::on_actionAbout_triggered()
{
    about_dialog.show();
}

//--------------------------- Save localized button -------------------------
void MainWindow::on_actionSave_localized_file_triggered()
{
    QString filename;
    filename=QFileDialog::getSaveFileName(0, "Save dat file", "", "Haruhi Script(*.dat)", 0, 0);

    datparser.dat.write_codec=curTextCodec;
    datparser.dat.patch_and_save(filename);
}

//----------------------------------- Saving file -----------------------------
void MainWindow::on_actionSave_triggered()
{
    datparser.dat.write_codec=curTextCodec;
    datparser.dat.patch_and_save(datparser.last_filename);
}

//--------------------------- Open Parser Window -------------------------
void MainWindow::on_actionParser_Window_triggered()
{
    datparser.show();
}


//---------------------------- Cyrillic -> Latin ------------------------

QString MainWindow::toLatin(QString string){
    QString c,outstr;
    outstr="";
    for(int i=0;i<string.length();i++){
            c="";
            c.append(string.at(i));

            if(c=="А") outstr.append('A'); else
            if(c=="В") outstr.append('B'); else
            if(c=="Е") outstr.append('E'); else
            if(c=="К") outstr.append('K'); else
            if(c=="М") outstr.append('M'); else
            if(c=="Н") outstr.append('H'); else
            if(c=="О") outstr.append('O'); else
            if(c=="Р") outstr.append('P'); else
            if(c=="С") outstr.append('C'); else
            if(c=="Т") outstr.append('T'); else
            if(c=="Х") outstr.append('X'); else

            if(c=="а") outstr.append('a'); else
            if(c=="е") outstr.append('e'); else
            if(c=="о") outstr.append('o'); else
            if(c=="р") outstr.append('p'); else
            if(c=="с") outstr.append('c'); else
            if(c=="у") outstr.append('y'); else
            if(c=="х") outstr.append('x'); else


            outstr.append(string.at(i));

    }
    return outstr;
}

QString MainWindow::getCurrentLocalizedText()
{
    return ui->textEdit_localized->toPlainText();
}

QString MainWindow::getCurrentLocalizedTextStatic()
{
    return me->getCurrentLocalizedText();
}

void MainWindow::on_pushButton_compress_clicked()
{
    QString string;
    string=toLatin(ui->textEdit_localized->toPlainText());

    ui->textEdit_localized->setText(string);
}

//---------------------------Save Patch element to file -----------------------
void MainWindow::on_actionSave_patch_to_file_triggered()
{
    QString filename;
    filename=QFileDialog::getSaveFileName(0, "Save text patch", "", "Haruhi Text Patch(*.htp)", 0, 0);
    if(!filename.endsWith(".htp")) filename.append(".htp");

    datparser.dat.save_patch_to_file(filename);
}

void MainWindow::on_actionLoad_patch_from_file_triggered()
{
    QString filename;
    filename=QFileDialog::getOpenFileName(0, "Open text patch", "", "Haruhi Text Patch(*.htp)", 0, 0);

    datparser.dat.load_patch_from_file(filename);
}

//------------------------- To Shift-JIS --------------------------

void MainWindow::on_actionShift_JIS_to_hex_triggered()
{
    shiftjiscoder.show();
}

//-------------------------------- Configuration for Translation Helper ----------------
void MainWindow::on_buttonTHConfig_clicked()
{
    translator_form.init(settings,&translator,ui->textEdit_source->toPlainText());
    translator_form.show();
}

void MainWindow::on_MainWindow_destroyed()
{

}

void MainWindow::on_actionCopy_source_to_clipboard_triggered()
{

    QApplication::clipboard()->setText(ui->textEdit_source->toPlainText());

    datparser.showMessageBox("Copied to clipboard");
}

void MainWindow::on_actionNew_Line_Tester_triggered()
{
    nl_tester.setText(ui->textEdit_localized->toPlainText());
    nl_tester.show();
}

//-------------------------- Split Button -----------------------

void MainWindow::on_pushButton_split_clicked()
{
    QMessageBox::StandardButton r = QMessageBox::question(this,"Splitting","Do you realy want to split this dialog?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No );
    if (r == QMessageBox::No)
        return;

    QStringList str = ui->textEdit_localized->toPlainText().split("\n");
    QString text1,text2;

    for(int i=0;i<str.length();i++){
        if(i<3){
            text1+=str.at(i)+"\n";
        }else{
            text2+=str.at(i)+"\n";
        }
    }
    datparser.dat.set_patched_text(cursor,text1);
    datparser.dat.create_new_text_command(cursor,text2);
    print_to_ui();
}

//----------------------- reset patch list -----------------------
void MainWindow::on_actionReset_translation_triggered()
{
    datparser.dat.clear_patch_info();
    print_to_ui();
}

//-------------------------------- open BTD editor -------------------------
void MainWindow::on_actionBTD_editor_triggered()
{
    btdeditor.show();
}

void MainWindow::on_actionReferences_triggered()
{
    references.show();
    references.update_data();
}

void MainWindow::on_actionScript_Map_triggered()
{
    scriptmap.update(datparser.dat);
    scriptmap.show();
}
