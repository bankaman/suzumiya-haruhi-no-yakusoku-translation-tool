#!/bin/bash

if [ ! -f 'build/Makefile' ] ; then
    mkdir build
    t=$(pwd)
    cd build
    cmake ..
    cd "$t"
fi

make -C build
