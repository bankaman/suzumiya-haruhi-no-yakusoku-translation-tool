set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

set colorcolumn=110
highlight ColorColumn ctermbg=darkgray

let &path.="../,"

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


set makeprg=make\ -C\ build\ -j9

nnoremap <F4> :make!<cr>
nnoremap <F5> :! build/yakusoku-tr '../translated files/PSP_GAME/USRDIR/data/script/s_har1603.dat'<cr>

nnoremap <F2> :wa<Cr>


" path to directory where library can be found
let g:clang_library_path='/usr/lib/llvm-6.0/lib'
" or path directly to the library file
let g:clang_library_path='/usr/lib/x86_64-linux-gnu/libclang-7.so.1'
