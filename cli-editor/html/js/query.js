//Запросы ----------------------------------------------------------------------------------------------------------------

function api_get_files_list(path, callback){
	var query = {
      command : 'get_files_list',
      params : {
		  path : path
	  }
  };
  send_api_query(query, function(r){
		callback(r.result);
	});
}

function api_open_file(path, callback){
	var query = {
      command : 'open_file',
      params : {
		  path : path
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_open_btd_file(path, callback){
	var query = {
      command : 'open_btd_file',
      params : {
		  path : path
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}


function api_close_file(id, callback){
	var query = {
      command : 'close_file',
      params : {
		  id : Number(id)
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_save_btd_file(id, callback){
	var query = {
      command : 'save_btd_file',
      params : {
		  id : Number(id)
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_close_btd_file(id, callback){
	var query = {
      command : 'close_btd_file',
      params : {
		  id : Number(id)
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_save_file(id, callback){
	var query = {
      command : 'save_file',
      params : {
		  id : Number(id)
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_get_list_opened_files(callback){
	var query = {
      command : 'get_list_opened_files',
      params : {}
  };
  send_api_query(query, function(r){
		callback(r.result);
	});
}

function api_get_file_map(id,source,callback){
	//console.log('id:'+id);
	var query = {
      command : 'get_file_map',
      params : {
		  id : Number(id),
		  map_from_source : source
	  }
  };
  send_api_query(query, function(r){
		//console.log(r.result);
		callback(r.result);
	});
}

function api_get_line_info(file_id,line_id,callback){
	//console.log('id:'+id);
	var query = {
      command : 'get_line_info',
      params : {
		  file_id : Number(file_id),
		  line_id : Number(line_id)
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_edit_dialog(file_id,line_id,newText,callback){
	//console.log('id:'+id);
	var query = {
      command : 'edit_dialog',
      params : {
		  file_id : Number(file_id),
		  dialog_id : Number(line_id),
		  new_text : newText
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_split_dialog(file_id,line_id,callback){
	var query = {
      command : 'split_dialog',
      params : {
		  file_id : Number(file_id),
		  dialog_id : Number(line_id),
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_thelper_add_word(jp_text,ru_text,callback){
	var query = {
      command : 'thelper_add_word',
      params : {
		  jp_text : jp_text,
		  ru_text : ru_text
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_thelper_del_word(rowid,callback){
	var query = {
      command : 'thelper_del_word',
      params : {
		  rowid : rowid
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_thelper_edit_pronounciation(rowid, pronounciation, callback){
	var query = {
      command : 'thelper_edit_pronounciation',
      params : {
		  rowid : rowid,
          pronounciation: pronounciation
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_thelper_translate(text,callback){
	var query = {
      command : 'thelper_translate',
      params : {
		  text : text
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_get_btd_tree(id,callback){
	var query = {
      command : 'get_btd_tree',
      params : {
		  id : Number(id)
	  }
  };
  send_api_query(query, function(r){
		callback(r.result);
	});
}

function api_set_btd_theme_text(file_id, theme_id, text, callback){
	var query = {
      command : 'set_btd_theme_text',
      params : {
		  file_id : Number(file_id),
		  theme_id : Number(theme_id),
		  text : text
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

function api_set_btd_variant_text(file_id, theme_id, variant_id, text, callback){
	var query = {
      command : 'set_btd_variant_text',
      params : {
		  file_id : Number(file_id),
		  theme_id : Number(theme_id),
		  variant_id : Number(variant_id),
		  text : text
	  }
  };
  send_api_query(query, function(r){
		callback(r);
	});
}

//api query
function send_api_query(query, callback){
	send_query("/api",JSON.stringify(query),function(text){
			var responce = JSON.parse(text);
			if(responce.error)
				return show_error(responce.error);
			callback(responce);
	});
}

function show_error(e){
	alert(e);
}

//Отправка запросаresponce.errorresponce.error
function send_query(filename,query,after_complete){

	var xmlhttp = getXmlHttp(); // Создаём объект XMLHTTP
	xmlhttp.open('POST', filename, true); // Открываем асинхронное соединение
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Отправляем кодировку
	xmlhttp.send(query);

	//t=outP.innerHTML;
	query_in_process=true;
	xmlhttp.onreadystatechange = function() { // Ждём ответа от сервера
		if (xmlhttp.readyState == 4) { // Ответ пришёл
			query_in_process=false;
			if(xmlhttp.status == 200) { // Сервер вернул код 200 (что хорошо)
				//console.log(xmlhttp.responseText); // Выводим ответ сервера
				//outP.innerHTML=outputVar+xmlhttp.responseText;
				/*if(!xmlhttp.responseText){
					return;
				}
				try{
					data = JSON.parse(xmlhttp.responseText);
				}catch(e){
					show_error(xmlhttp.responseText);
					return;
				}

				if(!data){
					show_error(xmlhttp.responseText);
					return;
				}

				if(data.errors.length)
					show_error(JSON.stringify(data.errors));*/

				if(after_complete!=null) {
				    //after_complete(data.text);
				    after_complete(xmlhttp.responseText);
			    }


			}else show_error("Ошибка при отправке запроса "+xmlhttp.status);
		}
	}
}

function getXmlHttp() {
	var xmlhttp;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}
