#include "webserver.h"
#include <QMimeType>
#include <QMimeDatabase>
#include "api.h"

#ifdef WIN32
#include "io.h"
#endif

/*
 * webserver module
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

WebServer::WebServer()
{

}

WebServer::~WebServer()
{
    stop();
    this->wait();
}



MHD_Result WebServer::answer_to_connection(void *, MHD_Connection *connection, const char *url, const char *method, const char *, const char *upload_data, size_t *upload_data_size, void **con_cls)
{
    QByteArray buffer;
    bool from_file=true;
    int error = MHD_HTTP_OK;

    MHD_Result ret;

    struct MHD_Response *response;
    //struct MHD_PostProcessor *pp;

    struct iter *it;

    QString mime = "text/html";
    QString filename = url;

    //api processing
    if((strcmp(url,"/api/")==0)||(strcmp(url,"/api")==0)){
        if(0==strcmp(method,"POST")){

            mime = "application/json";

            from_file=false;

            it=(iter*)*con_cls;
            if(NULL==it){
                it=new iter;
                *con_cls = it;
                it->iter=0;
                it->buffer.clear();
                return MHD_YES;
            }
            it->iter++;
            if (0 != *upload_data_size){
                  it->buffer.append(upload_data, (long int)*upload_data_size);
                  *upload_data_size = 0;
                  return MHD_YES;
            }
            API::process(buffer,it->buffer);
            delete it;
        }else{
            filename="/errors/use_post.html";
        }
    }
    //static files
    if(from_file){

        if(filename == "/")
            filename = "/index.html";
		filename = "html"+filename;
        QFile fpage(filename);
        if(!fpage.exists()){
            qDebug()<<"Error 404: "<<filename;
            error = MHD_HTTP_NOT_FOUND;
        }else{
            if(filename.endsWith(".css"))
                mime = "text/css";
            if(filename.endsWith(".svg"))
                mime = "image/svg+xml";
            if(filename.endsWith(".jpg"))
                mime = "image/jpeg";
            if(filename.endsWith(".js"))
                mime = "application/javascript";

            fpage.open(QIODevice::ReadOnly);
            buffer = fpage.readAll();
            fpage.close();
        }
    }
	
	//uploaded files
    /*if(filename.startsWith("/files/")){
        QString file_id_str;
        file_id_str=filename.mid(filename.lastIndexOf("/")+1);
        bool ok=true;
        int file_id = file_id_str.toInt(&ok);
        if(ok){
            qDebug()<<"file_id:"<<file_id;
            LocalDatabase db;
            QString fname = db.get_file_path_by_id(file_id);
            if(fname.length()!=0){
                QFile file(fname);
                if(file.exists()){
                    if(file.open(QIODevice::ReadOnly)){
                        qDebug()<<"Webserver has opened file: "<<fname<<"size:"<<file.size()<<"handle:"<<file.handle();
                        int handle = file.handle();
                        response = MHD_create_response_from_fd(file.size(),dup(handle));
                        QMimeDatabase db;
                        QMimeType mime = db.mimeTypeForFile(fname);
                        qDebug()<<"mime is:"<<mime.name();
                        MHD_add_response_header(response, "Content-Type", mime.name().toStdString().c_str());
                        ret = MHD_queue_response(connection, error, response);
                        MHD_destroy_response (response);
                        file.close();
                        return ret;
                    }
                }
            }
        }
    }*/

    if(error==MHD_HTTP_NOT_FOUND){
        QFile fpage("html/errors/404.html");
        fpage.open(QIODevice::ReadOnly);
        buffer = fpage.readAll();
        fpage.close();
    }

    //forming page
    void *p = malloc(buffer.length());
    memcpy(p,buffer.data(),buffer.length());


    response =
    MHD_create_response_from_buffer (buffer.length(), p,
                     MHD_RESPMEM_MUST_FREE);
    MHD_add_response_header(response, "Content-Type", mime.toStdString().c_str());
    ret = MHD_queue_response (connection, error, response);
    MHD_destroy_response (response);

    return ret;
}

void WebServer::run()
{
    daemon = MHD_start_daemon (MHD_USE_SELECT_INTERNALLY, port, NULL, NULL,
    &WebServer::answer_to_connection, NULL, MHD_OPTION_END);
    if (NULL == daemon){
        qDebug()<<"error starting web server";
        return;
    }
    do_run = true;
    while (do_run) {
        sleep(1);
    }

    MHD_stop_daemon (daemon);
}

void WebServer::stop()
{
    do_run = false;
}

void WebServer::set_port(int port)
{
    this->port = port;
}

QString WebServer::getFilename(QString url)
{
    return url.mid(url.lastIndexOf("/"));
}
