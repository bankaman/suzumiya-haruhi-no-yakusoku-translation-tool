#ifndef GLOB_H
#define GLOB_H

#include <QVector>

#include <datfile.h>
#include <btdfile.h>
#include <translationhelper.h>
#include <QSettings>

class Glob
{
public:
    static QVector<DatFile*> opened_files;
	static QVector<BtdFile*> opened_btd_files;
	static void init();
	static void destroy();

	static TranslationHelper *thelp;
	static QSettings *settings;
};

#endif // GLOB_H