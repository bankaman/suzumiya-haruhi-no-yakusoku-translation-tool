#include "DatEditor.h"

#include <iostream>

#include <QString>
#include <QDebug>



QString DatEditor::read_string(){
	QString out;

	std::string input;
	std::getline(std::cin,input);
	out = QString(input.c_str());
    return out;
}

void DatEditor::draw_script_map()
{
    printf("%s",dat.get_map(false,false,true).toStdString().c_str());
}

void DatEditor::script_map_menu()
{
    bool exit = false;
    while(!exit){
        draw_script_map();

        qDebug()<<"_____________________________";
        qDebug()<<"commands: edit [id], save, exit";
        QString inp = read_string();
        if(inp == "ex") //exit
            exit=true;
        else
        if(inp.startsWith("ed")){  //edit
            QStringList list = inp.split(" ");
            if(list.length()<2){
                qDebug()<<"Error: You should enter id of a text line!";
                read_string();
                continue;
            }
            bool ok;
            int id = list.at(1).toInt(&ok);
            if(!ok){
                qDebug()<<"Error: id should be an integer number!";
                read_string();
                continue;
            }
            edit_line_menu(id);

        }else if(inp.startsWith("s")){  //save
            save_file();
        }
    }
}

void DatEditor::edit_line_menu(int id)
{
    bool exit=false;
    while(!exit){

        QString orig_text = dat.get_original_text(id);
        QString tran_text = dat.get_patched_text(id);
		QString helper_out = thelp.translate(orig_text);

        int text_id = dat.get_text_command_index_by_command_index(id)+1;
        int text_cnt = dat.get_text_commands_count();
        qDebug()<<"id:"<<id<<"| "<<text_id<<"/"<<text_cnt<<"("<<(100*text_id/text_cnt)<<"% )";
        qDebug()<<"voice:"<<dat.get_audio_name(id);
        printf(
				"\n%s\n\n%s\n\n%s\n\n______________________\n",
				orig_text.toStdString().c_str(),
				tran_text.toStdString().c_str(),
				helper_out.toStdString().c_str()
			);

        qDebug()<<"commands: save, exit, prev, next";
        QString com = read_string();

        if(com.startsWith("e")){ //exit
            exit=true;
        }else if(com.startsWith("n")){ //next
            id = dat.next_text_command_id(id);
        }else if(com.startsWith("p")){ //prev
            id = dat.prev_text_command_id(id);
        }else if(com.startsWith("s")){ //save
            save_file();
        }

    }
}

void DatEditor::save_file()
{
    if(!dat.patch_and_save(filename)){
        qDebug()<<"Save Error!"<<dat.get_and_clear_log();
        read_string();
    }
}

void DatEditor::run(QString filename){
    dat.setReadCodec(QTextCodec::codecForName("Shift-JIS"));
    if(!dat.load_file(filename)){
        qDebug()<<"Load Error!"<<dat.get_and_clear_log();
        return;
    }
    if(!dat.parse_all()){
		qDebug()<<"Parse Error!"<<dat.get_and_clear_log();
		return;
	}

	settings = new QSettings("config.ini",QSettings::IniFormat);
	thelp.connect(settings->value("words_database","words.db3").toString());	

    this->filename = filename;

    script_map_menu();

	delete settings;
}


