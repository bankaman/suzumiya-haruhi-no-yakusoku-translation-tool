#ifndef DATEDITOR_H
#define DATEDITOR_H

#include <QSettings>

#include "../datfile.h"
#include "../translationhelper.h"

class DatEditor
{
public:
    void run(QString filename);
	
	static QString read_string();

private:
	DatFile dat;
    QString filename;
    TranslationHelper thelp;
    QSettings *settings;

    void draw_script_map();

    void script_map_menu();
    void edit_line_menu(int id);

    void save_file();
};

#endif
