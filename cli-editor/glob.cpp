#include "glob.h"
#include <QDebug>

QVector<DatFile*> Glob::opened_files;
QVector<BtdFile*> Glob::opened_btd_files;
TranslationHelper *Glob::thelp;
QSettings *Glob::settings;

void Glob::init(){
	qDebug()<<"glob init start";
	settings = new QSettings("config.ini",QSettings::IniFormat);
	thelp = new TranslationHelper();
	
	qDebug()<<"glob init ok";
}

void Glob::destroy(){
	delete thelp;
	delete settings;
	qDebug()<<"glob destroy ok";
}
	