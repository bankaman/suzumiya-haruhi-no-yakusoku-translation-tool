#include <stdio.h>
#include <QString>
#include <QDebug>

#include "DatEditor.h"
#include "webserver.h"
#include "glob.h"

int main (int argc, char** argv){
    if(argc<2){
        qDebug()<<"Ussage: yakusoku-tr <filename.dat>";
		qDebug()<<"Ussage in server mode, where 3000 is server port : yakusoku-tr -S 3000 ";
		return 0;
    }
	
	if(strcmp(argv[1],"-S") == 0){
		Glob::init();
		WebServer *server = new WebServer();
		
		QString port_s = QString(argv[2]);
		server->set_port(port_s.toInt());
		
		server->start();
		
		bool quit=false;
		QString str;
		while(!quit){
			str = DatEditor::read_string();
			if(str == "q"){
				quit=true;
				server->stop();
			}
		}
		Glob::destroy();
		delete server;
		return 0;
	}

    QString filename = argv[1];

    qDebug()<<"filename: "<<filename;

	DatEditor editor;
	editor.run(filename);
    return 0;
}
