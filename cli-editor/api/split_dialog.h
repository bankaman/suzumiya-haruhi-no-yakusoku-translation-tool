#ifndef API_SPLIT_DIALOG_H
#define API_SPLIT_DIALOG_H

#include <api.h>

namespace api {
class split_dialog : public API_command
{
public:
    split_dialog();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_split_dialog;

}

#endif // API_EDIT_DIALOG_H
