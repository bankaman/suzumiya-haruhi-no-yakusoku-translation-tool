#include "thelper_translate.h"

#include <glob.h>

using namespace api;
thelper_translate::thelper_translate()
{
    API::register_command("thelper_translate",this);
}

void thelper_translate::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "text"))
        return;
	
	Glob::thelp->connect(Glob::settings->value("words_database","words.db3").toString());
	
	QString text = param.value("text").toString();
	
	QString out = Glob::thelp->translate2(text);
	
	QJsonArray links;
	for(int i=0;i<Glob::thelp->links.length();i++){
		links.append(QJsonObject({
			{"rowid", Glob::thelp->links[i]->rowid},
			{"source", Glob::thelp->links[i]->source},
			{"translated", Glob::thelp->links[i]->translated},
			{"pronunciation", Glob::thelp->links[i]->pronunciation}
		}));
	}
	
	Glob::thelp->closeDB();
    
    QJsonObject obj = {
		{"result", out},
		{"links", links},
        {"error",""}
    };
    return send(buffer, obj);
}
