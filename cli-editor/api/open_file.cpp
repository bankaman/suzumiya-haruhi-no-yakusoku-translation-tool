#include "open_file.h"

#include <QTextCodec>

#include <glob.h>
#include <datfile.h>

using namespace api;
open_file::open_file()
{
    API::register_command("open_file",this);
}

void open_file::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "path"))
        return;
	
	
	DatFile *dat = new DatFile();
	dat->setReadCodec(QTextCodec::codecForName("Shift-JIS"));
	dat->setWriteCodec(QTextCodec::codecForName("Shift-JIS"));
	
	QString path = param.value("path").toString();
	
	if(!dat->load_file(path)){
		QString log = "Load Error!" + dat->get_and_clear_log();
		delete dat;
        return error(buffer, log);
    }
	
	if(!dat->parse_all()){
		QString log = "Parse Error!" + dat->get_and_clear_log();
		delete dat;
        return error(buffer, log);
	}
	
	int id=-1;
	
	for(int i=0;i<Glob::opened_files.length();i++){
		if(Glob::opened_files.at(i)==nullptr){
			id=i;
			continue;
		}
		if(Glob::opened_files.at(i)->getFileName()==path){
			delete dat;
			return error(buffer, "this file already opened as id:"+QString::number(i));
		}
	}
	
	if(id<0){
		id = Glob::opened_files.length();
		Glob::opened_files.append(dat);
	}else{
		Glob::opened_files.replace(id,dat);
	}
	
	QString log = dat->get_and_clear_log();
	
	QJsonObject obj = {
        {"id", id},
		{"log", log},
        {"error",""}
    };
    return send(buffer, obj);
}
