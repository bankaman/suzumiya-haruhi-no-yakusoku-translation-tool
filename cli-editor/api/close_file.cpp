#include "close_file.h"

#include <glob.h>
#include <datfile.h>

using namespace api;
close_file::close_file()
{
    API::register_command("close_file",this);
}

void close_file::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "id"))
        return;
	
	int id = param.value("id").toInt();
	if(id<0 || id>=Glob::opened_files.length())
		return error(buffer, "wrong file id");
	
	DatFile *dat = Glob::opened_files.at(id);
	
	if(dat==nullptr)
		return error(buffer, "wrong file id");
	
	delete dat;
	Glob::opened_files.replace(id,nullptr);
		
    return ok(buffer);
}
