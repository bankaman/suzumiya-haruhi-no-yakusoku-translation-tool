#ifndef API_GET_BTD_TREE_H
#define API_GET_BTD_TREE_H

#include <api.h>

namespace api {
class get_btd_tree : public API_command
{
public:
    get_btd_tree();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_get_btd_tree;

}

#endif // API_GET_BTD_TREE_H
