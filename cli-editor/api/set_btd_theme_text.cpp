#include "set_btd_theme_text.h"

#include <glob.h>
#include <btdfile.h>

using namespace api;
set_btd_theme_text::set_btd_theme_text()
{
    API::register_command("set_btd_theme_text",this);
}

void set_btd_theme_text::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "file_id,theme_id,text"))
        return;
	
	int id = param.value("file_id").toInt();
	if(id<0 || id>=Glob::opened_btd_files.length())
		return error(buffer, "wrong file id");
	
	BtdFile *btd = Glob::opened_btd_files.at(id);
	
	if(btd==nullptr)
		return error(buffer, "wrong file id");
	
	int theme_id = param.value("theme_id").toInt();
	QString text = param.value("text").toString();
	
	btd->set_theme_text(theme_id, text);
	
	QByteArray t_ba = btd->text_codec->fromUnicode(text);
	int bytes_left = btd->get_theme_max_size(theme_id)-t_ba.length();
	
    QJsonObject obj = {
        {"bytes_left", bytes_left},
        {"error",""}
    };
    return send(buffer, obj);
}
