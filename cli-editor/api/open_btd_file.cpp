#include "open_btd_file.h"

#include <glob.h>

using namespace api;
open_btd_file::open_btd_file()
{
    API::register_command("open_btd_file",this);
}

void open_btd_file::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "path"))
        return;
	
	
	BtdFile *btd = new BtdFile();
	
	QString path = param.value("path").toString();
	
	qDebug()<<path;
	
	if(!btd->load(path)){
		QString log = "Load Error!";
		delete btd;
        return error(buffer, log);
    }
	//qDebug()<<"Load btd - ok";
	int id=-1;
	
	for(int i=0;i<Glob::opened_btd_files.length();i++){
		if(Glob::opened_btd_files.at(i)==nullptr){
			id=i;
			continue;
		}
		if(Glob::opened_btd_files.at(i)->getFileName()==path){
			delete btd;
			return error(buffer, "this file already opened as id:"+QString::number(i));
		}
	}
	//qDebug()<<"id search - ok id: "<<id;
	if(id<0){
		id = Glob::opened_btd_files.length();
		Glob::opened_btd_files.append(btd);
	}else{
		Glob::opened_btd_files.replace(id,btd);
	}
	
	//qDebug()<<"append - ok id: "<<id;
	
	QJsonObject obj = {
        {"id", id},
        {"error",""}
    };
    return send(buffer, obj);
}
