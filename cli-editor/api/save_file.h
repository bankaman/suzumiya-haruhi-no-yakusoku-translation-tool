#ifndef API_SAVE_FILE_H
#define API_SAVE_FILE_H

#include <api.h>

namespace api {
class save_file : public API_command
{
public:
    save_file();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_save_file;

}

#endif // API_SAVE_FILE_H
