#ifndef API_OPEN_FILE_H
#define API_OPEN_FILE_H

#include <api.h>

namespace api {
class open_file : public API_command
{
public:
    open_file();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_open_file;

}

#endif // API_OPEN_FILE_H
