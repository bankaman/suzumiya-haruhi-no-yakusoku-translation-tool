#ifndef API_CLOSE_BTD_FILE_H
#define API_CLOSE_BTD_FILE_H

#include <api.h>

namespace api {
class close_btd_file : public API_command
{
public:
    close_btd_file();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_close_btd_file;

}

#endif // API_CLOSE_BTD_FILE_H
