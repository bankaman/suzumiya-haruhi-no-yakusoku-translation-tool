#include "get_file_map.h"

#include <glob.h>
#include <datfile.h>

using namespace api;
get_file_map::get_file_map()
{
    API::register_command("get_file_map",this);
}

void get_file_map::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "id,map_from_source"))
        return;
	
	
	int id = param.value("id").toInt();
	bool map_from_source = param.value("map_from_source").toBool();
	//qDebug()<<"map_from_source:"<<map_from_source;
	if(id<0 || id>=Glob::opened_files.length())
		return error(buffer, "wrong file id");
	
	DatFile *dat = Glob::opened_files.at(id);
	
	if(dat==nullptr)
		return error(buffer, "wrong file id");
	
	QString map = dat->get_map(true,map_from_source,true);
	
	QJsonObject obj = {
        {"result", map},
        {"error",""}
    };
    return send(buffer, obj);
}
