#ifndef API_SET_BTD_VARIANT_TEXT_H
#define API_SET_BTD_VARIANT_TEXT_H

#include <api.h>

namespace api {
class set_btd_variant_text : public API_command
{
public:
    set_btd_variant_text();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_set_btd_variant_text;

}

#endif // API_SET_BTD_VARIANT_TEXT_H
