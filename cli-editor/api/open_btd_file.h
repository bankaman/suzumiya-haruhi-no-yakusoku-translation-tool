#ifndef API_OPEN_BTD_FILE_H
#define API_OPEN_BTD_FILE_H

#include <api.h>

namespace api {
class open_btd_file : public API_command
{
public:
    open_btd_file();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_open_btd_file;

}

#endif // API_OPEN_BTD_FILE_H
