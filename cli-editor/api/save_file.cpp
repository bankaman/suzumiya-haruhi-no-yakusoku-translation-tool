#include "save_file.h"

#include <glob.h>
#include <datfile.h>

using namespace api;
save_file::save_file()
{
    API::register_command("save_file",this);
}

void save_file::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "id"))
        return;
	
	int id = param.value("id").toInt();
	if(id<0 || id>=Glob::opened_files.length())
		return error(buffer, "wrong file id");
	
	DatFile *dat = Glob::opened_files.at(id);
	
	if(dat==nullptr)
		return error(buffer, "wrong file id");
	
	QString filename = dat->getFileName();
	qDebug()<<"filename:"<<filename;
	if(!dat->patch_and_save(filename)){
		qDebug()<<"error:";
		QString err = dat->get_and_clear_log();
		qDebug()<<err;
		return error(buffer,err);
	}
		
    return ok(buffer);
}
