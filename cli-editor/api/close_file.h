#ifndef API_CLOSE_FILE_H
#define API_CLOSE_FILE_H

#include <api.h>

namespace api {
class close_file : public API_command
{
public:
    close_file();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_close_file;

}

#endif // API_CLOSE_FILE_H
