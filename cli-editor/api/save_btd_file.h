#ifndef API_SAVE_BTD_FILE_H
#define API_SAVE_BTD_FILE_H

#include <api.h>

namespace api {
class save_btd_file : public API_command
{
public:
    save_btd_file();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_save_btd_file;

}

#endif // API_SAVE_BTD_FILE_H
