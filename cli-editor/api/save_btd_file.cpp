#include "save_btd_file.h"

#include <glob.h>
#include <btdfile.h>

using namespace api;
save_btd_file::save_btd_file()
{
    API::register_command("save_btd_file",this);
}

void save_btd_file::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "id"))
        return;
	
	int id = param.value("id").toInt();
	if(id<0 || id>=Glob::opened_btd_files.length())
		return error(buffer, "wrong file id");
	
	BtdFile *btd = Glob::opened_btd_files.at(id);
	
	if(btd==nullptr)
		return error(buffer, "wrong file id");
	
	QString filename = btd->getFileName();
	qDebug()<<"filename:"<<filename;
	if(!btd->save(filename)){
		QString err = "error saving btd";
		return error(buffer,err);
	}
		
    return ok(buffer);
}
