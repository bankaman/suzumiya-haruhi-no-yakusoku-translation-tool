#ifndef API_THELPER_ADD_WORD_H
#define API_THELPER_ADD_WORD_H

#include <api.h>

namespace api {
class thelper_add_word : public API_command
{
public:
    thelper_add_word();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_thelper_add_word;

}

#endif // API_THELPER_ADD_WORD_H
