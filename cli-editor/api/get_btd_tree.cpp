#include "get_btd_tree.h"

#include <glob.h>
#include <btdfile.h>

using namespace api;
get_btd_tree::get_btd_tree()
{
    API::register_command("get_btd_tree",this);
}

void get_btd_tree::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "id"))
        return;
	
	int id = param.value("id").toInt();
	if(id<0 || id>=Glob::opened_btd_files.length())
		return error(buffer, "wrong file id");
	
	BtdFile *btd = Glob::opened_btd_files.at(id);
	
	if(btd==nullptr)
		return error(buffer, "wrong file id");
	
	QJsonArray ar;
	
	QStringList themes = btd->get_themes();
	for(int i=0 ; i< themes.length();i++){
		QJsonArray var;
		QStringList variants = btd->get_variants(i); 
		for(int j=0 ; j< variants.length();j++){
			var.append(QJsonObject({
				{"variant",j},
				{"text",variants[j]},
				{"file_name",btd->get_variant_filename(i,j)}
			}));
		}
		ar.append(QJsonObject({
			{"theme",i},
			{"text",themes[i]},
			{"variants",var}
		}));
	}
		
    QJsonObject obj = {
        {"result", ar},
        {"error",""}
    };
    return send(buffer, obj);
}
