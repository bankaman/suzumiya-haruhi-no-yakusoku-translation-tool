#ifndef API_GET_LINE_INFO_H
#define API_GET_LINE_INFO_H

#include <api.h>

namespace api {
class get_line_info : public API_command
{
public:
    get_line_info();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_get_line_info;

}

#endif // API_GET_LINE_INFO_H
