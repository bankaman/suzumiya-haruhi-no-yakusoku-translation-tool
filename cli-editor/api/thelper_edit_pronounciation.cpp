#include "thelper_edit_pronounciation.h"

#include <glob.h>

using namespace api;
thelper_edit_pronounciation::thelper_edit_pronounciation()
{
    API::register_command("thelper_edit_pronounciation",this);
}

void thelper_edit_pronounciation::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "rowid,pronounciation"))
        return;
	
	Glob::thelp->connect(Glob::settings->value("words_database","words.db3").toString());
	
	int rowid = param.value("rowid").toInt();
    QString pronounciation = param.value("pronounciation").toString();
	
	Glob::thelp->updateData(rowid, TranslationHelper::PRONOUNCIATION_FIELD, pronounciation);
	
	Glob::thelp->closeDB();
    
    return ok(buffer);
}
