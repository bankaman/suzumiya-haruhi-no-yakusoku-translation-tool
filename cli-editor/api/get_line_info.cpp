#include "get_line_info.h"

#include <QTextCodec>

#include <glob.h>
#include <datfile.h>

using namespace api;
get_line_info::get_line_info()
{
    API::register_command("get_line_info",this);
}

void get_line_info::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "file_id,line_id"))
        return;
	
	int id = param.value("file_id").toInt();
	if(id<0 || id>=Glob::opened_files.length())
		return error(buffer, "wrong file id");
	
	DatFile *dat = Glob::opened_files.at(id);
	
	if(dat==nullptr)
		return error(buffer, "wrong file id");
	
	int line_id = param.value("line_id").toInt();

	Glob::thelp->connect(Glob::settings->value("words_database","words.db3").toString());
	
	QString original_text = dat->get_original_text(line_id);
	QString patched_text = dat->get_patched_text(line_id);
	QString helper_out = Glob::thelp->translate2(original_text);
	
	QTextCodec *codec = dat->getReadCodec();
	int byte_count = codec->fromUnicode(patched_text).length();

	QJsonArray links;
	for(int i=0;i<Glob::thelp->links.length();i++){
		links.append(QJsonObject({
			{"rowid", Glob::thelp->links[i]->rowid},
			{"source", Glob::thelp->links[i]->source},
			{"translated", Glob::thelp->links[i]->translated},
            {"pronunciation", Glob::thelp->links[i]->pronunciation}
		}));
	}

	Glob::thelp->closeDB();
	
    QJsonObject obj = {
        {"original_text", original_text},
		{"patched_text", patched_text},
		{"helper", helper_out},
		{"voice", dat->get_audio_name(line_id)},
		{"byte_count", byte_count},
		{"links", links},
        {"error",""}
    };
    return send(buffer, obj);
}
