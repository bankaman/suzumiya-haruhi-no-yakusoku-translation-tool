#ifndef API_THELPER_EDIT_PRONOUNCIATION_H
#define API_THELPER_EDIT_PRONOUNCIATION_H

#include <api.h>

namespace api {
class thelper_edit_pronounciation : public API_command
{
public:
    thelper_edit_pronounciation();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_thelper_edit_pronounciation;

}

#endif // API_THELPER_EDIT_PRONOUNCIATION_H
