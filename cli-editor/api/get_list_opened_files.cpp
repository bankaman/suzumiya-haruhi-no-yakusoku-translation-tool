#include "get_list_opened_files.h"

#include <glob.h>
using namespace api;
get_list_opened_files::get_list_opened_files()
{
    API::register_command("get_list_opened_files",this);
}

void get_list_opened_files::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
		
	QJsonArray ar;
	
	for(int i=0;i<Glob::opened_btd_files.length();i++){
		if(Glob::opened_btd_files.at(i)==nullptr)
			continue;
        ar.append(QJsonObject({
			{"id",QString::number(i)},
			{"file",Glob::opened_btd_files.at(i)->getFileName()},
			{"type","btd"}
		}));
	}
	
    for(int i=0;i<Glob::opened_files.length();i++){
		if(Glob::opened_files.at(i)==nullptr)
			continue;
        ar.append(QJsonObject({
			{"id",QString::number(i)},
			{"file",Glob::opened_files.at(i)->getFileName()},
			{"type","dat"}
		}));
	}
	
	QJsonObject obj = {
        {"result", ar},
        {"error",""}
    };
    return send(buffer, obj);
}
