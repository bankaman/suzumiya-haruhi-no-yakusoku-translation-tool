#ifndef API_EDIT_DIALOG_H
#define API_EDIT_DIALOG_H

#include <api.h>

namespace api {
class edit_dialog : public API_command
{
public:
    edit_dialog();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_edit_dialog;

}

#endif // API_EDIT_DIALOG_H
