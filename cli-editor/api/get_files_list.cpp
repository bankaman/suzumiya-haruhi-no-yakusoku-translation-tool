#include "get_files_list.h"
#include <QDirIterator>

using namespace api;
get_files_list::get_files_list()
{
    API::register_command("get_files_list",this);
}

void get_files_list::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "path"))
        return;
	
    QJsonArray ar;
	QDirIterator it(param.value("path").toString());
	while(it.hasNext()){
		ar.append(it.next());
	}

    QJsonObject obj = {
        {"result", ar},
        {"error",""}
    };
    return send(buffer, obj);
}
