#ifndef GET_FILES_LIST_H
#define GET_FILES_LIST_H

#include <api.h>

namespace api {
class get_files_list : public API_command
{
public:
    get_files_list();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_get_files_list;

}

#endif // GET_API_FUNCTIONS_LIST_H
