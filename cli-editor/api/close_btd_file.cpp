#include "close_btd_file.h"

#include <glob.h>
#include <btdfile.h>

using namespace api;
close_btd_file::close_btd_file()
{
    API::register_command("close_btd_file",this);
}

void close_btd_file::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "id"))
        return;
	
	int id = param.value("id").toInt();
	if(id<0 || id>=Glob::opened_btd_files.length())
		return error(buffer, "wrong file id");
	
	BtdFile *btd = Glob::opened_btd_files.at(id);
	
	if(btd==nullptr)
		return error(buffer, "wrong file id");
	
	delete btd;
	Glob::opened_btd_files.replace(id,nullptr);
		
    return ok(buffer);
}
