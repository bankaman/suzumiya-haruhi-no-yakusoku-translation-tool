#include "thelper_add_word.h"

#include <glob.h>

using namespace api;
thelper_add_word::thelper_add_word()
{
    API::register_command("thelper_add_word",this);
}

void thelper_add_word::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "jp_text,ru_text"))
        return;
	
	Glob::thelp->connect(Glob::settings->value("words_database","words.db3").toString());
	
	QString jp_text = param.value("jp_text").toString();
	QString ru_text = param.value("ru_text").toString();
	
	Glob::thelp->addWord(jp_text,ru_text);
	
	Glob::thelp->closeDB();
    
    return ok(buffer);
}
