#ifndef API_SET_BTD_THEME_TEXT_H
#define API_SET_BTD_THEME_TEXT_H

#include <api.h>

namespace api {
class set_btd_theme_text : public API_command
{
public:
    set_btd_theme_text();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_set_btd_theme_text;

}

#endif // API_SET_BTD_THEME_TEXT_H
