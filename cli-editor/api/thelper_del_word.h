#ifndef API_THELPER_DEL_WORD_H
#define API_THELPER_DEL_WORD_H

#include <api.h>

namespace api {
class thelper_del_word : public API_command
{
public:
    thelper_del_word();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_thelper_del_word;

}

#endif // API_THELPER_DEL_WORD_H
