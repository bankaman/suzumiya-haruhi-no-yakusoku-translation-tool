#include "edit_dialog.h"

#include <glob.h>
#include <datfile.h>

using namespace api;
edit_dialog::edit_dialog()
{
    API::register_command("edit_dialog",this);
}

void edit_dialog::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "file_id,dialog_id,new_text"))
        return;
	
	int id = param.value("file_id").toInt();
	if(id<0 || id>=Glob::opened_files.length())
		return error(buffer, "wrong file id");
	
	DatFile *dat = Glob::opened_files.at(id);
	
	if(dat==nullptr)
		return error(buffer, "wrong file id");
	
	
	int dialog_id = param.value("dialog_id").toInt();
	QString text = param.value("new_text").toString();
	
	QTextCodec *codec = dat->getReadCodec();
	int byte_count = codec->fromUnicode(text).length();
	
	dat->set_patched_text(dialog_id, text);
		
    QJsonObject obj = {
		{"byte_count", byte_count},
        {"error",""}
    };
    return send(buffer, obj);
}
