#ifndef API_GET_FILE_MAP_H
#define API_GET_FILE_MAP_H

#include <api.h>

namespace api {
class get_file_map : public API_command
{
public:
    get_file_map();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_get_file_map;

}

#endif // API_GET_FILE_MAP_H
