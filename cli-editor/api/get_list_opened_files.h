#ifndef API_GET_LIST_OPENED_FILES_H
#define API_GET_LIST_OPENED_FILES_H

#include <api.h>

namespace api {
class get_list_opened_files : public API_command
{
public:
    get_list_opened_files();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_get_list_opened_files;

}

#endif // API_GET_LIST_OPENED_FILES_H
