#include "split_dialog.h"

#include <glob.h>
#include <datfile.h>

using namespace api;
split_dialog::split_dialog()
{
    API::register_command("split_dialog",this);
}

void split_dialog::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "file_id,dialog_id"))
        return;
	
	int id = param.value("file_id").toInt();
	if(id<0 || id>=Glob::opened_files.length())
		return error(buffer, "wrong file id");
	
	DatFile *dat = Glob::opened_files.at(id);
	
	if(dat==nullptr)
		return error(buffer, "wrong file id");
	
	
	int dialog_id = param.value("dialog_id").toInt();
	
	QTextCodec *codec = dat->getReadCodec();
	
	
	QString text = dat->get_patched_text(dialog_id);
	QStringList str = text.split("\n");
	QString text1,text2;

    for(int i=0;i<str.length();i++){
        if(i<3){
            text1+=str.at(i)+"\n";
        }else{
            text2+=str.at(i)+"\n";
        }
    }
    dat->set_patched_text(dialog_id,text1);
    dat->create_new_text_command(dialog_id,text2);
	
	int byte_count = codec->fromUnicode(text1).length();
		
    QJsonObject obj = {
		{"byte_count", byte_count},
        {"error",""}
    };
    return send(buffer, obj);
}
