#include "thelper_del_word.h"

#include <glob.h>

using namespace api;
thelper_del_word::thelper_del_word()
{
    API::register_command("thelper_del_word",this);
}

void thelper_del_word::run(QByteArray &buffer, QByteArray&, QJsonObject& param)
{
	
	if(!check_params(buffer, param, "rowid"))
        return;
	
	Glob::thelp->connect(Glob::settings->value("words_database","words.db3").toString());
	
	int rowid = param.value("rowid").toInt();
	
	Glob::thelp->deleteWord(rowid);
	
	Glob::thelp->closeDB();
    
    return ok(buffer);
}
