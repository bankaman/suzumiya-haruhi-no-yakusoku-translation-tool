#ifndef API_THELPER_TRANSLATE_H
#define API_THELPER_TRANSLATE_H

#include <api.h>

namespace api {
class thelper_translate : public API_command
{
public:
    thelper_translate();
    void run(QByteArray &buffer, QByteArray &query, QJsonObject &param);
} c_thelper_translate;

}

#endif // API_THELPER_TRANSLATE_H
