#include "shiftjiscoder.h"
#include "ui_shiftjiscoder.h"
#include "mainwindow.h"

ShiftJisCoder::ShiftJisCoder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShiftJisCoder)
{
    ui->setupUi(this);
    jp=QTextCodec::codecForName("Shift-JIS");
}

ShiftJisCoder::~ShiftJisCoder()
{
    delete ui;
}

void ShiftJisCoder::log(QString string){
    ui->plainTextEdit->moveCursor(QTextCursor::End);
    ui->plainTextEdit->insertPlainText(string);
}

QString ShiftJisCoder::toHex(QString string){
    QString t,t2;
    t=jp->fromUnicode(string).toHex();

    t2.clear();
    for(int i=0;i<t.length();i+=2){
        t2.append(t.at(i));
        t2.append(t.at(i+1));
        t2.append(" ");
    }
    return t2;
}

void ShiftJisCoder::on_pushButton_clicked()
{
    QString text=ui->lineEdit->text();
    int count=jp->fromUnicode(text).length();
    log(ui->lineEdit->text()+" - "+"\""+toHex(text).toUpper()+"\" "+QString::number(count)+" байт.\n");

}

void ShiftJisCoder::on_pushButton_2_clicked()
{
    QString text=MainWindow::toLatin(ui->lineEdit->text());
    int count=jp->fromUnicode(text).length();
    log(text+" - "+"\""+toHex(text).toUpper()+"\" "+QString::number(count)+" байт.\n");
}
