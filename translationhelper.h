#ifndef TRANSLATIONHELPER_H
#define TRANSLATIONHELPER_H

#include <QtSql/QSqlDatabase>
#include <QFileInfo>
#include <QSqlQuery>
#include <QSqlError>
#include <QVector>


class LinkElement{
public:
    int rowid;
    QString source, translated, pronunciation;

    LinkElement(int _rowid, QString _source, QString _translated, QString _pronunciation){
        rowid=_rowid;
        source=_source;
        translated=_translated;
        pronunciation=_pronunciation;
    }
};


class TranslationHelper
{
public:
    static const char* PRONOUNCIATION_FIELD;

    TranslationHelper();
    ~TranslationHelper();

    QVector<LinkElement*> links;

    void connect(QString _db_name);
    void closeDB();
    QString translate(QString source);
    QString translate2(QString source);
    QString getDBname();
    void addWord(QString source, QString translated);
    void deleteWord(int rowid);
    void updateData(int rowid, QString field_name, QString value);

private:
    QSqlDatabase db;
    QString db_name;

    QString findText(QString in);
    void clearLinks();
};

#endif // TRANSLATIONHELPER_H
