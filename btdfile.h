#ifndef BTDFILE_H
#define BTDFILE_H

#include <QByteArray>
#include <QString>
#include <QFile>
#include <QTextCodec>
#include <QDebug>

#include "references.h"


class BtdFile
{
    struct Variant{
        QByteArray before;
        QString text;
        QByteArray text_bytes;
        QByteArray dat_params;
        QString dat_file;
    };

    struct Theme{
        QByteArray before;
        QString text;
        QByteArray text_bytes;
        QVector<Variant*> variants;
        QByteArray after;
    };
    struct BTD{
        QVector<Theme*> themes;
        QByteArray after;
    };

public:
    BtdFile();
    ~BtdFile();

    bool load(QString filename);
    bool save(QString filename);
    QStringList get_themes();
    QStringList get_variants(int theme_id);
    QString get_variant(int theme_id,int variant_id);
    QString get_variant_filename(int theme_id,int variant_id);
    QString get_theme(int theme_id);
    int get_theme_max_size(int theme_id);
    void set_theme_text(int theme_id, QString text);
    void set_variant_text(int theme_id, int variant_id, QString text);
	
	QString getFileName();

    static const int THEME_MAX_SIZE=24;
    static const int VARIANT_MAX_SIZE=48;
    QTextCodec *text_codec;

private:
    QString read_as_string(QByteArray &data, int cursor);
    QByteArray read_until(unsigned char byte, QByteArray &data, int cursor);
    void clear_btd();
    void adjust_size(QByteArray &ba, int size);
	
	QString filename;

    BTD btd;
};

#endif // BTDFILE_H
