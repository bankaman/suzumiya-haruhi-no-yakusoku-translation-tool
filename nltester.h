#ifndef NLTESTER_H
#define NLTESTER_H

#include <QWidget>
#include <QPainter>
#include <QTextCodec>

namespace Ui {
class NLTester;
}

class NLTester : public QWidget
{
    Q_OBJECT

public:
    explicit NLTester(QWidget *parent = 0);
    ~NLTester();
    void setText(QString text_);

private:
    void drawCanvas();

    Ui::NLTester *ui;

    QImage bg;
    QImage canvas,scale;

    QString text;

    QTextCodec *utf_codec;

    QHash<ushort,QImage*> letters;

protected:
    void paintEvent(QPaintEvent *e);
    void showEvent(QShowEvent *e);
private slots:
    void on_pushButton_clicked();
};

#endif // NLTESTER_H
