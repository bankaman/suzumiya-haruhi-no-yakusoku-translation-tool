#include "translationhelper.h"
#include <QDebug>

const char* TranslationHelper::PRONOUNCIATION_FIELD = "pronunciation";

TranslationHelper::TranslationHelper()
{

}

TranslationHelper::~TranslationHelper()
{
    db.close();
    clearLinks();
}

//connect to database and creates needed tables if they dosent exists;
void TranslationHelper::connect(QString _db_name){
    db_name=_db_name;

    db=QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(db_name);
    db.open();

    QSqlQuery query;
    query.exec("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='words';");
    query.first();
    if(query.value(0).toInt()<1){
        query.exec("\
            create table words ( \
                source text, \
                translation text, \
                pronunciation text, \
                import_from text, \
                url text \
                notes text \
            );");
    }

}
//closing connection
void TranslationHelper::closeDB(){
    db.close();
}

// Search text in database
QString TranslationHelper::findText(QString in){
    QSqlQuery query;

    int count=0;
    query.exec("select rowid, source, translation, pronunciation from words where source like '"+in+"'");
    while(query.next()){
        count++;
        links.append(new LinkElement(query.value(0).toInt(), query.value(1).toString(), query.value(2).toString(), query.value(3).toString()));
    }

    if(count>0){
        query.first();
        //qDebug()<<"testing ok \""<<in<<"\"";
        return query.value(2).toString();
    }else{
        //qDebug()<<"testing fail \""<<in<<"\"";
        return "";
    }
}



//translate text
QString TranslationHelper::translate(QString source){
    QString result="",temp;

    clearLinks();

    int start=0,size=source.length();
    source.remove('\n');

    //qDebug()<<"source length = "<<size;
    bool need_n=false;
    while(start<source.length()){
        while(size>0){
            temp=findText(source.mid(start,size));
            if(temp.length()>0){
                if(need_n){
                    result.append("\n");
                    need_n=false;
                }
                result.append(source.mid(start,size)+" - "+temp+"\n");
                start+=source.mid(start,size).length();
                break;
            }else{
                size--;
            }
        }
        if(size<=0){
            size=1;
            result.append(source.mid(start,size));
            start++;            
            need_n=true;
        }
        size=source.length()-start;
    }

    return result;
}

//translate text2
QString TranslationHelper::translate2(QString source){
    QString result="",temp;

    clearLinks();

    int start=0,size=source.length();
    source.remove('\n');

    //qDebug()<<"source length = "<<size;
    bool need_n=false;
    while(start<source.length()){
        while(size>0){
            temp=findText(source.mid(start,size));
            if(temp.length()>0){
                if(need_n){
                    result.append(" ");
                    need_n=false;
                }
                result.append("<a href='"+source.mid(start,size)+"'>["+temp+"]</a> ");
                start+=source.mid(start,size).length();
                break;
            }else{
                size--;
            }
        }
        if(size<=0){
            size=1;
            result.append(source.mid(start,size));
            start++;
            need_n=true;
        }
        size=source.length()-start;
    }

    return result;
}


void TranslationHelper::clearLinks(){
    for(int i=0;i<links.length();i++){
        delete links.at(i);
    }
    links.clear();
}


QString TranslationHelper::getDBname(){
    return db_name;
}



//adding new word
void TranslationHelper::addWord(QString source, QString translated){
    QSqlQuery query;

    query.exec("insert into words (source, translation) values ('"+source+"', '"+translated+"')");

}


//deleting word from table
void TranslationHelper::deleteWord(int rowid){
    QSqlQuery query;
    if(!query.exec("delete from words where rowid="+QString::number(rowid)))
        qDebug()<<query.lastError().text();


}


//update data
void TranslationHelper::updateData(int rowid, QString field_name, QString value){
    QSqlQuery query;

    if(!query.exec("update words set "+field_name+"='"+value+"' where rowid="+QString::number(rowid)))
        qDebug()<<"query="<<query.lastQuery()<<"; "<<query.lastError().text();

    qDebug()<<"query="<<query.lastQuery();
}
