#include "datfile.h"
#include <QFile>
#include <references.h>

DatFile::DatFile()
{    
    utf=QTextCodec::codecForName("UTF-8");

    ifgotocallback=nullptr;
}
DatFile::~DatFile()
{
    clear();
}

void DatFile::set_link_offset(int from_command_id, int offset)
{
    for(int i=0; i<links.length();i++){
        Link *link = links.at(i);
        if(link->from_command_id==from_command_id){
            link->shift = offset;
            break;
        }
    }
}

void DatFile::log(QString str)
{
    log_string.append(str);
}

QVector<Link *> DatFile::get_links_from_command(int command_id)
{
    QVector<Link*> out;
    for(int i=0;i<links.length();i++){
        if(links.at(i)->from_command_id==command_id){
            out.append(links.at(i));
        }
    }
    return out;
}

void DatFile::ifgoto(int where)
{
    if(!follow_goto_commands)
        return;
    if(ifgotocallback!=nullptr){
        if(ifgotocallback(where)){
            cursor=where;
        }
    }
}


void DatFile::clear(){
    for(int i=0;i<commands.length();i++) free(commands[i]);
    commands.clear();
    for(int i=0;i<links.length();i++) free(links[i]);
    links.clear();
    not_resolved_links.clear();
    for(int i=0;i<text_patches.length();i++) free(text_patches[i]);
    text_patches.clear();
    text_commands_adresses.clear();
}

int DatFile::read_as_2byte_int(QByteArray &data,int cursor){
    int out=0;
    out=(data.at(cursor)&0xff);cursor++;
    out+=((data.at(cursor)&0xff)<<8);cursor++;
    if(((data.at(cursor-1)&0xff)>>7)&1==1){
        out=out|0xffff0000;
    }

    return out;
}

QString DatFile::read_as_string(QByteArray &data, int cursor){
    int temp;
    temp=read_as_2byte_int(data,cursor);
    return read_codec->toUnicode(data.mid(cursor+2,temp));
}

int DatFile::get_command_id_by_address(int address){
    for(int i=0;i<commands.length();i++){
        if(commands.at(i)->original_address==address){
            return i;
        }
    }
    return -1;
}

void DatFile::add_link(int from_command_id, int to_command_address, int shift, int length){
    Link *link;
    link=new Link();
    link->from_command_id=from_command_id;
    link->shift=shift;
    link->length=length;
    link->to_command_address=to_command_address;
    link->to_command_id=get_command_id_by_address(to_command_address);
    links.append(link);
    if(link->to_command_id==-1){
        not_resolved_links.append(links.length()-1);
    }
}

void DatFile::append_command(int address, QByteArray command){
    Command *com;
    com=new Command();
    com->original_address=address;
    com->command=command;
    commands.append(com);

    //Ищем комманды с сылками
    int temp,temp2;
    switch(command.at(0)){
        //вариант выбора
        case 0x11:
            temp = read_as_2byte_int(command,4);
            temp2 = read_as_2byte_int(command,6+temp)+address+command.length();
            add_link(commands.length()-1,temp2,6+temp,2);
        break;
        //Условный переход
        case 0x12:
            temp=read_as_2byte_int(command,6);
            temp2=address+command.length()+temp;
            add_link(commands.length()-1,temp2,6,2);
        break;
        //Безусловный переход
        case 0x13:
            temp=read_as_2byte_int(command,1);
            temp2=address+command.length()+temp;
            add_link(commands.length()-1,temp2,1,2);
        break;
        //Вызов подпрограммы
        case 0x14:
            temp=read_as_2byte_int(command,1);
            temp2=address+command.length()+temp;
            add_link(commands.length()-1,temp2,1,2);
        break;
    }

    //Добавляем в список текстовых комманд
    if(is_text_command(commands.length()-1)){
        text_commands_adresses.append(commands.length()-1);
    }

    //Пробуем найти связь для не решённых ссылок
    for(int i=0;i<not_resolved_links.length();i++){
        temp=links[not_resolved_links[i]]->to_command_address;
        links[not_resolved_links[i]]->to_command_id=get_command_id_by_address(temp);
        if(links[not_resolved_links[i]]->to_command_id!=-1){
            not_resolved_links.remove(i);
        }
    }
}

//Проверяем на наличие текста в команде
bool DatFile::is_text_command(int id){
    if(id<0)
        return false;
    if(id>=commands.length())
        return false;
    switch(commands.at(id)->command.at(0)){
        case 0x03:
        case 0x11:
        case 0x69:
        case 0x75:
            return true;
    default: return false;
    }
}


//Получаем ID следующей текстовой команды
int DatFile::next_text_command_id(int from_id){
    for(int i=from_id+1;i<=commands.length();i++){
        if(is_text_command(i)) return i;
    }
    return from_id;
}

//Получаем ID предыдущей текстовой команды
int DatFile::prev_text_command_id(int from_id){
    for(int i=from_id-1;i>=0;i--){
        if(is_text_command(i)) return i;
    }
    return from_id;
}


QString DatFile::get_original_text(int command_id){
    int temp;
    QByteArray com;
    if(command_id>=commands.length()){
        return "";
    }
    com=commands.at(command_id)->command;
    switch(com.at(0)){
        case 0x03:
        case 0x69:
            temp=read_as_2byte_int(com,1);
            return read_as_string(com,4+temp);
        case 0x11:
            return read_as_string(com,4);
        case 0x75: return read_as_string(com,1);
    default: return "";
    }
}

int DatFile::get_text_patch_id(int command_id){
    for(int i=0;i<text_patches.length();i++){
        if(text_patches[i]->command_id==command_id){
            return i;
        }
    }
    return -1;
}
//Починка сломанного текстового патча
void DatFile::normalize_textpatch()
{
    bool ok;
    int ind;
    TextPatch *tp;
    do{
        ok=true;
        for(int i=0;i<text_patches.length();i++){
            tp=text_patches.at(i);
            if(tp->command_id>=commands.length()){
                ok=false;
                ind=i;
                break;
            }
        }
        if(!ok){
            delete text_patches.at(ind);
            text_patches.removeAt(ind);
        }
    }while (!ok);
}

QString DatFile::get_patched_text(int command_id){
    int index=get_text_patch_id(command_id);

    if(index==-1){
        index=set_patched_text(command_id,get_original_text(command_id));
    }

    return text_patches.at(index)->text;
}

bool check_is_audio(int command_id){
    switch(command_id){
    case 0x69:
    case 0x03:
        return true;
    break;
    default: return false;
    }
}

QString DatFile::get_audio_name(int command_id){
    if(command_id>=commands.length()){
        return "";
    }
    if(check_is_audio(commands.at(command_id)->command.at(0))){
        return read_as_string(commands.at(command_id)->command,1);
    }
    return "";
}


int DatFile::set_patched_text(int command_id, QString text){
    int index=get_text_patch_id(command_id);

    if(index!=-1){
        text_patches.at(index)->text=text;
    }else{
        TextPatch *patch;
        patch=new TextPatch();
        patch->command_id=command_id;
        patch->text=text;
        text_patches.append(patch);
        index=text_patches.length()-1;
    }
    return index;
}
// Создание новой команды вывода текста
void DatFile::create_new_text_command(int after_command_id, QString text)
{
    //03 - воспроизвести часть диалога 03 [длина звук. файла 2 байта] [имя звук файла] [01] [длина строки 2 байта] [текст]
    //Команда вывода текста без звука : Комманда 0x03| Часть диалога, звук.файл: "non0" 0x1 текст: ""

    char com_id;

    char id = commands.at(after_command_id)->command.at(0);
    if(id!=0x75){
        com_id = id;
    }

    //Создание команды
    QByteArray t;
    Command *com = new Command();
    com->command.clear();
    com->command.append(com_id);
    append_2byte_int(com->command,4);
    com->command.append("non0");
    com->command.append(0x01);
    t = read_codec->fromUnicode(text);
    append_2byte_int(com->command,t.length());
    com->command.append(t);
    com->original_address = -10;

    //Втавка команды
    commands.insert(after_command_id+1, com);

    //исправление ссылок
    Link *l;
    for(int i = 0; i<links.length(); i++){
        l=links.at(i);
        if(l->from_command_id>after_command_id)
            l->from_command_id++;
        if(l->to_command_id > after_command_id)
            l->to_command_id++;
    }

    //исправление списка патчей
    TextPatch *tp;
    for(int i = 0; i<text_patches.length(); i++){
        tp = text_patches.at(i);
        if(tp->command_id>after_command_id)
            tp->command_id++;
    }

    //обновление списка текстовых комманд
    text_commands_adresses.clear();
    for(int i=0;i<commands.length();i++){
        if(is_text_command(i))
            text_commands_adresses.append(i);
    }
}


void DatFile::append_2byte_int(QByteArray &data, int value){
    int temp;
    temp=value&0xff;
    data.append(temp);
    temp=(value>>8)&0xff;
    data.append(temp);
}



//Применяем изменения и сохраняем
bool DatFile::patch_and_save(QString filename){

    QByteArray out,t_ar;
    int temp,temp2;

    for(int i=0;i<commands.length();i++){
        commands.at(i)->new_command.clear();
        commands.at(i)->new_command.append(commands.at(i)->command);
    }

    normalize_textpatch();

    //Для начала применяем патчи к текстовым коммандам
    for(int i=0;i<text_patches.length();i++){
        QByteArray &com=commands[text_patches[i]->command_id]->command;
        switch(com.at(0)){            
            case 0x03:
            case 0x69:
                temp=read_as_2byte_int(com,1);
                temp2=3+temp+1;
                out.clear();
                out.append(com.mid(0,temp2));
                t_ar=write_codec->fromUnicode(text_patches[i]->text);
                append_2byte_int(out,t_ar.length());
                out.append(t_ar);
                //out.append(com->mid(temp2));
                //showMessageBox("tar="+t_ar.toHex()+"\n com="+com.toHex()+"\n out="+out.toHex());
            break;

            case 0x11:
                out.clear();
                out.append(com.mid(0,4));
                t_ar=write_codec->fromUnicode(text_patches[i]->text);
                append_2byte_int(out,t_ar.length());
                out.append(t_ar);
                set_link_offset(text_patches[i]->command_id,out.length());//фиксим положение ссылки на комманду
                out.append((char)0);
                out.append((char)0);
            break;

            case 0x75:
                out.clear();
                out.append(com.at(0));
                t_ar=write_codec->fromUnicode(text_patches[i]->text);
                append_2byte_int(out,t_ar.length());
                out.append(t_ar);
            break;

            default:
                out.clear();
                out.append(com.mid(0));
            break;
        }
        commands[text_patches[i]->command_id]->new_command.clear();
        commands[text_patches[i]->command_id]->new_command.append(out.mid(0));
    }


    //Теперь из за того что размер некоторых комманд изменился поправляем ссылки
    int from, to, length;
    for(int i=0;i<links.length();i++){
        from=links[i]->from_command_id;
        to=links[i]->to_command_id;

        //считаем длину между коммандами
        int j;
        if(from>to){
            j=from;
            to--;
        }else{
            j=from+1;
        }
        length=0;
        while(j!=to){

            length+=commands[j]->new_command.length();

            if(j<to) j++;
            else j--;
        }


        if(from>to) {
            length=-length;
        }

        if(links[i]->length==2){
            commands[from]->new_command[links[i]->shift]=length&0xff;
            commands[from]->new_command[links[i]->shift+1]=(length>>8)&0xff;
        }
    }

    //Объеденяем всё в один массив
    QByteArray out_data;
    length=0;
    out_data.clear();

    out_data.append(0x4F);
    out_data.append(0x42);
    out_data.append(0x4A);
    out_data.append(length);

    out_data.append(length);
    out_data.append(length);
    out_data.append(length);
    out_data.append(length);

    for(int i=0;i<commands.length();i++){
        out_data.append(commands[i]->new_command);
    }

    length=out_data.length();
    log("length="+QString::number(length));
    out_data[4]=length&0xFF;
    out_data[5]=(length>>8)&0xFF;
    out_data[6]=(length>>16)&0xFF;
    out_data[7]=(length>>24)&0xFF;


    //И записываем полученное в файл
    QFile file;
    file.setFileName(filename);
    if(!file.open(QFile::WriteOnly|QFile::Truncate)){
        log("Error opening file for writing.");
        return false;
    }
    file.write(out_data);
    file.close();
    return true;
}


//Сохраняем в файл массив text_patches
bool DatFile::save_patch_to_file(QString filename){
    QByteArray out;

    out.append("HTP");
    out.append((char)0);
    append_2byte_int(out,text_patches.length());

    for(int i=0;i<text_patches.length();i++){
        //temp.clear();
        append_2byte_int(out,text_patches[i]->command_id);
        out.append(utf->fromUnicode(text_patches[i]->text));
        out.append((char)0);
    }


    QFile file;
    file.setFileName(filename);
    if(!file.open(QFile::WriteOnly|QFile::Truncate)){
        log("Error opening file for writing.");
        return false;
    }
    file.write(out);
    file.close();
    return true;
}

QString DatFile::read_string_until_zero(QByteArray &data, int &cursor){
    QByteArray bar;
    QString out;
    bar.clear();
    while(data.at(cursor)!=0x00){
        bar.append(data.at(cursor));
        cursor++;
    }
    out.clear();
    out.append(utf->toUnicode(bar));
    cursor++;
    return out;
}

//Загружаем массив text_patches из файла
bool DatFile::load_patch_from_file(QString filename){
    QFile file;
    file.setFileName(filename);
    if(!file.open(QIODevice::ReadOnly)){
        log("Can't open this file.");
        return false;
    }
    QByteArray data;
    data.clear();
    data = file.readAll();
    file.close();

    QString temp;
    int cursor=0;
    temp=read_string_until_zero(data,cursor);
    if(!temp.startsWith("HTP")){
        log("This is not 'Haruhi Text Patch' file!");
        return false;
    }

    int count=read_as_2byte_int(data,cursor);cursor+=2;
    int rec_id=-1;
    for(int i=0;i<count;i++){
        rec_id = read_as_2byte_int(data,cursor);cursor+=2;
        set_patched_text(rec_id,read_string_until_zero(data,cursor));
    }
    return true;
}
// Очистка данных патча
void DatFile::clear_patch_info()
{
    text_patches.clear();
}

int DatFile::get_text_command_index_by_command_index(int command_id)
{
    for(int i=0;i<text_commands_adresses.length();i++){
        if(text_commands_adresses.at(i)==command_id)
            return i;
    }
    return -1;
}

int DatFile::get_text_commands_count()
{
    return text_commands_adresses.length();
}

bool DatFile::load_file(QString filename)
{
    QFile data_file;
    data_file.setFileName(filename);
    if(!data_file.open(QIODevice::ReadOnly)){
        log("Can't open this file.");
        return false;
    }

    last_filename = filename;
    data.clear();
    data.append(data_file.readAll());
    cursor = 0;

    QString signature = read_string(3);
    if(signature != "OBJ"){
        log("Wrong file signature "+signature);
        return false;
    }
    cursor=4;
    int length=read_4_byte_int();
    log("Длина файла "+QString::number(length)+" байт\n");
    log(signature+"\n");


    max_string_length=0;

    return true;
}

bool DatFile::parse_all()
{
    try{
        while(cursor<data.length()){
            if(!parse_command())
                return false;
        }
        log("Максимальная длина сообщения: "+QString::number(max_string_length)+"\n");

    }catch(int e){
        switch(e){
            case ERROR_OUT_OF_RANGE: log("\n\n(!)Error: out of range! cursor=0x"+QString::number(cursor,16)+" ("+QString::number(cursor)+")\n"); break;
            case ERROR_UNKNOWN_COMMAND:
                int count=20;
                if(count+cursor>data.length()) count=data.length()-cursor;
                log("Неизвестная комбинация\n");
                print_as_hex(count);
                log("\n(\"");
                print_as_text(count);
                log("\")\n");
            return  false;
            break;
        }
    }
    return true;
}

bool DatFile::parse_command()
{
    bool progress=false;
    log(QString("%1").arg(cursor, 4, 16, QChar('0')).toUpper()+"|");
    //update_screen();
    int temp,temp2,scur;
    QString string;
    scur=cursor;


    switch(data.at(cursor)){
        //Ноль в конце файла
        case 0x00:
            if(cursor==data.length()-1){
                cursor++;
            }else throw ERROR_UNKNOWN_COMMAND;
        break;

        //0x01 вывод в диалог-бокс
        case 0x01:
            cursor++;
            temp=read_2_byte_int();
            log("Команда 0x01| Вывод текста: \"\n");
            print_as_text(temp);
            cursor+=temp;
            log("\"\n");
        break;

        //0x03 - часть диалога
        case 0x03:
            cursor++;
            temp=read_2_byte_int();
            log("Команда 0x03| Часть диалога, звук.файл: \"");
            print_as_text(temp);
            cursor+=temp;
            log("\" 0x"+QString::number(data.at(cursor)&0xff,16)+" текст: \"\n");
            cursor++;
            temp=read_2_byte_int();
            if(temp>max_string_length) max_string_length=temp;
            print_as_text(temp);
            cursor+=temp;
            log("\"\n");
        break;

        //0x11 - вариант выбора действия
        case 0x11:
            cursor++;
            log("Команда 0x11| ");
            print_as_hex(1);
            cursor++;
            temp = read_2_byte_int();
            log(" выбор № "+QString::number(temp));
            temp = read_2_byte_int();
            log(" текст: \"");
            print_as_text(temp);
            cursor+=temp;
            log("\" ");
            temp = read_2_byte_int();
            log(" cursor+=0x"+QString::number(temp,16).toUpper()+"("+QString::number(temp)+")("+QString::number(temp+cursor,16).toUpper()+")");
            log("\n");
        break;

        //0x12 - Условный переход
        case 0x12:
            cursor++;
            log("Команда 0x12| Условный переход Параметры: ");
            print_as_hex(5);
            cursor+=5;
            temp = read_2_byte_int();
            log(" cursor+=0x"+QString::number(temp,16).toUpper()+"("+QString::number(temp)+")("+QString::number(temp+cursor,16).toUpper()+")");
            log("\n");
            ifgoto(temp+cursor);
        break;

        //0x13 - предположительно переход на адрес
        case 0x13:

            cursor++;
            temp=read_2_byte_int();
            //log("Команда 0x13| cursor+=0x"+QString::number(temp,16).toUpper()+" ("+QString::number(temp)+")("+QString::number(cursor+temp,16).toUpper()+")\n");
            log("Команда 0x13| goto ("+QString::number(cursor+temp,16).toUpper()+")\n");
            if(follow_goto_commands) cursor+=temp;
        break;

        //0x14 - вызов подпрограммы
        case 0x14:

            cursor++;
            temp=read_2_signed_byte_int();
            //log("Команда 0x14|0x"+QString::number(cursor,16).toUpper()+" cursor+=0x"+QString::number(temp,16).toUpper()+" ("+QString::number(temp)+")("+QString::number(temp+cursor,16).toUpper()+")\n");
            log("Команда 0x14| GotoSub ("+QString::number(temp+cursor,16).toUpper()+")\n");
            if(follow_goto_commands){
                stack.push(cursor);
                cursor+=temp;
            }
        break;

        //0x15 - Возврат из подпрограммы
        case 0x15:
            cursor++;
            temp=cursor;
            if(follow_goto_commands) temp=stack.pop();
            log("Команда 0x15| return\n");
            if(follow_goto_commands) cursor=temp;
        break;

        //0x16 - Неизвестно, возврат куда-то
        case 0x16:
            cursor++;
            log("Команда 0x16| Неизвестно, возврат куда-то \n");
        break;

        //0x17 - Возврат к выбору темы разговога
        case 0x17:
            cursor++;
            log("Команда 0x17| Возврат к выбору темы разговора\n");
        break;

        //0x18 - Запуск другого скрипта
        case 0x18:
            cursor++;
            log("Команда 0x18| Запуск скрипта \"");
            string = read_string();
            References::add(References::TYPE_SCRIPT, string);
            log(string+"\" \n");
        break;

        //0x1A - неизвестная Команда
        case 0x1A:

            cursor++;
            log("Команда 0x1A| Начало загрузки\n");
        break;

        //0x1B - неизвестная Команда
        case 0x1B:

            cursor++;
            log("Команда 0x1B| Конец загрузки\n");
        break;

        //0x1C - формирование диалога выбора варианта
        case 0x1C:
            cursor++;
            log("Команда 0x1C| Формирование диалога выбора варианта\n");
        break;

        //0x1D - Вывод диалога выбора варианта
        case 0x1D:
            cursor++;
            if(data.at(cursor)==0x01){
                progress=true;
                cursor++;
                log("Команда 0x1D| 01 Вывод диалога выбора варианта\n");
            }
            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x20 - Запуск интерактивного диалога
        case 0x20:
            cursor++;
            log("Команда 0x20| Запуск интерактивного диалога, параметры: ");
            print_as_hex(2);
            References::add(References::TYPE_BTD,last_filename);
            cursor+=2;
            log("\n");
        break;

        case 0x21:
            cursor++;
            log("Команда 0x21| Запуск карты \"");
            string = read_string();
            References::add(References::TYPE_MAP,string);
            log(string+"\" ");
            print_as_hex(1);
            log("\n");
            cursor++;
        break;

        //0x30 - Изменение выражения лица
        case 0x30:
            cursor++;
            log("Команда 0x30| Изменение выражения лица, параметры: ");
            print_as_hex(4);
            log("\n");
            cursor+=4;
        break;

        //0x31 - Неизвестно
        case 0x31:
            cursor++;
            log("Команда 0x31| Неизвестно\n");
        break;

        //0x32 - Показать персонажа
        case 0x32:
            cursor++;
            if(data.at(cursor)==0x00){
                progress=true;
                cursor++;
                temp = data.at(cursor); cursor++;
                log("Команда 0x32| Воспроизвести анимацию '"+decrypt_name_plate(temp)+"', параметры: ");
                print_as_hex(7);
                log("\n");
                cursor+=7;
            }

            if(data.at(cursor)==0x01){
                progress=true;
                cursor++;
                temp = data.at(cursor); cursor++;
                log("Команда 0x32| Показать персонажа '"+decrypt_name_plate(temp)+"', параметры: ");
                print_as_hex(9);
                log("\n");
                cursor+=9;
            }
            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x33 - Скрыть персонажа
        case 0x33:
            cursor++;
            if(data.at(cursor)==0x00){
                progress=true;
                cursor++;
                temp = data.at(cursor); cursor++;
                log("Команда 0x33| Скрыть персонажа '"+decrypt_name_plate(temp)+"', параметры: ");
                print_as_hex(1);
                log("\n");
                cursor+=1;
            }
            if(data.at(cursor)==0x01){
                progress=true;
                cursor++;
                temp = data.at(cursor); cursor++;
                log("Команда 0x33| Плавно скрыть персонажа '"+decrypt_name_plate(temp)+"', параметры: ");
                print_as_hex(5);
                log("\n");
                cursor+=5;
            }
            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x34 - Передвижение персонажа
        case 0x34:
            cursor++;
            if(data.at(cursor)==0x01){
                progress=true;
                cursor++;
                log("Команда 0x34| 0x01 Передвинуть персонажа ");
                print_as_hex(1);
                cursor++;
                temp = data.at(cursor); cursor++;
                log(" \""+decrypt_name_plate(temp)+"\", параметры: ");
                print_as_hex(5);
                log("\n");
                cursor+=5;
            }
            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x35 - Неизвестная Команда
        case 0x35:
            cursor++;
            if(data.at(cursor)==0x00){
                progress=true;
                cursor++;
                temp = data.at(cursor); cursor++;
                log("Команда 0x35| Загрузка персонажа,\""+decrypt_name_plate(temp)+"\" параметры: ");
                print_as_hex(2);
                log("\n");
                cursor+=2;
            }
            if(data.at(cursor)==0x01){
                progress=true;
                cursor++;
                temp = read_2_byte_int();
                log("Команда 0x35| Выгрузка персонажа,\""+decrypt_name_plate(temp));
                log("\n");
            }
            if(data.at(cursor)==0x02){
                progress=true;
                log("Команда 0x35| Выгрузить персонажей (0x");
                print_as_hex(1);
                log(")\n");
                cursor++;
            }

            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x36 - Показать изображение на экране
        case 0x36:

            cursor++;
            temp2=data.at(cursor)&0xff;
            cursor++;
            log("Команда 0x36| Показать изображение 0x"+QString::number(temp2,16).toUpper()+" \"");

            temp=read_2_byte_int();
            print_as_text(temp);
            cursor+=temp;
            log("\" ");
            switch(temp2){
                case 0x0: temp=9; break;
                case 0x2: temp=5; break;
            default:  temp=5; break;
            }
            print_as_hex(temp);cursor+=temp;
            log("\n");
        break;

        //0x37 - Убрать изображение с экрана
        case 0x37:

            cursor++;
            log("Команда 0x37| Скрыть изображение 0x");
            print_as_hex(1);cursor++;
            temp=read_2_byte_int();
            log(" \"");
            print_as_text(temp);
            log("\"\n");
            cursor+=temp;
        break;

        //0x38 - загрузка изображения bg
        case 0x38:
            cursor++;
            if(data.at(cursor)==0x00){
                progress=true;
                log("Команда 0x38| Загрузка изображения, (bg) 0x"+QString::number(data.at(cursor)&0xFF).toUpper()+" \"");
                cursor++;
                string = read_string();
                References::add(References::TYPE_BG_IMAGE,string);
                log(string+"\" ");
                print_as_hex(2);
                log("\n");
                cursor+=2;
            }

            if(data.at(cursor)==0x01){
                progress=true;
                log("Команда 0x38| Выгрузка изображения, (bg) 0x"+QString::number(data.at(cursor)&0xFF).toUpper()+" \"");
                cursor++;
                string = read_string();
                log(string+"\"\n");
            }

            if(data.at(cursor)==0x02){
                progress=true;
                log("Команда 0x38| Очистка изображений (bg) 0x"+QString::number(data.at(cursor)&0xFF).toUpper()+"\n");
                cursor++;
            }

            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //Появление объекта
        case 0x39:
            cursor++;

            //eyecatch
            if(data.at(cursor)==0x00){
                progress=true;
                log("Команда 0x39| 0x"+QString::number(data.at(cursor)).toUpper()+" Плавное появление объекта (eyecatch) \"");
                cursor++;
                temp=read_2_byte_int();
                print_as_text(temp);
                cursor+=temp;
                log("\" ");
                print_as_hex(5);
                cursor+=5;
                log("\n");
            }

            //плавное появление
            if(data.at(cursor)==0x01){
                progress=true;
                log("Команда 0x39| 0x"+QString::number(data.at(cursor)).toUpper()+" Плавное появление объекта \"");
                cursor++;
                temp=read_2_byte_int();
                print_as_text(temp);
                cursor+=temp;
                log("\" ");
                print_as_hex(9);
                cursor+=9;
                log("\n");
            }

            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }

        break;

        //Исчезновение объекта
        case 0x3A:
            cursor++;
            //Плавное исчезновение объекта eyecatch
            if(data.at(cursor)==0x00){
                progress=true;
                log("Команда 0x3A| 0x"+QString::number(data.at(cursor)).toUpper()+" Плавное исчезновение объекта \"");
                cursor++;
                temp=read_2_byte_int();
                print_as_text(temp);
                cursor+=temp;
                log("\" \n");
            }

            //Плавное исчезновение объекта
            if(data.at(cursor)==0x01){
                progress=true;
                log("Команда 0x3A| 0x"+QString::number(data.at(cursor)).toUpper()+" Плавное исчезновение объекта \"");
                cursor++;
                temp=read_2_byte_int();
                print_as_text(temp);
                cursor+=temp;
                log("\" ");
                print_as_hex(4);
                cursor+=4;
                log("\n");
            }

            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x3C - загрузка изображения obj
        case 0x3C:
            progress = false;
            cursor++;

            //Загрузка изображения
            if(data.at(cursor)==0x00){
                log("Команда 0x3C| Загрузка изображения, (obj) 0x"+QString::number(data.at(cursor)&0xFF).toUpper()+" \"");
                cursor++;
                string = read_string();
                References::add(References::TYPE_OBJ_IMAGE, string);
                log(string+"\" ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
                progress = true;
            }

            //Выгрузка изображения
            if(data.at(cursor)==0x01){
                log("Команда 0x3C| Выгрузка изображения, (obj) 0x"+QString::number(data.at(cursor)&0xFF).toUpper()+" \"");
                cursor++;
                temp=read_2_byte_int();
                print_as_text(temp);
                cursor+=temp;
                log("\"\n");
                progress = true;
            }

            //Выгрузка всех изображений
            if(data.at(cursor)==0x02){
                log("Команда 0x3C| Выгрузка всех изображений, 0x"+QString::number(data.at(cursor)&0xFF).toUpper()+" \n");
                cursor++;
                progress = true;
            }

            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x3D - Вывести табличку с именем
        case 0x3D:
            cursor++;

            if(data.at(cursor)==0x00){
                progress=true;
                log("Команда 0x3D| Поменять текст на табличке с именем, 0x"+QString::number(data.at(cursor),16).toUpper());
                cursor++;
                string=decrypt_name_plate(read_2_byte_int());
                log(" \""+string+"\" ");
                print_as_hex(2);cursor+=2;
                log("\n");
            }

            if(data.at(cursor)==0x01){
                progress=true;
                log("Команда 0x3D| Вывести табличку с именем, 0x"+QString::number(data.at(cursor),16).toUpper());
                cursor++;
                string=decrypt_name_plate(read_2_byte_int());
                log(" \""+string+"\" ");
                print_as_hex(2);cursor+=2;
                log("\n");
            }
            if(data.at(cursor)==0x03){
                progress=true;
                log("Команда 0x3D| Убрать табличку с именем, параметры: ");
                print_as_hex(3);
                cursor+=3;
                log("\n");
            }
            if(!progress) {
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x3E - Вывести табличку для вывода диалога
        case 0x3E:

            cursor++;
            switch(data.at(cursor)){
                case 0x00: log("Команда 0x3E| Вывести табличку для вывода диалога, параметры: "); break;
                case 0x01: log("Команда 0x3E| Убрать табличку для вывода диалога, параметры: "); break;
                default: log("Команда 0x3E| Неизвестно, параметры: "); break;
            }
            print_as_hex(3);
            cursor+=3;
            log("\n");
        break;

        //0x40 - Установка местоположения
        case 0x40:
            if(data.at(cursor+1)==0x00){

                cursor+=2;
                log("Команда 0x40| 0x00 Показать инфу о местоположении, ");
                log("\""+decrypt_place_plate(read_2_byte_int())+"\" ");
                print_as_hex(2);cursor+=2;
                log("\n");
            }
            if(data.at(cursor+1)==0x02){

                cursor+=2;
                log("Команда 0x40| 0x02 Установка местоположения: ");
                log("\""+decrypt_place_plate(read_2_byte_int())+"\" ");
                log("\n");
            }
        break;

        //0x52 - Передвижение заднего фона
        case 0x52:{
            cursor+=1;
            QString name = read_string();
            int x = read_2_signed_byte_int();
            int y = read_2_signed_byte_int();
            int delay = read_2_byte_int();
            log("Команда 0x52| Движение заднего фона \""+name+"\" X:"+QString::number(x)+", Y:"+QString::number(y)+", задержка: "+QString::number(delay)+"; неизв:");
            print_as_hex(2);cursor+=2;
            log("\n");
        }break;

        //0x56 - Выход из темноты
        case 0x56:
            cursor++;

            if(data.at(cursor)==0x00){
                progress=true;
                log("Команда 0x56| Выход из темноты, параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }

            if(data.at(cursor)==0x01){
                progress=true;
                log("Команда 0x56| Выход из темноты (Вспышка?), параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }

            if(data.at(cursor)==0x04){
                progress=true;
                log("Команда 0x56| Выход из темноты с переходом \"шторка\" сверху вниз, параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }

            if(data.at(cursor)==0x06){
                progress=true;
                log("Команда 0x56| Выход из темноты с переходом \"шторка\" слева на право, параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }

            if(data.at(cursor)==0x08){
                progress=true;
                log("Команда 0x56| Выход из темноты, кружок, 0x");
                print_as_hex(1);
                log("\n");
                cursor++;
            }

            if(data.at(cursor)==0x09){
                progress=true;
                log("Команда 0x56| Выход из розового, маска H, 0x");
                print_as_hex(1);
                log("\n");
                cursor++;
            }

            if(data.at(cursor)==0x0A){
                progress=true;
                log("Команда 0x56| Выход из белого цвета, кружок, 0x");
                print_as_hex(1);
                log("\n");
                cursor++;
            }

            if(data.at(cursor)==0x0B){
                progress=true;
                log("Команда 0x56| Выход из синего цвета, маска H, 0x");
                print_as_hex(1);
                log("\n");
                cursor++;
            }

            if(data.at(cursor)==0x0C){
                progress=true;
                log("Команда 0x56| Переход из темноты, маска H, 0x");
                print_as_hex(1);
                log("\n");
                cursor++;
            }

            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x57 - Уход в темноту
        case 0x57:
            cursor++;
            if(data.at(cursor)==0x00){
                progress=true;
                log("Команда 0x57| Уход в темноту, параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }
            if(data.at(cursor)==0x01){
                progress=true;
                log("Команда 0x57| Уход в белый свет, параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }
            if(data.at(cursor)==0x05){
                progress=true;
                log("Команда 0x57| Уход в темноту с переходом \"шторка\" сверху вниз, параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }
            if(data.at(cursor)==0x07){
                progress=true;
                log("Команда 0x57| Уход в темноту с переходом \"шторка\" слева на право, параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }
            if(data.at(cursor)==0x0C){
                progress=true;
                log("Команда 0x57| Уход в темноту c маской, параметры: ");
                print_as_hex(7);
                log("\n");
                cursor+=7;
            }
            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x59 - Запуск тряски камеры
        case 0x59:
            cursor++;
            if(data.at(cursor)==0x02){
                progress=true;
                cursor++;
                log("Команда 0x59| 0x02 Запуск тряски камеры, параметры: ");
                print_as_hex(4);
                log("\n");
                cursor+=4;
            }
            if(data.at(cursor)==0x03){
                progress=true;
                cursor++;
                log("Команда 0x59| 0x03 Прекращение тряски камеры\n");
            }

            if(!progress){
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }

        break;

        //0x5B - Запустить видео
        case 0x5B:

            cursor++;
            log("Команда 0x5B| Запустить видео \"");
            string = read_string();
            References::add(References::TYPE_VIDEO, string);
            log(string+"\" ");
            print_as_hex(2);
            cursor+=2;
            log("\n");
        break;

        //0x5C - Эффект моргания
        case 0x5C:
            cursor++;
            log("Команда 0x5C| Эффект моргания ");
            print_as_hex(3);
            cursor+=3;
            log("\n");
        break;

        //0x5D - Звуковой эффект
        case 0x5D:
            cursor++;
            //Запуск звукового эффекта
            if(data.at(cursor)==0x00){
                progress=true;
                cursor++;
                log("Команда 0x5D| 00 Звуковой эффект, параметры: ");
                print_as_hex(6);
                cursor+=6;
                log("\n");
            }

            //Неизвестно
            if(data.at(cursor)==0x01){
                progress=true;
                cursor++;
                log("Команда 0x5D| 01 Звуковой эффект(?), параметры: ");
                print_as_hex(6);
                cursor+=6;
                log("\n");
            }

            //Остановка звуковых эффектов (?)
            if(data.at(cursor)==0x03){
                progress=true;
                cursor++;
                log("Команда 0x5D| 03 Остановка звук эффектов");
                log("\n");
            }

            if(!progress) {
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x5E - Запустить/остановить музыку
        case 0x5E:
            cursor++;
            if(data.at(cursor)==0x00){
                progress=true;
                log("Команда 0x5E| Запустить музыку 0x");
                print_as_hex(1);cursor++;
                log(" \"");
                temp=read_2_byte_int();
                print_as_text(temp);
                log("\" ");
                cursor+=temp;
                print_as_hex(4);
                cursor+=4;
                log("\n");
            }
            if(data.at(cursor)==0x01){
                progress=true;
                log("Команда 0x5E| Остановить музыку 0x");
                print_as_hex(1);cursor++;
                log(", параметры: ");
                print_as_hex(4);
                cursor+=4;
                log("\n");
            }
            if(!progress) {
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x60 - Неизвестная Команда
        case 0x60:

            cursor++;
            log("Команда 0x60| Назначение неизвестно параметры: ");
            print_as_hex(5);
            log("\n");
            cursor+=5;
        break;

        //0x61 - Неизвестная Команда
        case 0x61:

            cursor++;
            log("Команда 0x61| ");
            print_as_hex(1);cursor++;
            temp = read_2_byte_int();
            log("Пауза "+QString::number(temp));
            log("\n");
        break;

        //0x63 - Неизвестная Команда
        case 0x63:

            cursor++;
            log("Команда 0x63| Назначение неизвестно параметры: ");
            print_as_hex(2);
            log("\n");
            cursor+=2;
        break;

        //0x64 - Неизвестная Команда
        case 0x64:

            cursor++;
            log("Команда 0x64| Назначение неизвестно параметры: ");
            print_as_hex(6);
            log("\n");
            cursor+=6;
        break;

        //0x65 - Неизвестная Команда
        case 0x65:

            cursor++;
            log("Команда 0x65| Назначение неизвестно\n");
        break;

        //0x66 - Неизвестная Команда
        case 0x66:
            cursor++;
            if(data.at(cursor)==0x00){
                progress=true;
                cursor++;
                log("Команда 0x66| 00 Назначение неизвестно параметры: ");
                print_as_hex(3);
                log("\n");
                cursor+=3;
            }
            if(data.at(cursor)==0x01){
                progress=true;
                cursor++;
                log("Команда 0x66| 01 Назначение неизвестно \n");
            }
            if(data.at(cursor)==0x02){
                 progress=true;
                 cursor++;
                 log("Команда 0x66| 02 Назначение неизвестно \n");
            }
            if(!progress) {
                cursor--;
                throw ERROR_UNKNOWN_COMMAND;
            }
        break;

        //0x67 - Неизвестная Команда
        case 0x67:
            cursor++;
            log("Команда 0x67| Назначение неизвестно параметры: ");
            print_as_hex(1);
            log("\n");
            cursor+=1;
        break;

        //0x68 - Неизвестная Команда
        case 0x68:
            cursor++;
            log("Команда 0x68| Назначение неизвестно параметры: ");
            print_as_hex(1);
            log("\n");
            cursor+=1;
        break;

        //0x69 - часть диалога (в интерактивных диалогах)
        case 0x69:
            cursor++;
            temp=read_2_byte_int();
            log("Команда 0x69| Часть диалога, звук.файл: \"");
            print_as_text(temp);
            cursor+=temp;
            log("\" 0x"+QString::number(data.at(cursor)&0xff,16)+" текст: \"\n");
            cursor++;
            temp=read_2_byte_int();
            if(temp>max_string_length) max_string_length=temp;
            print_as_text(temp);
            cursor+=temp;
            log("\"\n");
        break;

        //0x6A - Неизвестная Команда
        case 0x6A:
            cursor++;
            log("Команда 0x6A| Назначение неизвестно параметры: ");
            print_as_hex(2);
            log("\n");
            cursor+=2;
        break;

        //0x6B - Неизвестная Команда
        case 0x6B:
            cursor++;
            log("Команда 0x6B| Назначение неизвестно параметры: ");
            print_as_hex(3);
            log("\n");
            cursor+=3;
        break;

        //0x6С - Неизвестная Команда
        case 0x6C:
            cursor++;
            log("Команда 0x6C| Немного приблизить персонажа\n");
        break;

        //0x71 - приглушить звук
        case 0x71:
            cursor++;
            log("Команда 0x71| Приглушить звук");
            log("\n");
        break;

        //0x73 - Неизвестная Команда
        case 0x73:

            cursor++;
            log("Команда 0x73| Назначение неизвестно, параметр 0x");
            print_as_hex(1);cursor++;
            log("\n");
        break;

        //0x75 - установить название сцены
        case 0x75:

            cursor++;
            temp=read_2_byte_int();
            log("Команда 0x75| Установить название сцены в \"");
            print_as_text(temp);
            cursor+=temp;
            log("\"\n");
        break;

        //0x76 - полупрозрачное затемнение
        case 0x76:

            cursor++;
            log("Команда 0x76| Полупрозрачное затемнение, параметр 0x");
            print_as_hex(1);cursor++;
            log("\n");
        break;

        //0x77 - неизвестно
        case 0x77:

            cursor++;
            temp=read_2_byte_int();
            log("Команда 0x77| Назначение неизвестно, номер сцены: "+QString::number(temp)+", ");
            temp=read_2_byte_int();
            log("номер записи: "+QString::number(temp)+"\n");

        break;

        //0x78 - установить номер текущей сцены
        case 0x78:

            cursor++;
            temp=read_2_byte_int();
            log("Команда 0x78| Установить номер сцены в "+QString::number(temp)+"\n");
        break;

        //0x79 - Неизвестная Команда
        case 0x79:
            cursor++;
            log("Команда 0x79| Назначение неизвестно\n");
        break;

        //0x7A - Показывать ли субтитры
        case 0x7A:

            cursor++;
            log("Команда 0x7A| Диалог показывать ли субтитры?\n");
        break;

        //0x7B - Табличка Quick Save (クイック　セーブ)
        case 0x7B:

            cursor++;
            log("Команда 0x7B| Табличка Quick Save (クイック　セーブ)\n");
        break;

        //0x7C - Неизвестная команда
        case 0x7C:
            cursor++;
            log("Команда 0x7C| Назначение неизвестно, параметры: ");
            print_as_hex(2);
            cursor+=2;
            log("\n");
        break;

        //0x7D - Неизвестная Команда
        case 0x7D:

            cursor++;
            log("Команда 0x7D| Назначение неизвестно\n");
        break;

        //0x7E - Неизвестная Команда
        case 0x7E:

            cursor++;
            log("Команда 0x7E| Назначение неизвестно\n");
        break;
        //0x7F - Отметить не прочитанным
        case 0x7F:

            cursor++;
            log("Команда 0x7F| Отметить не прочитанным\n");
        break;

        default: throw ERROR_UNKNOWN_COMMAND; break;

        }

    //Добавляем результат в dat объект
    if(add_to_dat_object){
        append_command(scur,data.mid(scur,cursor-scur));
    }
    return true;
}



void DatFile::clear_log()
{
    log_string.clear();
}

QString DatFile::get_and_clear_log()
{
    QString out;
    out.append(log_string);
    log_string.clear();
    return out;
}

void DatFile::setIfgoto(void *func_pointer)
{
    ifgotocallback = (bool(*)(int))func_pointer;
}

QString DatFile::resore_cyrillic(QString str)
{
    QString c,outstr;
    outstr="";
    for(int i=0;i<str.length();i++){
            c="";
            c.append(str.at(i));

            if(c=="A") outstr.append("А"); else
            if(c=="B") outstr.append("В"); else
            if(c=="E") outstr.append("Е"); else
            if(c=="K") outstr.append("К"); else
            if(c=="M") outstr.append("М"); else
            if(c=="H") outstr.append("Н"); else
            if(c=="O") outstr.append("О"); else
            if(c=="P") outstr.append("Р"); else
            if(c=="C") outstr.append("С"); else
            if(c=="T") outstr.append("Т"); else
            if(c=="X") outstr.append("Х"); else

            if(c=="a") outstr.append("а"); else
            if(c=="e") outstr.append("е"); else
            if(c=="o") outstr.append("о"); else
            if(c=="p") outstr.append("р"); else
            if(c=="c") outstr.append("с"); else
            if(c=="y") outstr.append("у"); else
            if(c=="x") outstr.append("х"); else


            outstr.append(str.at(i));

    }
    return outstr;
}

QString DatFile::get_map()
{
    return get_map(false, true, false);
}

QString DatFile::get_map(
		bool do_restore_cyrillic,
		bool use_source_text,
		bool print_ids
	)
{
    QString out;
    Command *com;

    int gosub_id=-1;
    bool first_goto=true;
    for(int i=0;i<commands.length();i++){
        com=commands.at(i);

        if(gosub_id<0){
            for(int k=0;k<links.length();k++){
                if(links.at(k)->to_command_id == i){
                    out+="(LABEL "+QString::number(i)+")\n";
                }
            }
        }

        //кнопка выбора
        if(com->command.at(0)==0x11){
        	if(print_ids)
				out+="[[id: "+QString::number(i)+"]]\n";
            out+="    ("+get_original_text(i)+")";
            QVector<Link*> t = get_links_from_command(i);
            out+="(GOTO "+QString::number(t.at(0)->to_command_id)+")\n";
        }

        //goto
        if(com->command.at(0)==0x13){
            QVector<Link*> t = get_links_from_command(i);
            out+="-(GOTO "+QString::number(t.at(0)->to_command_id)+")-\n";
            if(first_goto){
                i=t.at(0)->to_command_id-1;
                first_goto=false;
            }
        }

        //if
        if(com->command.at(0)==0x12){
            QVector<Link*> t = get_links_from_command(i);
            out+="-(IF ... )-(GOTO "+QString::number(t.at(0)->to_command_id)+")-\n";
        }

        //script
        if(com->command.at(0)==0x18){
            out+=" -( запустить '"+read_as_string(com->command,1)+"' )-\n";
        }

        //Интерактивный диалог
        if(com->command.at(0)==0x20){
            out+="-(Интерактивный диалог)-\n";
        }

        //Интерактивный диалог
        if(com->command.at(0)==0x1C){
            out+="-(начало выбора)-\n";
        }

        //Интерактивный диалог
        if(com->command.at(0)==0x1D){
            out+="-(конец выбора)-\n";
        }


        //Текст на табличке с именем
        if(com->command.at(0)==0x3D){
            if(com->command.at(1)==0x00 || com->command.at(1)==0x01){
                int name=read_as_2byte_int(com->command,2);
                out+="["+decrypt_name_plate(name)+"]\n";
            }
        }

        //Местоположение
        if(com->command.at(0)==0x40){
            if(com->command.at(1)==0x02){
                int place = read_as_2byte_int(com->command,2);
                out+="("+decrypt_place_plate(place)+")\n";
            }
        }

        //Scene name
        if(com->command.at(0)==0x75){
            QString tmp = read_as_string(com->command,1);
            if(do_restore_cyrillic) tmp=resore_cyrillic(tmp);
			if(print_ids)
				out+="[[id: "+QString::number(i)+"]]\n";
            out+="(("+tmp+"))\n";
        }

        //gosub
        if(gosub_id<0){
            if(com->command.at(0)==0x14){
                gosub_id=i;
                QVector<Link*> links = get_links_from_command(i);
                i=links.at(0)->to_command_id-1;
                continue;
            }
        }else{
            if(is_text_command(i)){
                QString tmp; 
				if(use_source_text)
					tmp = get_original_text(i);
				else
					tmp = get_patched_text(i); 

                if(do_restore_cyrillic) tmp=resore_cyrillic(tmp);
				if(print_ids)
					out+="[[id: "+QString::number(i)+"]]\n";
                out+=tmp+"\n";
            }
            if(com->command.at(0)==0x15){
                i=gosub_id;
                gosub_id=-1;
                continue;
            }
        }
    }

    return out;
}

QString DatFile::decrypt_name_plate(int num){
    switch(num){
        case 0x01: return QString("Кён");
        case 0x02: return QString("Харухи");
        case 0x03: return QString("Асахина-сан");
        case 0x04: return QString("Нагато");
        case 0x05: return QString("Коидзуми");
        case 0x06: return QString("Цуруя-сан");
        case 0x07: return QString("Сямисэн");
        case 0x08: return QString("Девушка");
        case 0x09: return QString("Танигути");
        case 0x0A: return QString("Куникида");
        case 0x0B: return QString("Кимидори-сан");
        case 0x0C: return QString("Асахина-сан");
        case 0x0D: return QString("Глава компьютерного клуба");
        case 0x0E: return QString("Член комп. клуба 1");
        case 0x0F: return QString("Член комп. клуба 2");
        case 0x10: return QString("Сестрёнка");
        case 0x11: return QString("Харухи");
        case 0x12: return QString("Микуру");
        case 0x13: return QString("Юки");
        case 0x14: return QString("Ицки");
        case 0x15: return QString("Рефери");
        case 0x16: return QString("Члены клуба");//множественное число
        case 0x17: return QString("Комп. Лаборатория");
        case 0x18: return QString("球拾い部隊");
        case 0x19: return QString("天の声");
        case 0x63: return QString("? ? ?");
    default:return QString("[Неизвестно (0x"+QString::number(num,16).toUpper()+")]");
    }
}

QString DatFile::decrypt_place_plate(int num)
{
    switch(num){
        case 0x00: return QString("――");
        case 0x01: return QString("Лестничная площадка");
        case 0x02: return QString("Класс Кёна");
        case 0x03: return QString("Спортивная площадка");
        case 0x04: return QString("Площадь Генкоцу");
        case 0x05: return QString("Спортзал");
        case 0x06: return QString("Запасная лестница");
        case 0x07: return QString("Комната клуба");
        case 0x08: return QString("Вход в главное здание");
        case 0x09: return QString("Класс Микуру");
        case 0x0A: return QString("Галерея");
        case 0x0B: return QString("野球場");
        case 0x0C: return QString("Пляж");
        case 0x0D: return QString("高原");
        case 0x0E: return QString("Коридор старого здания");
        case 0x0F: return QString("Столовая");
        case 0x10: return QString("Внутренний двор");
        case 0x11: return QString("Кафе");
        case 0x12: return QString("Коридор");
        case 0x13: return QString("昇降口");
        case 0x14: return QString("Главные ворота");
        case 0x16: return QString("Крыша");
        //--
    default:return QString("[Неизвестно (0x"+QString::number(num,16).toUpper()+")]");
    }
}

void DatFile::print_as_text(int count){
    if(cursor+count>data.length()) throw ERROR_OUT_OF_RANGE;
    log(read_codec->toUnicode(data.mid(cursor,count)));
}

void DatFile::print_as_hex(int count){
    if(cursor+count>data.length()) throw ERROR_OUT_OF_RANGE;
    QString temp=data.mid(cursor,count).toHex().toUpper();
    QString hex="";
    for(int i=0;i<temp.length();i+=2){
        hex.append(temp.at(i));
        hex.append(temp.at(i+1));
        hex.append(" ");
    }
    log(hex);
}


