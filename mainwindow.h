#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QSettings>

#include "aboutdialog.h"
#include "shiftjiscoder.h"
#include "datparser.h"
#include "translationhelper.h"
#include "translationhelperconfig.h"
#include "nltester.h"
#include "btdeditor.h"
#include "references.h"
#include "referencesform.h"
#include "scriptmap.h"

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    static void setTextCodec(QTextCodec *codec);
    static QString toLatin(QString string);
    static QTextCodec *curTextCodec;
    QString getCurrentLocalizedText();
    static QString getCurrentLocalizedTextStatic();
    void addToRecentFiles(const QString &filePath);
    void openScriptFile(QString string);
    void update_title(QString filename);

    void gotoId(int id);
    void updateTranslationHelper();

    QSettings* getSettings();

    static MainWindow *me;

private slots:
    void on_actionLoad_obj_triggered();

    void on_pushButton_prevRec_clicked();

    void on_pushButton_nextRec_clicked();

    void on_textEdit_localized_textChanged();

    void on_actionAbout_triggered();

    void on_actionSave_localized_file_triggered();

    void on_actionParser_Window_triggered();

    void on_pushButton_compress_clicked();

    void on_actionSave_patch_to_file_triggered();

    void on_actionLoad_patch_from_file_triggered();

    void on_actionShift_JIS_to_hex_triggered();

    void openRecent();

    void on_buttonTHConfig_clicked();

    void on_MainWindow_destroyed();

    void on_actionCopy_source_to_clipboard_triggered();

    void on_actionNew_Line_Tester_triggered();

    void on_actionSave_triggered();

    void on_pushButton_split_clicked();

    void on_actionReset_translation_triggered();

    void on_actionBTD_editor_triggered();

    void on_actionReferences_triggered();

    void on_actionScript_Map_triggered();

private:
    Ui::MainWindow *ui;


    void print_to_ui();
    void update_translation_info();
    void load_script_file();

    void updateRecentActionList();


    QTextCodec *jp_codec;
    QTextCodec *ru_codec;
    QTextCodec *utf_codec;

    QSettings *settings;
    QList<QAction*> recentFileActionList;
    const int maxFileNr=10;

    AboutDialog about_dialog;
    ShiftJisCoder shiftjiscoder;
    DatParser datparser;
    BtdEditor btdeditor;

    TranslationHelper translator;
    TranslationHelperConfig translator_form;

    NLTester nl_tester;

    ReferencesForm references;

    ScriptMap scriptmap;

    QString current_file;
    int cursor;
};


#endif // MAINWINDOW_H
