#include "translationhelperconfig.h"
#include "ui_translationhelperconfig.h"

TranslationHelperConfig::TranslationHelperConfig(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TranslationHelperConfig)
{
    ui->setupUi(this);
    network = new QNetworkAccessManager(this);
    connect(network, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(network_replyFinished(QNetworkReply*)));
}

TranslationHelperConfig::~TranslationHelperConfig()
{
    delete ui;
    delete network;
}


void TranslationHelperConfig::updateUI(){
    ui->textEdit_result->clear();
    ui->textEdit_result->append(translator->translate(source_text));

    ui->labelDBfile->setText("Words database:"+translator->getDBname());

    ui->table_item_links->clear();
    ui->table_item_links->setColumnCount(3);
    ui->table_item_links->setRowCount(translator->links.length());

    QStringList m_TableHeader;
    m_TableHeader<<"id"<<"Source"<<"Translation";
    ui->table_item_links->setHorizontalHeaderLabels(m_TableHeader);

    for(int i=0;i<translator->links.length();i++){
        ui->table_item_links->setItem(i,0,new QTableWidgetItem(QString::number(translator->links.at(i)->rowid)));
        ui->table_item_links->setItem(i,1,new QTableWidgetItem(translator->links.at(i)->source));
        ui->table_item_links->setItem(i,2,new QTableWidgetItem(translator->links.at(i)->translated));
    }
    helper2_update();
}


//------------------------ initialisation --------------------
void TranslationHelperConfig::init(QSettings *set, TranslationHelper *trans, QString _source_text){
    settings=set;
    translator=trans;
    source_text=_source_text;

    updateUI();
}

//------------------------- Changing words database ----------------
void TranslationHelperConfig::on_buttonChangeDB_clicked()
{
    QString filename;
    filename=QFileDialog::getOpenFileName(0, "Select words database", "", "SQLite3 Database(*.db3)", 0, 0);
    if(filename.isEmpty())
        return;

    settings->setValue("words_database",filename);
    settings->sync();

    translator->closeDB();
    translator->connect(filename);


    updateUI();
}


//--------------------------------- add new translation -------------------------
void TranslationHelperConfig::on_pushButton_addWord_clicked()
{
    translator->addWord(ui->lineEdit_source->text(),ui->lineEdit_translated->text());

    updateUI();
}

//delete row from table
void TranslationHelperConfig::on_pushButton_delete_clicked()
{
    int row=ui->table_item_links->currentRow();
    if(ui->table_item_links->selectionModel()->selectedRows().size()>0){
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Delete", "Do you realy want to delete translation: \n\""+
                                      translator->links.at(row)->source+"\" => \""+
                                      translator->links.at(row)->translated+"\" \n ?",
                                        QMessageBox::Yes|QMessageBox::No);

        if(reply==QMessageBox::Yes){
            translator->deleteWord(translator->links.at(row)->rowid);
            updateUI();
        }
    }
}


//Change data

void TranslationHelperConfig::on_table_item_links_itemChanged(QTableWidgetItem *item)
{

    bool changeButton=false;

    switch(item->column()){
    case 1:
        if(translator->links.at(item->row())->source!=item->text()){
            //qDebug()<<"changed! "<<item->text();
            translator->updateData(translator->links.at(item->row())->rowid,"source",item->text());
            changeButton=true;
        }
    break;

    case 2:
        if(translator->links.at(item->row())->translated!=item->text()){
            //qDebug()<<"changed! "<<item->text();
            translator->updateData(translator->links.at(item->row())->rowid,"translation",item->text());
            changeButton=true;
        }
    break;
    }

    if(changeButton){
        ui->button_apply_changes->setEnabled(true);
    }
}

void TranslationHelperConfig::on_button_apply_changes_clicked()
{
    ui->button_apply_changes->setEnabled(false);
    updateUI();
}


//--------------------------------------------------- Helper 2 -----------------------------------------------------

//--------------------------helper2 update-----------

void TranslationHelperConfig::helper2_update()
{
    cursor_mode = QTextCursor::MoveAnchor;
    ui->textBrowser_result_2->setOpenLinks(false);

    ui->textEdit_source->setText(source_text.remove("\n"));
    ui->textBrowser_result_2->setText(translator->translate2(ui->textEdit_source->toPlainText()));
}

void TranslationHelperConfig::higligth_word(QString word){
    QTextEdit *te = ui->textEdit_source;
    QString text = te->toPlainText();
    QTextCharFormat fmt;
    fmt = te->currentCharFormat();
    fmt.clearBackground();
    te->setCurrentCharFormat(fmt);
    te->setText(te->toPlainText());

    int position,from=0;
    do{
        position = text.indexOf(word,from);
        if(position>=0){
            from+=word.length();
            QTextCursor tcur = te->textCursor();
            tcur.setPosition(position);
            tcur.movePosition(QTextCursor::Right,QTextCursor::KeepAnchor,word.length());
            te->setTextCursor(tcur);
            fmt = te->currentCharFormat();
            fmt.setBackground(QBrush(QColor::fromRgbF(0,0.5,0.7,1.0)));
            te->setCurrentCharFormat(fmt);
        }
    }while(position>=0);


}

void TranslationHelperConfig::on_textBrowser_result_2_anchorClicked(const QUrl &arg1)
{
    higligth_word(arg1.toString());
}

void TranslationHelperConfig::on_pushButton_select_clicked()
{
    if(cursor_mode==QTextCursor::MoveAnchor){
        cursor_mode=QTextCursor::KeepAnchor;
        ui->pushButton_select->setText("Move");
    }else{
        cursor_mode=QTextCursor::MoveAnchor;
        ui->pushButton_select->setText("Select");
    }
}

void TranslationHelperConfig::on_cursorToLeft_clicked()
{
    QTextEdit *te = ui->textEdit_source;
    QTextCursor tcur = te->textCursor();
    tcur.movePosition(QTextCursor::Left,cursor_mode);
    te->setTextCursor(tcur);
}

void TranslationHelperConfig::on_cursorToRight_clicked()
{
    QTextEdit *te = ui->textEdit_source;
    QTextCursor tcur = te->textCursor();
    tcur.movePosition(QTextCursor::Right,cursor_mode);
    te->setTextCursor(tcur);
}

void TranslationHelperConfig::on_pushButton_clicked()
{
    QApplication::clipboard()->setText(ui->textEdit_source->textCursor().selectedText());
}
//--------------------------------------------------- yakusu.ru -----------------------------------------------------
void TranslationHelperConfig::on_pushButton_yakusu_search_clicked()
{

    QNetworkRequest request(QUrl("http://yakusu.ru"));
    request.setRawHeader(QByteArray("Accept"),QByteArray("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
    request.setRawHeader(QByteArray("User-Agent"),QByteArray("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0"));

    QTextCodec *cp=QTextCodec::codecForName("UTF-8");
    QString str;
    str=ampersand_encode(cp->fromUnicode(ui->lineEdit_yakusu->text()));
    //str=ui->lineEdit_yakusu->text();

    QByteArray ba;
    ba.append("rul=a2&id=4&str=");
    ba.append(QUrl::toPercentEncoding(str));
    network->post(request,ba);
}

void TranslationHelperConfig::network_replyFinished(QNetworkReply *rep)
{
    int index;
    QString page;
    QTextCodec *cp=QTextCodec::codecForName("CP1251");

    QJsonDocument json;

    switch (ui->tabWidget->currentIndex()) {
        case 3:

            page = cp->toUnicode(rep->readAll());

            index = page.indexOf("dictcell");
            page = page.mid(index);

            qDebug()<<"index="<<index;
            ui->textEdit_yakusu_result->setPlainText(page);
        break;
        case 4:
            page = rep->readAll();
            json = QJsonDocument::fromJson(page.toUtf8());

            if(json.object().value("code").toInt()==200){
                qDebug()<<json.object().value("text");
                ui->textEdit_yandex->setPlainText(json.object().value("text").toArray().at(0).toString());
            }else
                ui->textEdit_yandex->setPlainText(page);
        break;
    }

}

QString TranslationHelperConfig::ampersand_encode(const QString &string) {
  QString encoded;
  for(int i=0;i<string.size();++i) {
    QChar ch = string.at(i);
    if(ch.unicode() > 255)
      encoded += QString("&#%1;").arg((int)ch.unicode());
    else
      encoded += ch;
  }
  return encoded;
}
// ---------------------------------------- yandex --------------------------------
void TranslationHelperConfig::on_pushButton_yandex_go_clicked()
{
    QSettings set("config.ini",QSettings::IniFormat);
    QString key,translate_direction;
    key=set.value("yandex_apikey","").toString();

    if(ui->comboBox_yandex->currentIndex()==0)
        translate_direction = "ja-ru";
    if(ui->comboBox_yandex->currentIndex()==1)
        translate_direction = "ja-en";

    QNetworkRequest request(QUrl("https://translate.yandex.net/api/v1.5/tr.json/translate?key="+key+"&lang="+translate_direction));
    request.setRawHeader(QByteArray("Accept"),QByteArray("*/*"));
    request.setRawHeader(QByteArray("Content-Type"),QByteArray("application/x-www-form-urlencoded"));

    QByteArray param;
    param.append("text=");
    param.append(QByteArray(QUrl::toPercentEncoding(ui->lineEdit_yandex->text())));

    network->post(request,param);

}

void TranslationHelperConfig::on_pushButton_yandex_save_apikey_clicked()
{
    QSettings set("config.ini",QSettings::IniFormat);
    set.setValue("yandex_apikey",ui->lineEdit_yandex_api_key->text());
    set.sync();
}
/**
 * @brief кнопка ввода произвольного текста и перевод его
 */
void TranslationHelperConfig::on_pushButton_process_custom_text_clicked()
{
    source_text = ui->textEdit_custom_text->toPlainText();
    updateUI();
}
