#ifndef TRANSLATIONHELPERCONFIG_H
#define TRANSLATIONHELPERCONFIG_H

#include <QMainWindow>
#include <QFileDialog>
#include <QSettings>
#include <QDebug>
#include <QMessageBox>
#include <QTableWidgetItem>
#include <QTextCursor>
#include <QClipboard>
#include <QTextCodec>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <translationhelper.h>

namespace Ui {
class TranslationHelperConfig;
}

class TranslationHelperConfig : public QMainWindow
{
    Q_OBJECT

public:
    explicit TranslationHelperConfig(QWidget *parent = 0);
    ~TranslationHelperConfig();

    void init(QSettings *set, TranslationHelper *trans, QString source_text);

public slots:
    //yakusu.ru
    void network_replyFinished(QNetworkReply* rep);

private slots:
    void on_buttonChangeDB_clicked();

    void on_pushButton_addWord_clicked();

    void on_pushButton_delete_clicked();

    void on_table_item_links_itemChanged(QTableWidgetItem *item);

    void on_button_apply_changes_clicked();

    void on_textBrowser_result_2_anchorClicked(const QUrl &arg1);

    void on_pushButton_select_clicked();

    void on_cursorToLeft_clicked();

    void on_cursorToRight_clicked();

    void on_pushButton_clicked();

    void on_pushButton_yakusu_search_clicked();



    void on_pushButton_yandex_go_clicked();

    void on_pushButton_yandex_save_apikey_clicked();

    void on_pushButton_process_custom_text_clicked();

private:
    Ui::TranslationHelperConfig *ui;

    QSettings *settings;
    TranslationHelper *translator;
    QString source_text;

    void updateUI();

    //helper2
    QTextCursor::MoveMode cursor_mode;
    void helper2_update();
    void higligth_word(QString word);

    //yakusu.ru
    QNetworkAccessManager *network;
    QString ampersand_encode(const QString &string);
};

#endif // TRANSLATIONHELPERCONFIG_H
